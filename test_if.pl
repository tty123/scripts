#!/usr/bin/env perl
#===============================================================================
#
#         FILE: test_if.pl
#
#        USAGE: ./test_if.pl  
#
#  DESCRIPTION:  вариант логического оператора условия if (){} else{} - () ? () : ()
#
#      OPTIONS: ---
# REQUIREMENTS: ---
#         BUGS: ---
#        NOTES: ---
#       AUTHOR: Tolmachev V.N. (), 
# ORGANIZATION: 
#      VERSION: 1.0
#      CREATED: 19.06.2020 11:35:52
#     REVISION: ---
#===============================================================================

use strict;
use warnings;
use utf8;
use 5.010;

say "конструкция вида -  (проверка условия) ? (выполняем если true) : (выполняем если false);";
my $a = (10 eq 5 ?" true result ":" false result ");
say $a;

