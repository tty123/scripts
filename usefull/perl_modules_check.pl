#!/usr/bin/perl

# Узнать какие модули доставлены в систему
#
#
use ExtUtils::Installed;
my $instmod = ExtUtils::Installed->new();
foreach my $module ($instmod->modules())
{ my $version = $instmod->version($module) || "-"; print "$module --> $version \r\n"; }
