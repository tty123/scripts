#
#===============================================================================
#
#         FILE: MyClass.pm
#
#  DESCRIPTION: test module
#
#        FILES: ---
#         BUGS: ---
#        NOTES: ---
#       AUTHOR: Tolmachev V.N. (), 
# ORGANIZATION: 
#      VERSION: 1.0
#      CREATED: 26.03.2019 15:25:57
#     REVISION: ---
#===============================================================================
package MyClass;
use base 'Exporter';

our @EXPORT=qw(&test_hash);
our @EXPORT=qw(&test_mass);

use strict;
use warnings;
use Data::Printer;

our $VERSION = '1.00';
my %test_hash;
 $test_hash{'test'} = '123';
my @test_mass=qw(1..10);

sub new {
my $class = shift;
my $self = bless{}, $class;
return $self
}

sub ShowEnv{
my $self=shift;
my @t;
my $i=0;
foreach my $key ( keys %ENV){
	 @t[$i] = "$key => $ENV{$key}\n";
	$i++;
	}
	return @t
}
sub hello{
my ($self,$prn)=@_;
print "$self.  $prn. Hello World!!!\n";
p $self;
}
;
sub hello2{
my ($self,$prn)=@_;
print "$self.  $prn. hai World!!!\n";
p $self;
}
;
