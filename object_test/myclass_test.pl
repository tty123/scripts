#!/usr/bin/env perl
#===============================================================================
#
#         FILE: myclass_test.pl
#
#        USAGE: ./myclass_test.pl  
#
#  DESCRIPTION: testing MyClass.pm
#
#      OPTIONS: ---
# REQUIREMENTS: ---
#         BUGS: ---
#        NOTES: ---
#       AUTHOR: Tolmachev V.N. (), 
# ORGANIZATION: 
#      VERSION: 1.0
#      CREATED: 26.03.2019 15:30:46
#     REVISION: ---
#===============================================================================

use strict;
use warnings;
use utf8;

# use lib '/home/tvn/Scripts/object_test';
use FindBin qw($Bin);
use lib $Bin;

use MyClass;
use Data::Dumper;
use Data::Printer;

print "content-type: idris\n";
my $self = MyClass ->new();
print Dumper($self);
print "--------------\n";
p $self;
print $self->ShowEnv;
my $sd='test';
$self->hello($sd);

my $sd2='green';
MyClass->hello2($sd2);

# my %df=MyClass::test_hash;
my @mf=MyClass::test_mass;

#p %df;
p @mf;

