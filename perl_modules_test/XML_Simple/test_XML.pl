#!/usr/bin/env perl
#===============================================================================
#
#         FILE: test_XML.pl
#
#        USAGE: ./test_XML.pl  
#
#  DESCRIPTION: отладка считывания и записи XML файла
#
#      OPTIONS: ---
# REQUIREMENTS: ---
#         BUGS: ---
#        NOTES: ---
#       AUTHOR: Tolmachev V.N. (), 
# ORGANIZATION: 
#      VERSION: 1.0
#      CREATED: 21.05.2020 11:28:13
#     REVISION: ---
#===============================================================================

use strict;
use warnings;
use utf8;
use XML::Simple;
use Data::Printer;

my $ref = XMLin('test_out.xml'); #забираем из файла XML в хэш
p $ref;


my %hash_ref = (
		root=>{
			fields=>{
				field100=>'0123456',
				AMOUNT_ALL=>'1000',
				test=>'проверка',
			},
			payment_create_dt=>'10.12.2019 16:42:43',

		}
);

my $path='./XML/test_out.xml';
open my $fh, '>:encoding(windows-1251)', $path or die "open($path): $!";
XMLout(\%hash_ref, OutputFile => $fh, NoAttr => 1, KeepRoot=>1, XMLDecl=>'<?xml version="1.0" encoding="windows-1251" standalone="yes"?>');
