#!/usr/bin/env perl
#===============================================================================
#
#         FILE: test_array.pl
#
#        USAGE: ./test_array.pl  
#
#  DESCRIPTION: тестирование массивов
#
#      OPTIONS: ---
# REQUIREMENTS: ---
#         BUGS: ---
#        NOTES: ---
#       AUTHOR: Tolmachev V.N. (), 
# ORGANIZATION: 
#      VERSION: 1.0
#      CREATED: 08.04.2020 17:26:02
#     REVISION: ---
#===============================================================================

use strict;
use warnings;
use utf8;
use 5.010;
use Data::Printer;

# Проверка, что массив все еще пуст.
my @match=(2,3);
say "variant 1 ok" if (!@match);
say "variant 2 ok" if (@match == 0);
say "variant 3 ok" if (scalar @match == 0);
##########################################

# Аналог функции split, в массив попадает список директорий и файл
my $a = "/usr/local/perl/perl.bin";
my @dir = $a=~m[/(\w*)/(\w*)/(\w*)/(.*)];
p @dir;
##########################################

my @a = qw/delo protiv vsex/; # задаём массив через список без кавычек
p @a;

##########################################
#say qx/ls -la/; # интерполяция с последующим исполнением

##########################################
# удаление дубликатов из массива
##########################################
my @src = qw# 1 2 3 4 5 6 6 1 9 8#;
my %hash = map { $_ => 1} @src;
my @dst = sort {$b <=> $a} keys %hash; # сортировка чисел в массиве через {$a<=>$b), строк {$a cmp $b}
p @dst;

