#!/usr/bin/env perl
#===============================================================================
#
#         FILE: test_given.pl
#
#        USAGE: ./test_given.pl  
#
#  DESCRIPTION: тестирование конструкции вида given
#
#      OPTIONS: ---
# REQUIREMENTS: ---
#         BUGS: ---
#        NOTES: ---
#       AUTHOR: Tolmachev V.N. (), 
# ORGANIZATION: 
#      VERSION: 1.0
#      CREATED: 26.03.2020 14:43:21
#     REVISION: ---
#===============================================================================

use strict;
use warnings;
use utf8;
use open qw(:std :utf8);
no warnings 'experimental'; 
use v5.16;

my $num = rand();

given ($num){
	when ($_>0.7){
		say "num > 0.7";
	}
	when ($_>0.4){
		say "num > 0.4";
	}
	default{
		say "num = $_ samething else";
	}
}

