#!/usr/bin/env perl
#===============================================================================
#
#         FILE: test_sprintf.pl
#
#        USAGE: ./test_sprintf.pl  
#
#  DESCRIPTION: 
#
#      OPTIONS: ---
# REQUIREMENTS: ---
#         BUGS: ---
#        NOTES: ---
#       AUTHOR: Tolmachev V.N. (), 
# ORGANIZATION: 
#      VERSION: 1.0
#      CREATED: 23.03.2020 17:11:33
#     REVISION: ---
#===============================================================================

use strict;
use warnings;
use utf8;
use DDP;
use 5.010;

# перевод из HEX -> BIN
my $str='FF';
say sprintf('%b', hex($str));

# форматированный вывод переменной
my $str = 5000;
say sprintf('%6s.##', $str);
