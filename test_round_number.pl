#!/usr/bin/env perl
#===============================================================================
#
#         FILE: test_round_number.pl
#
#        USAGE: ./test_round_number.pl  
#
#  DESCRIPTION: процедура дробного числа до ближайшего целого
#
#      OPTIONS: ---
# REQUIREMENTS: ---
#         BUGS: ---
#        NOTES: ---
#       AUTHOR: Tolmachev V.N. (), 
# ORGANIZATION: 
#      VERSION: 1.0
#      CREATED: 31.01.2020 00:04:10
#     REVISION: ---
#===============================================================================

use strict;
use warnings;
use utf8;
use Data::Printer;
sub  name  {
my $number=shift;
        return 0 if(!$number);
        $number =~ s/,/./; 
     my   $n=0;my $r=0;
        ($n,$r)=split('\.',$number);
        if($r) {
	            $r=substr($r, 0, 1);  
	            } else {
		               $r=0;
		               }
		               $n++ if ($r>=5);
		               return $n;
		       }
my $test = name(6.55);
p $test;
