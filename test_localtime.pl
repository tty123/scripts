#!/usr/bin/env perl
#===============================================================================
#
#         FILE: test_localtime.pl
#
#        USAGE: ./test_localtime.pl  
#
#  DESCRIPTION: тестирование функции localtime 
#
#      OPTIONS: ---
# REQUIREMENTS: ---
#         BUGS: ---
#        NOTES: ---
#       AUTHOR: Tolmachev V.N. (), 
# ORGANIZATION: 
#      VERSION: 1.0
#      CREATED: 26.12.2019 17:28:49
#     REVISION: ---
#===============================================================================

use strict;
use warnings;
use utf8;
use Time::Piece;
use POSIX qw(strftime);
use 5.010;

my ($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst) = localtime(time);
print "$sec sec,$min min,$hour hour,$mday mday,$mon month,$year year,$wday wday,$yday yday,$isdst isdst\n";
#my ($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst) = localtime(time)[];

my $tt=localtime;
say "func localtime = ".$tt; #"Thu Oct 13 04:54:34 1994"
say "func localtime(time) = ".localtime(time);

my $tp = Time::Piece -> new;
say $tp->datetime;

#POSIX strftime
#my $now_string = strftime "%d.%m.%Y T %X", localtime;
my $now_string = strftime "%d.%m.%Y %X", localtime;
say "POSIX strftime  = $now_string";
