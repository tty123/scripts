#!/usr/bin/env perl
#===============================================================================
#
#         FILE: test_printer_status.pl
#
#        USAGE: ./test_printer_status.pl  
#
#  DESCRIPTION: получение статуса от POS принтера 
#               VKP80II 
#      OPTIONS: ---
# REQUIREMENTS: ---
#         BUGS: ---
#        NOTES: ---
#       AUTHOR: Tolmachev V.N. (), 
# ORGANIZATION: 
#      VERSION: 1.0
#      CREATED: 18.05.2020 16:48:53
#     REVISION: ---
#===============================================================================

use strict;
use warnings;
use utf8;
use Data::Printer;

my $device="/dev/usb/lp0";
open(PRN,"+>$device")|| die "device not open: $!";

## La sequenza di ESCAPE DLE EOT n consente 
## la trasmissione in realtime
## dello status
## n=1: printer status
## n=2: printer offline status
## n=3: error status
## n=4: paper roll sensor status
## 
## Per n=4 i bits valorizzati sono:
## BIT   Off/On  Dec Desc
## 0     Off     0   not used, fixed to Off
## 1     On      2   not used, fixed to On
## 2,3   Off     0   Paper adequate
## 2,3   On      12  Paper near end detected
## 4     On      16  Not used, fixed to On
## 5,6   Off     0   Paper present   
## 5,6   Off     96  Paper roll end
## 7     Off     0   Not used, fixed to Off

#my $send_str = kbyte(16).kbyte(4).kbyte(1);
#my $send_str = kbyte('1de700005449434554310a5449434554320a5449434554330a1df81b69');#напечатали  
										   #            TICKET1 
										   #            TICKET2
										   #            TICKET3
										   #            и отрезали чек
										   
my $send_str = kbyte('100404');# ASC POS command: DLE EOT n
			       #             hex:  10 04 04
			       # Paper roll sensor status
p $send_str;
print(PRN $send_str);

#close(PRN);

#open(PRN,"$device");
#$ret=fgets($r_printer);
#sleep(2);

#my $ret=getc(PRN);
my $ret;
read PRN, $ret, 1;
p $ret;
close(PRN);
print unpack('B*',$ret)."\n";
print unpack('H*',$ret)."\n";
print unpack('C*',$ret)."\n";
#$bit_val=ord($ret[0]);
#print "Retval=".$bit_val;

my $status = unpack('C*', $ret);
my $rulon_paper = $status&12;
my $paper_in_feed = $status&96;
print "res (x&12) = ".($status & 12)."\n";
print "res (x&96) = ".($status & 96)."\n";
print "vector = ".vec($ret,0,8)."\n";

if ($rulon_paper){ print "paper bulk ended\n";} else {print "paper bulk full\n";};

if ($paper_in_feed){ print "paper out\n";} else {print "paper in feed\n"};
#if(($bit_val & 12) || ($bit_val & 96))
#    print "******Out of paper******\n";
#    else
#        print "---Paper ok\n";

#	function kbyte($num) {
#	    return pack('C', $num);
#    }
#
sub kbyte{
my $num=shift;
return pack('H*', $num);
}
