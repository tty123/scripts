#!/usr/bin/env perl
#===============================================================================
#
#         FILE: test_regex.pl
#
#        USAGE: ./test_regex.pl  
#
#  DESCRIPTION: 
#
#      OPTIONS: ---
# REQUIREMENTS: ---
#         BUGS: ---
#        NOTES: ---
#       AUTHOR: Tolmachev V.N. (), 
# ORGANIZATION: 
#      VERSION: 1.0
#      CREATED: 21.01.2020 09:43:27
#     REVISION: ---
#===============================================================================

use strict;
use warnings;
use utf8;
binmode(STDOUT,':utf8');

use Data::Printer;
use Data::Dumper;
use 5.010;

#sub check {
#my ($ip) = @_;
#
#print Dumper $ip;
#
##my %error;
#a
##my $res;
##
##use re 'debug';  # включение отладки регулярки  
##
##my $ip=\'10.240.16.54';
##my $regex=qr/^(\d{0,3})\.(\d{0,3})\.(\d{0,3})\.(\d{0,3})$/i;
##no re 'debug';
##if ((${$ip}=~$regex)){   #  проверка валидности ip адреса
##p $1;
##p $2;
##p $3;
##p $4;
##	}
#use re 'debug';
#my $rg = '1.2.3.6666';
#my $rg1 = '1.2.1';
#
#my $oid = '1.2';
#$rg1 =~ qr/^($oid)\.(.+)$/i;
#p $1;
#p $2;
#no re 'debug';

use re 'debug';
my $oid = '1.6.2.1.4.6790';
$oid =~ s/^(.*\.(\d+))$/port $2/;
no re 'debug';
p $oid;


say '-------------------------';
my  $k='8015';
if ($k=~/80(..)/){say $1} else {say'not ok'}


say '-------------------------';
my $dogovor = '12220a';
if ($dogovor=~/^\d+$/){say 'dogovor OK'}
else {say 'dogovor not OK'}

#use re 'debug';
say '--------- для мобилок ----------------';
my @mobile = ('+79025557199', '890266699234','2323245656', 'assssdd','123445566777');
foreach (@mobile){
	if ($_=~m/^[+7|7|8]\d{9,12}$/i){
        say $_;
	}
}

#no re 'debug';
