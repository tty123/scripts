#!/usr/bin/env perl
#===============================================================================
#
#         FILE: oid-search.pl
#
#        USAGE: ./oid-search.pl  
#
#  DESCRIPTION: тестовый скрипт проверяем значения OID-ов для разных моделей коммутаторов.
#              
#      OPTIONS: ---
# REQUIREMENTS: ---
#         BUGS: ---
#        NOTES: ---
#       AUTHOR: Tolmachev V.N. (), 
# ORGANIZATION: Vladlink.ru 
#      VERSION: 1.0
#      CREATED: 13.02.2019 08:56:38
#     REVISION: ---
#===============================================================================

use strict;
use warnings;
use utf8;
use Net::SNMP;
use Data::Printer;

$|=1; # отключаем буферизацию вывода
#----------------------------------------------------------------
my $file_input='./info/ip_dlink_model'; #'./info/ip-sw_model'; #
my $file_output='oid.txt';
my $community = 'public';
#----------------------------------------------------------------
my $val;
my @ipm; # массив ip адресов полученный из файла
my %ip_mac;

my @oids=();
my @tt=();
my $kt;
$tt[0]='';

my %routeros=(                          # RouterOS RB1100AHx2 (enterprise.14988.1)
	'sysDescr'                    => '1.3.6.1.2.1.1.1',   # название модели свитча
	'sysObjectID'                 => '1.3.6.1.2.1.1.2',   # VendorID
	'sysUpTime'                   => '1.3.6.1.2.1.1.3',   # время работы после последней перезагрузки
	'sysName'                     => '1.3.6.1.2.1.1.5',   # присвоенное пользователем имя
#	'ifAdminStatus'               => '1.3.6.1.2.1.2.2.1.7', # массив значений по каждому сетевому интерфейсу
#	'ifOperStatus'                => '1.3.6.1.2.1.2.2.1.8', # массив значений по каждому сетевому интерфейсу
#	'ifInUcastPkts'               => '1.3.6.1.2.1.2.2.1.11', # массив значений по каждому сетевому интерфейсу
#	'ifInNUcastPkts'              => '1.3.6.1.2.1.2.2.1.12', # массив значений по каждому сетевому интерфейсу
#	'ifInErrors'                  => '1.3.6.1.2.1.2.2.1.14', # массив значений по каждому сетевому интерфейсу
#	'ifOutUcastPkts'              => '1.3.6.1.2.1.2.2.1.11', # массив значений по каждому сетевому интерфейсу точка-точка
#	'ifOutNUcastPkts'             => '1.3.6.1.2.1.2.2.1.12', # массив значений по каждому сетевому интерфейсу широковещат+мультикаст
#	'ifOutErrors'                 => '1.3.6.1.2.1.2.2.1.20', # массив значений по каждому сетевому интерфейсу
##	'entPhysicalHardwareRev'      => '1.3.6.1.2.1.47.1.1.1.1.8', # hardware revision
##	'entPhysicalFirmwareRev'      => '1.3.6.1.2.1.47.1.1.1.1.9', # firmware revision
##	'entPhysicalSoftwareRev'      => '1.3.6.1.2.1.47.1.1.1.1.10', # software revision
##	'entPhysicalSerialNum'        => '1.3.6.1.2.1.47.1.1.1.1.11', # строка с серийником
##	'entPhysicalModelName'        => '1.3.6.1.2.1.47.1.1.1.1.13', # название модели
##	'entPhysicalDescr'            => '1.3.6.1.2.1.47.1.1.1.1.2' , # (.*) 1 строка-модель устройства далее описание каждого порта 100мбит 10000 fiber copper
#	'ifInMulticastPkts'           => '1.3.6.1.2.1.31.1.1.1.2', # (.*) количество мультикаст пакетов по  портам	
#	'ifInBroadcastPkts'           => '1.3.6.1.2.1.31.1.1.1.3', # (.*) количество широковещат  пакетов по  портам	
#	'ifOutMulticastPkts'          => '1.3.6.1.2.1.31.1.1.1.4', # (.*) количество мультикаст пакетов по всем портам	
#	'ifOutBroadcastPkts'          => '1.3.6.1.2.1.31.1.1.1.5', # (.*) количество широковещат пакетов по всем портам
	#-- enterprise ---
#	'temperatureSensor'           => '1.3.6.1.4.1.40418.2.2.4.1', # (30) температура в градусах
#	'voltageSensorContact10'      => '1.3.6.1.4.1.40418.2.2.4.2', # (1420) Напряжение на сенсоре 14.2V	
);

#my $OID_mac_find=$dlink2{'swFanStatus'}; #показать мак-адрес
##my $OID_mac_find1=$dlink2{'swPowerID'}; #показать мак-адрес
#push (@oids, $OID_mac_find);
##push (@oids, $OID_mac_find1);

while (my ($dd, $OID_mac_find) = each %routeros){

## создаём массив адресов из файла
#	open (FILE, $file_input) or die "can`t open file";
#	while (defined (my $file_line = <FILE>)) {
#		if ($file_line=~/\s(\d{1,3}[.]\d{1,3}[.]\d{1,3}[.]\d{1,3})\s/){
#	push(@ipm,$1);
#		}
#	}
#	foreach (@ipm) {print "$_\n"}
#	close FILE;
#
#		# блок индикатора загрузки
#		my $mass_l=$#ipm+1;
#		print "----------------------------\n";
#		print "Find $mass_l ip.\n";
#		print "Starting  SNMP send-request: \n"; 
#		my $u_count=0; # подсчет неудачных запросов мак адреса
#		my $ig=0; 
#		my $igg=50; # максимальное  количество звёздочек на строку 
#		my $var=$igg;
#		my $z;
@ipm=('10.244.18.210', '10.240.11.242', '10.240.11.243','10.240.244.18','10.240.244.244','10.240.250.128','10.240.249.12');
@oids=();
push (@oids, $OID_mac_find);
# по каждому адресу получаем мак и создаём хэш адрес-макa
	foreach (@ipm){
	my ($session, $error) = Net::SNMP->session(
                        -hostname      => $_,
                        -community     => $community,
                        -translate     => [-octetstring => 0],
			#-version       => 'snmpv2',
                        #-version       => 'snmpv2c',
			-timeout       => 1,
                     );
my	$rfc_version = $session->version(); 
p $rfc_version;
	if (!defined $session) {
	printf "ERROR: %s.\n", $error; 
	exit 1;}

	my $result = $session->get_next_request(           # get_request for snmp v1
#	my $result = $session->get_table(             # get_table for snmp v2c 
			-varbindlist => \@oids,       # get_request for snmp v1
		        #-baseoid => $OID_mac_find,   # get_table for snmp v2c
	);

p $_;
p $OID_mac_find;
p $result;

	}
}

print "Save to file: $file_output\n";
## выводим хэш в файл 
#	open (FILE,">$file_output") or die " can`t open file";
#
#	while ( my ($ip_key, $ip_value) = each(%ip_mac) ) {
##	print FILE "$ip_key    $ip_value\n";
##вставляем в мак пробелы или чтонибудь другое
##	$ip_value=~s/(..)(..)(..)(..)(..)(..)/$1 $2 $3 $4 $5 $6/;
#	print FILE "        ";
#	printf FILE "%-20s", $ip_key;
#	print FILE "        ";
#	printf FILE "%17s", "$ip_value\n";
#	 }
#
#	close(FILE);

# сортировка массива

