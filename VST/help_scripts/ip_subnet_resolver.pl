#!/usr/bin/env perl
#===============================================================================
#
#         FILE: ip_subnet_resolver.pl
#
#        USAGE: ./ip_subnet_resolver.pl  
#
#  DESCRIPTION: отработка процедуры соответствия ip подсети
#
#      OPTIONS: ---
# REQUIREMENTS: ---
#         BUGS: ---
#        NOTES: ---
#       AUTHOR: Tolmachev V.N. (), 
# ORGANIZATION: 
#      VERSION: 1.0
#      CREATED: 17.05.2019 14:29:14
#     REVISION: ---
#===============================================================================

use strict;
use warnings;
use utf8;
use Net::IP::Match::XS;
use Devel::Timer;

my $find_net = '123.123.45.4/30'; 
my $some_ip  = '123.123.45.4'; 
my $timing = Devel::Timer->new();
$timing->mark('start programm');

my ($net_ip, $net_mask) = split(/\//, $find_net); 
my ($ip1, $ip2, $ip3, $ip4) = split(/\./, $net_ip); 
my $net_ip_raw = pack ('CCCC', $ip1, $ip2, $ip3, $ip4); 
my $net_mask_raw = pack ('B32', (1 x $net_mask), (1 x (32 - $net_mask)));

   ($ip1, $ip2, $ip3, $ip4) = split(/\./, $some_ip); 
my $some_ip_raw = pack ('CCCC', $ip1, $ip2, $ip3, $ip4);

# $some_ip_raw  вычисляем по аналогии с $net_ip_raw 
if (($some_ip_raw & $net_mask_raw) eq $net_ip_raw){ 
  print "$some_ip входит в подсеть $find_net\n"; 
}else { print "not ok.\n"} 

$timing->mark('start module');
if (match_ip($some_ip, $find_net)) { print "ok\n"; } else {print "not ok\n";};
$timing->mark('stop module');
$timing -> report (sort_by => 'interval');
