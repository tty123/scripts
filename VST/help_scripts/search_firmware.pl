#!/usr/bin/env perl
#===============================================================================
#
#         FILE: search_firmware.pl
#
#        USAGE: ./search_firmware.pl  
#
#  DESCRIPTION: Поиск коммутатора DGS-1210-52ME-B1 с прошивкой 7.03 MW007
#               для Серёги
#      OPTIONS: ---
# REQUIREMENTS: ---
#         BUGS: ---
#        NOTES: ---
#       AUTHOR: Tolmachev V.N. (), 
# ORGANIZATION: 
#      VERSION: 1.0
#      CREATED: 26.08.2019 14:45:26
#     REVISION: ---
#===============================================================================

use strict;
use warnings;
use utf8;

use Date::Parse;
use JSON;
use ClickHouse;
use Devel::Timer;
use Data::Printer;
use Data::Dumper;
use Storable;
$|=1; 

#==== Подключаем ClickHouse ===============
my $ch = ClickHouse->new(                   # активируем коннект на кликхаус
	database => 'igator_db',
	host     => '172.17.0.188',
	user	 => 'default',
	password => 'peretz',
	port     => 8123,
	) or die "Error connecting clickhouse database" ;
#----------------------------------------------------------
my $time = str2time('2019-08-20 15:07:20');
p $time;
my $rows = $ch->select("SELECT time, node, ip, data, condition FROM igator_db.dataset WHERE time = '$time' AND condition = 'ok'"); # загружаем данные из клауса
#p $rows;
my %res;
for my $row (@$rows) {
#p $row;
my $perl_scalar = decode_json @$row[3];
#  %res = $$perl_scalar;
if (defined $perl_scalar->{sysDescr}->{0} =~ /DGS-1210/){
if ( defined $perl_scalar->{sysFirmwareVersion}) {print "$row->[2] - $perl_scalar->{sysDescr}{0} - $perl_scalar->{sysFirmwareVersion}{0}\n";
	if ($perl_scalar->{'sysFirmwareVersion'}->{0} eq '7.03.MW007'){my $df = $perl_scalar->{'sysFirmwareVersion'}->{0}; p $df}
	}
#my $tt = $perl_scalar->{sysFirmwareVersion};
#print Dumper $tt;
}
}
#my $str = $rows->[0]->[1];
#@node_ipm = split(' ',$str);
#p @node_ipm;
##p @ipm; #--test


