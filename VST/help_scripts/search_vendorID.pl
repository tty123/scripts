#!/usr/bin/env perl
#===============================================================================
#
#         FILE: search_vendorID.pl
#
#        USAGE: ./search_vendorID.pl  
#
#  DESCRIPTION:  Поиск ip  соответствующих vendorID в базе igator
#
#      OPTIONS: ---
# REQUIREMENTS: ---
#         BUGS: ---
#        NOTES: ---
#       AUTHOR: Tolmachev V.N. (), 
# ORGANIZATION: 
#      VERSION: 1.0
#      CREATED: 18.09.2019 11:19:11
#     REVISION: ---
#===============================================================================

use strict;
use warnings;
use utf8;

use Date::Parse;
use JSON;
use ClickHouse;
use Devel::Timer;
use Data::Printer;
use Data::Dumper;
use Storable;
$|=1; 

#==== Подключаем ClickHouse ===============
my $ch = ClickHouse->new(                   # активируем коннект на кликхаус
	database => 'igator_db',
	host     => '172.17.0.188',
	user	 => 'default',
	password => 'peretz',
	port     => 8123,
	) or die "Error connecting clickhouse database" ;
#----------------------------------------------------------
my $time = str2time('2019-09-19 09:45:00');
p $time;
my $rows = $ch->select("SELECT time, node, ip, data, condition FROM igator_db.dataset WHERE time = '$time' AND condition = 'ok'"); # загружаем данные из клауса
#p $rows;
my %res;
for my $row (@$rows) {
#p $row;
my $perl_scalar = decode_json @$row[3];
if (defined ($perl_scalar->{sysObjectID}->{0})){ if ($perl_scalar->{sysObjectID}->{0} =~ /^1\.3\.6\.1\.4\.1\.17409\.1\.10/){
	my $df = $perl_scalar->{'sysObjectID'}->{0}; print "$$row[2]   $df\n";
	}
}
}

