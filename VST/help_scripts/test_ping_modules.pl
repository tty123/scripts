#!/usr/bin/env perl
#===============================================================================
#
#         FILE: node_requester.pl
#
#        USAGE: ./node_requester.pl  
#
#  DESCRIPTION: скрипт опрашивающий по SNMP коммутаторы узла
#
#      OPTIONS: ---
# REQUIREMENTS: ---
#         BUGS: ---
#        NOTES: ---
#       AUTHOR: Tolmachev V.N. (), 
# ORGANIZATION: 
#      VERSION: 1.0
#      CREATED: 17.05.2019 11:02:13
#     REVISION: ---
#===============================================================================

use strict;
use warnings;
use utf8;

use FindBin qw($Bin); # юзы для моего модуля
use lib $Bin;
use igator; 

use DBI();
use DBD::Pg;

use Socket;
use IO::Socket::INET;
use AnyEvent;
use AnyEvent::Ping;
use AsyncPing;
use AnyEvent::Run;
use AnyEvent::Socket;
use AnyEvent::FastPing;
use AnyEvent::SNMP;

use Net::SNMP;
use Net::IP;
use Net::IP::Match::XS;

use Devel::Timer;
use Data::Printer;
use Data::Dumper;


#----------------------------------------------------------
#  заполнить для подключения к базе 
#----------------------------------------------------------
my $host = '172.17.31.14'; # 172.17.31.13 ++++++++++++++ change
my $db_name = "utm";
my $port=5440;
my $user = "bfork";
my $pass = "nw47g979wh4t94k";

#---------------------------------------------------------
#  Параметры узла.  Адрес: г.Владивосток ул. Некрасовская д. 76с3 п. 1 № 1 (3841) Cisco 6509   (3849) Cisco 6509 
#---------------------------------------------------------
my $node_ID = ''; # уникальный идентификатор узла
my @node_subsets = ('10.240.250.0/24','10.240.0.0/22','10.240.4.0/22','10.240.12.0/22',
'10.240.60.0/22','10.243.4.0/22'); # список подсетей принадлежащих узлу
my @node_ipm; # массив ip-адресов принадлежащих узлу
my @accepted_node_ipm; # очищенный список адресов (только то, что нужно)
#---------------------------------------------------------
# параметры SNMP
#---------------------------------------------------------
my $community = 'public';

#---------------------------------------------------------
my @snmp_response;
my @response;
#----------------------------------------------------------

#==== Работа с базой данных ===============================
my $dbh = DBI->connect("DBI:Pg:dbname=$db_name; host=$host; port=$port", $user, $pass,{AutoCommit => 1}) 
		or die "Error connecting to database";
		$dbh->do("set application_name = 'perl_snmp';");
DBI->trace(1);
#----------------------------------------------------------

################# создаём массив ip адресов из базы #################################################
my @ipm; # массив ip адресов полученный из файла
my $ip_a;
my $query = $dbh->prepare("SELECT ip_address FROM switch_map.swp0;");
			  $query->execute;
		$query->bind_col(1,\$ip_a);
while ($query->fetch){ push (@ipm, $ip_a);
                                 } 
               $query->finish;
p @ipm; #--test
#----------------------------------------------------------------------------------
################ создаём массив @node_ipm адресов для даннного узла  #################################
foreach (@ipm) {
	if (match_ip($_, @node_subsets)) { push(@node_ipm, $_);}
};
p @node_ipm; #--test#

#-------------------------------------------------------------------
############### проверяем доступность адресов, отделяем молчащих  #####################################
my $t = Devel::Timer -> new();
$t -> mark ('start pinging');
my ($ref_ipm_pinged, $ref_ipm_notpinged) = igator -> ping_ipm (\@node_ipm);
my @node_ipm_pinged = @$ref_ipm_pinged;
my @node_ipm_notpinged = @$ref_ipm_notpinged;
p @node_ipm_pinged;
p @node_ipm_notpinged;

#foreach (@node_ipm){
#my $host  = $_;
#my $times = shift || 1;
#my $package_s = shift || 56;
#my $c = AnyEvent->condvar;
# 
#my $ping = AnyEvent::Ping->new(interval => 0.1, timeout => 0.2 );
# 
#print "PING $host $package_s(@{[$package_s+8]}) bytes of data\n";
#$ping->ping($host, $times, sub {
#    my $results = shift;
#    foreach my $result (@$results) {
#        my $status = $result->[0];
#        my $time   = $result->[1];
#        printf "%s from %s: time=%f ms\n", 
#            $status, $host, $time * 1000;
#if ($status eq 'OK') {push(@node_ipm_pinged, $host )}
#if ($status eq 'TIMEOUT') {push(@node_ipm_notpinged, $host)}
#    };
#    $c->send;
#});
# 
#$c->recv;
#$ping->end;
#}

##my @HANDLERS;
## 
##my $cv = AnyEvent->condvar;
## 
##foreach my $ip (@node_ipm)
##{
##  my $handle = AnyEvent::Run->new(
##    cmd      => [ 'ping', $ip, '-c1 -i0.2 -w1' ],
##    priority => 19,
##    on_read  => sub {
##      my $handler = shift;
##      $handler->push_read(
##        line => sub {
##          my ( $hdl, $line ) = @_;
###	  p $hdl;
##          p $line;
##        }
##      );
##    }
##  );
## 
##  push @HANDLERS, $handle;
##}
## 
##$cv->recv;

#@node_ipm = ('10.240.4.255','10.240.250.11','10.240.1.193', '10.240.0.67'); 
###my $asyncping=new AsyncPing(timeout=>3,try=>1); # tested 3-2
###my $result=$asyncping->ping(\@node_ipm);
###print Dumper $result;
###my $key; 
###my $value;
###while ( ($key, $value) = each %$result ) {
###	if ($value){ push(@node_ipm_pinged, $key) }
###	else { push(@node_ipm_notpinged, $key) };
###print "$key => $value\n";
###}
###
###p @node_ipm_pinged;
###p @node_ipm_notpinged;
###
$t -> mark ('end pinging');
$t -> report();
#-------------------------------------------------------------------
my $info_oid = igator -> get_vendor_id (@node_ipm_pinged); # получаем ссылку на хэш с vendor_oid

p $info_oid;
my $number = scalar @node_ipm; 
print "all node ip numbers = $number \n";
my $size = scalar keys %{$info_oid};
print "vendor_oid_ip_numbers = $size\n"; 






########## делаем выборку vendor_id для адресов узла ######################
#my $num_th = 100; # количество поднятых воркеров
##-----------------------------------------------------------------------
## функция для треда
##-----------------------------------------------------------------------
#		my @tt;
#		my %th_hash;
#		my $res;
#my $sysObjectID = '1.3.6.1.2.1.1.2.0'; # вендор ID
#push (my @oids, $sysObjectID) ;
#MMM: while (@node_ipm){
#my $cv = AnyEvent->condvar; 
#$cv->begin;
#for (my $i=0; $i < $num_th; $i++){
##while (@ipm)	{
#
#		my $ip_=pop @node_ipm;
#		unless ($ip_) {last};
#		$cv->begin;
#		#my $session = 
#		Net::SNMP->session(
#				-hostname      => $ip_,
#				-community     => $community,
#			       -translate     => [-octetstring => 0],
#			      # -version       => 'snmpv2c',
#			#	-timeout       => 1,
#				-nonblocking => 1,
#			     )->get_request(
#				-varbindlist => \@oids,
#			       # -delay => 1,
#				-callback => sub {
#			my $snmp_q=shift;
#			my %result;
##p $ip_;
#	if (defined $snmp_q->var_bind_list()){
#	 $result{$sysObjectID} = $snmp_q->var_bind_list()->{$sysObjectID}; }
##p %result ;
#		my $rr;
#		my @vv=values %result;
#		if (@vv==0){@tt[0]='timeout'} else {@tt=values %result}; 
#		if ($tt[0] eq 'timeout') {$rr='undefined';} 
#			else {$rr = $tt[0]};
#		$th_hash{$ip_}=$rr;
#	print" thread ip = $ip_ model=$rr; \n";
#	#	Net::SNMP->close();
#			$cv->send(\%th_hash);	});
##snmp_dispatcher();
#		#$cv->end;
#		}
#$cv->end; 
#$res=$cv->recv;   #wait;
##p $res;
#}
#p $res;

############# разделяем адреса узла на 2 группы accepted и undefined  #####################
#my %node_info = %{res};
#my %accepted_
#my %undef_vendor = ;










