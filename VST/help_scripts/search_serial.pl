#!/usr/bin/env perl
#===============================================================================
#
#         FILE: search_serial.pl
#
#        USAGE: ./search_serial.pl  
#
#  DESCRIPTION: Поиск серийных номеров D-Link в базе ClickHouse сервиса igator
#		для Вовы
#      OPTIONS: ---
# REQUIREMENTS: ---
#         BUGS: ---
#        NOTES: ---
#       AUTHOR: Tolmachev V.N. (), 
# ORGANIZATION: 
#      VERSION: 1.0
#      CREATED: 21.08.2019 10:03:53
#     REVISION: ---
#===============================================================================

use strict;
use warnings;
use utf8;

use Date::Parse;
use JSON;
use ClickHouse;
use Devel::Timer;
use Data::Printer;
use Data::Dumper;
use Storable;
$|=1; 

#==== Подключаем ClickHouse ===============
my $ch = ClickHouse->new(                   # активируем коннект на кликхаус
	database => 'igator_db',
	host     => '172.17.0.188',
	user	 => 'default',
	password => 'peretz',
	port     => 8123,
	) or die "Error connecting clickhouse database" ;
#----------------------------------------------------------
my $time = str2time('2019-08-20 15:07:20');
p $time;
my $rows = $ch->select("SELECT time, node, ip, data, condition FROM igator_db.dataset WHERE time = '$time' AND condition = 'ok'"); # загружаем данные из клауса
#p $rows;
my %res;
for my $row (@$rows) {
#p $row;
my $perl_scalar = decode_json @$row[3];
#  %res = $$perl_scalar;
if (defined $perl_scalar->{sysDescr}->{0} =~ /DGS-1210/){
if ( defined $perl_scalar->{sysSerialNumber}) {print "$row->[2] - $perl_scalar->{sysDescr}{0} - $perl_scalar->{sysSerialNumber}{0}\n";
	if ($perl_scalar->{'sysSerialNumber'}->{0} eq 'SB33033970'){my $df = $perl_scalar->{'sysSerialNumber'}->{0}; p $df}
	}
#my $tt = $perl_scalar->{sysSerialNumber};
#print Dumper $tt;
}
}
#my $str = $rows->[0]->[1];
#@node_ipm = split(' ',$str);
#p @node_ipm;
##p @ipm; #--test
