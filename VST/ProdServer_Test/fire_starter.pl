#!/usr/bin/env perl
#===============================================================================
#
#         FILE: fire_starter.pl
#
#        USAGE: ./fire_starter.pl  
#
#  DESCRIPTION: Скрипт для запуска node_requester.pl
#
#      OPTIONS: ---
# REQUIREMENTS: ---
#         BUGS: ---
#        NOTES: ---
#       AUTHOR: Tolmachev V.N. (), 
# ORGANIZATION: 
#      VERSION: 1.0
#      CREATED: 09.07.2019 16:57:05
#     REVISION: ---
#===============================================================================

use strict;
use warnings;
use utf8;
use POSIX ":sys_wait_h";

$|=1;
#my $cmd='./node_requester.pl';

#while (1){
my @cmd=( 
	'./node_requester.pl vld_adm_umasheva_d2c_p1_n1',
	'./node_requester.pl vld_bestujeva_d20c_p1_n1',
	'./node_requester.pl vld_ilicheva_d32c_p1_n1',
        './node_requester.pl vld_krasn_znam_d86c_p1_n1',
        './node_requester.pl vld_nekrasovsk_d76c_p1_n1',
	'./node_requester.pl vld_nekrasovsk_d76c3_p1_n1',      
	'./node_requester.pl vld_nekrasovsk_d76c6_p1_n1',
	'./node_requester.pl vld_okatovaya_d10c_p1_n1',   
	'./node_requester.pl vld_okeanskiy_d108c_p1_n1',  
	'./node_requester.pl vld_okeanskiy_d125c3_p1_n1', 
	'./node_requester.pl vld_russkaya_d65c_p1_n1',    
	'./node_requester.pl vld_saxalinskaya_d34c_p1_n1',
	'./node_requester.pl vld_100-letia_d143c_p1_n1',  
	'./node_requester.pl arsen_lomonos_d42c_p1_n1',     
	'./node_requester.pl artem_kirova_d25c_p1_n1',      
	'./node_requester.pl fokino_ysatogo_d8c_p1_n1',    
	'./node_requester.pl naxodka_gorkogo_d21c_p1_n1',   
	'./node_requester.pl naxodka_leningrad_d17ac_p1_n1',
	'./node_requester.pl naxodka_postisheva_d27c_p1_n1',
	'./node_requester.pl trud_svetlaya_d66_p1_n1',      
	'./node_requester.pl ussur_chicherina_d153c_p1_n1', 
	'./node_requester.pl ussur_krasnoznam_d129c_p1_n1' 
	);

sub fork_child{
my ($child_process_code)=@_;
my $pid = fork;
die "fork not running:$!\n" if !defined $pid;
print "running fork!\n";
return $pid if $pid!=0;
$child_process_code->();
exit;
}

my $i=0;
my @pid;
foreach my $comm (@cmd){
$pid[$i]=fork_child(sub{
exec"$comm" or die 'node not starting: $!\n';
print "starting node_requester $pid[$i]\n";
});
$i++;
}
foreach (@pid){waitpid $_,0;}

#fork ($cmd9);
#fork ($cmd10);
#fork ($cmd11);
#fork ($cmd12);
#fork ($cmd13);
#fork ($cmd14);
#waitpid ($cmd9, &WNOHANG);
#waitpid ($cmd10, &WNOHANG);
#waitpid ($cmd11, &WNOHANG);
#waitpid ($cmd12, &WNOHANG);
#waitpid ($cmd13, &WNOHANG);
#waitpid ($cmd14, &WNOHANG);
##system($cmd);
##exec("./node_requester.pl");
#}


