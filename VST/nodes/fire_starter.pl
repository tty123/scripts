#!/usr/bin/env perl
#===============================================================================
#
#         FILE: fire_starter.pl
#
#        USAGE: ./fire_starter.pl  
#
#  DESCRIPTION: Скрипт для запуска node_requester.pl
#
#      OPTIONS: ---
# REQUIREMENTS: ---
#         BUGS: ---
#        NOTES: ---
#       AUTHOR: Tolmachev V.N. (), 
# ORGANIZATION: 
#      VERSION: 1.0
#      CREATED: 09.07.2019 16:57:05
#     REVISION: ---
#===============================================================================

use strict;
use warnings;
use utf8;
use POSIX ":sys_wait_h";

$|=1;
#my $cmd='./node_requester.pl';

while (1){
my @cmd=('./node_requester_9.pl','./node_requester_10.pl','./node_requester_11.pl','./node_requester_12.pl','./node_requester_13.pl','./node_requester_14.pl');

sub fork_child{
my ($child_process_code)=@_;
my $pid = fork;
die "fork not running:$!\n" if !defined $pid;
print "running fork!\n";
return $pid if $pid!=0;
$child_process_code->();
exit;
}

my $i=0;
my @pid;
foreach my $comm (@cmd){
$pid[$i]=fork_child(sub{
exec"$comm" or die 'node not starting: $!\n';
print "starting node_requester $pid[$i]\n";
});
$i++;
}
foreach (@pid){waitpid $_,0;}

#fork ($cmd9);
#fork ($cmd10);
#fork ($cmd11);
#fork ($cmd12);
#fork ($cmd13);
#fork ($cmd14);
#waitpid ($cmd9, &WNOHANG);
#waitpid ($cmd10, &WNOHANG);
#waitpid ($cmd11, &WNOHANG);
#waitpid ($cmd12, &WNOHANG);
#waitpid ($cmd13, &WNOHANG);
#waitpid ($cmd14, &WNOHANG);
##system($cmd);
##exec("./node_requester.pl");
}


