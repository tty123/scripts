#!/usr/bin/env perl
#===============================================================================
#
#         FILE: create_node_requester.pl
#
#        USAGE: ./create_node_requester.pl  
#
#  DESCRIPTION: скрипт генерации файлов node_requester.pl для каждого узла
#
#      OPTIONS: ---
# REQUIREMENTS: ---
#         BUGS: ---
#        NOTES: ---
#       AUTHOR: Tolmachev V.N. (), 
# ORGANIZATION: 
#      VERSION: 1.0
#      CREATED: 10.07.2019 14:35:39
#     REVISION: ---
#===============================================================================

use strict;
use warnings;
use utf8;

my @node_numbers=(1..14);
foreach (@node_numbers) {
my $cmd = `cp node_requester.pl node_requester_$_.pl`;
}
