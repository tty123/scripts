#!/usr/bin/env perl
#===============================================================================
#
#         FILE: test_oids.pl
#
#        USAGE: ./test_oids.pl  
#
#  DESCRIPTION: пробивает OIDs для каждого коммутатора по 
#
#      OPTIONS: ---
# REQUIREMENTS: ---
#         BUGS: ---
#        NOTES: ---
#       AUTHOR: Tolmachev V.N. (), 
# ORGANIZATION: 
#      VERSION: 1.0
#      CREATED: 26.04.2019 14:08:27
#     REVISION: ---
#===============================================================================
use Socket;
use AnyEvent;
use AnyEvent::SNMP;
use Devel::Timer;
use strict;
use warnings;
use utf8;
use Net::SNMP;
use Math::Utils 'ceil';
use Data::Printer;
use Data::Dumper;

#==========================================================
my $timing = Devel::Timer->new();
$timing->mark('start programm');

$|=1; # отключаем буферизацию вывода
#################################################################
my $file_input='~/Scripts/VST/info/ip-sw_model'; #
my $file_oids = '';# файл с проверяемыми OIDs
my $file_output='OIDs-for-models.txt';
my $community = 'public';
#################################################################
my @ipm; # массив ip адресов полученный из файла
my %ip_mac; # : shared; # итоговый хэш, общий для всех тредов
my $ip_a;
my $OID_model_find='1.3.6.1.2.1.1.1.0'; #показать мак-адрес
my @oids : shared;
push (@oids, $OID_model_find);
#============================================
my %dlink2=(                           # D-Link DES-3200-28 DES-3200-28P/C1
	'sysDescr'                    => '1.3.6.1.2.1.1.1',   # название модели свитча
	'sysObjectID'                 => '1.3.6.1.2.1.1.2',   # VendorID
	'sysUpTime'                   => '1.3.6.1.2.1.1.3',   # время работы после последней перезагрузки
	'sysName'                     => '1.3.6.1.2.1.1.5',   # присвоенное пользователем имя
	'ifAdminStatus'               => '1.3.6.1.2.1.2.2.1.7', # массив значений по каждому сетевому интерфейсу
	'ifOperStatus'                => '1.3.6.1.2.1.2.2.1.8', # массив значений по каждому сетевому интерфейсу
	'ifInUcastPkts'               => '1.3.6.1.2.1.2.2.1.11', # массив значений по каждому сетевому интерфейсу
	'ifInNUcastPkts'              => '1.3.6.1.2.1.2.2.1.12', # массив значений по каждому сетевому интерфейсу
	'ifInErrors'                  => '1.3.6.1.2.1.2.2.1,13', # массив значений по каждому сетевому интерфейсу
	'ifOutUcastPkts'              => '1.3.6.1.2.1.2.2.1.11', # массив значений по каждому сетевому интерфейсу точка-точка
	'ifOutNUcastPkts'             => '1.3.6.1.2.1.2.2.1.12', # массив значений по каждому сетевому интерфейсу широковещат+мультикаст
	'ifOutErrors'                 => '1.3.6.1.2.1.2.2.1,13', # массив значений по каждому сетевому интерфейсу
	'entPhysicalSerialNum'        => '1.3.6.1.2.1.47.1.1.1.1.11', # строка с серийником
	'entPhysicalDescr'            => '1.3.6.1.2.1.47.1.1.1.1.2' , # (.*) 1 строка-модель устройства далее описание каждого порта 100мбит 10000 fiber copper
	'ifInMulticastPkts'           => '1.3.6.1.2.1.31.1.1.1.2', # (.*) количество мультикаст пакетов по  портам	
	'ifInBroadcastPkts'           => '1.3.6.1.2.1.31.1.1.1.3', # (.*) количество широковещат  пакетов по  портам	
	'ifOutMulticastPkts'          => '1.3.6.1.2.1.31.1.1.1.4', # (.*) количество мультикаст пакетов по всем портам	
	'ifOutBroadcastPkts'          => '1.3.6.1.2.1.31.1.1.1.5', # (.*) количество широковещат пакетов по всем портам
	#-- Cabel diagnostics --
	'swEtherCableDiagLinkStatus'  => '1.3.6.1.4.1.171.12.58.1.1.1.3', # (.*) состояние линка-соединения 	
	'swEtherCableDiagPair1Status' => '1.3.6.1.4.1.171.12.58.1.1.1.4', # (.*) состояние 1 пары {ok(0),open(1),short(2),open-short(3),crosstalk(4),unknown(5),count(6),no-cable(7),other(8)} 	
	'swEtherCableDiagPair2Status' => '1.3.6.1.4.1.171.12.58.1.1.1.5', # (.*) состояние 2 пары {ok(0),open(1),short(2),open-short(3),crosstalk(4),unknown(5),count(6),no-cable(7),other(8)} 	
	'swEtherCableDiagPair3Status' => '1.3.6.1.4.1.171.12.58.1.1.1.6', # (.*) состояние 3 пары {ok(0),open(1),short(2),open-short(3),crosstalk(4),unknown(5),count(6),no-cable(7),other(8)} 	
	'swEtherCableDiagPair4Status' => '1.3.6.1.4.1.171.12.58.1.1.1.7', # (.*) состояние 4 пары {ok(0),open(1),short(2),open-short(3),crosstalk(4),unknown(5),count(6),no-cable(7),other(8)} 	
	'swEtherCableDiagPair1Length' => '1.3.6.1.4.1.171.12.58.1.1.1.8', # (.*) примерная длина кабеля 1 пары в метрах (работает при длине не менее 2м) 	
	'swEtherCableDiagPair2Length' => '1.3.6.1.4.1.171.12.58.1.1.1.9', # (.*) примерная длина кабеля 2 пары в метрах (работает при длине не менее 2м) 	
	'swEtherCableDiagPair3Length' => '1.3.6.1.4.1.171.12.58.1.1.1.10', # (.*) примерная длина кабеля 3 пары в метрах (работает при длине не менее 2м) 	
	'swEtherCableDiagPair4Length' => '1.3.6.1.4.1.171.12.58.1.1.1.11', # (.*) примерная длина кабеля 4 пары в метрах (работает при длине не менее 2м) 
	#-- CPU, Port, RAM utilization --
	'agentCPUutilizationIn5sec'   => '1.3.6.1.4.1.171.12.1.1.6.1', # загрузка CPU в % за 5сек интервал
	'agentCPUutilizationIn1min'   => '1.3.6.1.4.1.171.12.1.1.6.2', # загрузка CPU в % за 1мин  интервал
	'agentCPUutilizationIn5min'   => '1.3.6.1.4.1.171.12.1.1.6.3', # загрузка CPU в % за 5мин интервал
	'agentPORTutilizationTX'      => '1.3.6.1.4.1.171.12.1.1.8.1.2', # (.*) текущая интенсивность отправляемых пакетов по порту
	'agentPORTutilizationRX'      => '1.3.6.1.4.1.171.12.1.1.8.1.3', # (.*) текущая интенсивность отправляемых пакетов по порту
	'agentPORTutilizationUtil'    => '1.3.6.1.4.1.171.12.1.1.8.1.4', # (.*) текущее процентное отношение статистики использования порта
	'agentDRAMutilization'        => '1.3.6.1.4.1.171.12.1.1.9.1.4', # (.*) загрузка оперативной памяти 0-100% 
	#-- Power, Fan, Temp --
	'swPowerUnitIndex'            => '1.3.6.1.4.1.171.12.11.1.6.1.1', # выдаёт 2 строки с id=1  источников питания  (осн., акк) 
	'swPowerID'                   => '1.3.6.1.4.1.171.12.11.1.6.1.2', # выдаёт 2 строки соответствие id - swPowerID.1.1 = 1 основной источник, swPowerID.1.2 = 2 аккум.
	'swPowerStatus'               => '1.3.6.1.4.1.171.12.11.1.6.1.3', # выдаёт 2 строки состояние по каждому источнику {other(0),lowVoltage(1),overCurrent(2),working(3),fail(4),connect(5),disconnect(6)}
	'swFanUnitIndex'              => '1.3.6.1.4.1.171.12.11.1.7.1.1', # выдаёт 2 строки для 2-х вентиляторов swFanUnitIndex.1.7 = 1, swFanUnitIndex.1.8 = 1
	'swFanID'                     => '1.3.6.1.4.1.171.12.11.1.7.1.2', # выдаёт 2 строки ID каждого вентилятора swFanID.1.7 = 7, swFanID.1.8 = 8
	'swFanStatus'                 => '1.3.6.1.4.1.171.12.11.1.7.1.3', # выдаёт 2 строки состояние каждого вентилятора {other(0),working(1),fail(2),speed-0(3),speed-low(4),speed-middle(5),speed-high(6)}
	'swTemperatureCurrent'        => '1.3.6.1.4.1.171.12.11.1.8.1.2', # информация с температурного датчика(ов) в градусах
	'agentSerialNumber'	      => '1.3.6.1.4.1.171.12.1.1.12', # серийный номер устройства
	#'agentFirmwareType'           => '1.3.6.1.4.1.171.12.1.1.13', # тип прошивки

);
#============================================
# Загружаем ip адреса
	open (FILE, $file_input) or die "can`t open file";
	while (defined (my $file_line = <FILE>)) {
		if ($file_line=~/\s(\d{1,3}[.]\d{1,3}[.]\d{1,3}[.]\d{1,3})\s/) {   # ip адрес обрамлён пробелами
	push(@ipm,$1);
		}
	}
	foreach (@ipm) {print "-=$_=-\n"}
	close FILE;
my @base_ipm=@ipm; # запоминаем начальное состояние массива адресов
#============================================

$timing->mark('create @ipm');

# по каждому адресу получаем мак и создаём хэш адрес-макa
# основной цикл выполнения потоков -------------------------------

my $ipmass=$#ipm; # текущий размер массива
my @tr_mac; # массив тредов
#------------------------------------------------------------------
my $num_th=300; # количество процессов на запуск
# разбираем массив на части, основной цикл обработки
#------------------------------------------------------------------

$timing->mark('mass ip divided for treads');
$timing->mark('run all treads');

#---------------------------------------------------------------------------- 

# ============================================================================

#-----------------------------------------------------------------------
# функция для треда
#-----------------------------------------------------------------------
		my @tt;
		my %th_hash;
		my $res;
MMM: while (@ipm){
my $cv = AnyEvent->condvar; 
$cv->begin;
for (my $i=0; $i < $num_th; $i++){
#while (@ipm)	{

		my $ip_=pop @ipm;
		unless ($ip_) {last};
	#	my $ip_='10.1.1.1';
		$cv->begin;
		#my $session = 
		Net::SNMP->session(
				-hostname      => $ip_,
				-community     => $community,
			       -translate     => [-octetstring => 0],
			      # -version       => 'snmpv2c',
			#	-timeout       => 1,
				-nonblocking => 1,
			     )->get_request(
				-varbindlist => \@oids,
			       # -delay => 1,
				-callback => sub {
			my $snmp_q=shift;
			my %result;
p $ip_;
	if (defined $snmp_q->var_bind_list()){
	foreach my $oi (@oids){ $result{$oi} = $snmp_q->var_bind_list()->{$oi}; } }
p %result ;
		my $rr;
		my @vv=values %result;
		if (@vv==0){@tt[0]='timeout'} else {@tt=values %result}; 
		if ($tt[0] eq 'timeout') {$rr='undefined';} 
			else {$rr = $tt[0]};
		$th_hash{$ip_}=$rr;
	print" thread ip = $ip_ model=$rr; \n";
	#	Net::SNMP->close();
			$cv->send(\%th_hash);	});
#snmp_dispatcher();
		#$cv->end;
		}
$cv->end; 
$res=$cv->recv;   #wait;
p $res;
}
p $res;
%ip_mac=%$res;
my @new_ipm =keys %ip_mac;

# @a - исходный массив 
# @b - модифицированый массив 
 
my %seen_a = (); 
my %seen_b = (); 
my @aonly = (); # уникальные элементы в исходном массиве 
 
foreach my $item (@new_ipm) { $seen_b{$item} = 1;} 
foreach my $item (@base_ipm) { $seen_a{$item} = 1;} 
 
foreach my $item (@base_ipm) { push(@aonly, $item) unless $seen_b{$item} } 
p @aonly;

if (@aonly) {@ipm=@aonly;
print "GOTO NEXT ITERATION !!!\n"; goto MMM; };


# ===================================================================
print "Find  $ipmass IP address.\n";
print "Save to file: $file_output\n";
# выводим хэш в файл 
	open (FILE,">$file_output") or die " can`t open file";

	while ( my ($ip_key, $ip_value) = each(%ip_mac) ) {
#	print  "$ip_key    $ip_value\n";
#вставляем в мак пробелы или чтонибудь другое
	#$ip_value=~s/(..)(..)(..)(..)(..)(..)/$1 $2 $3 $4 $5 $6/;
	print FILE "        ";
	printf FILE "%-20s", $ip_key;
	print  FILE "        ";
	printf FILE  "%17s", "$ip_value\n";
	 }

	close(FILE);

$timing->mark('save to file, end programm');
$timing->report(sort_by => 'interval');


