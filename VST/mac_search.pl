#!/usr/bin/env perl
#===============================================================================
#
#         FILE: oid-search.pl
#
#        USAGE: ./oid-search.pl  
#
#  DESCRIPTION: ищем мак адреса коммутаторов по snmp, задаем входной файл с ip
#               адресами и в  выходной файл получаем IP + MAC
#      OPTIONS: ---
# REQUIREMENTS: ---
#         BUGS: ---
#        NOTES: ---
#       AUTHOR: Tolmachev V.N. (), 
# ORGANIZATION: Vladlink.ru 
#      VERSION: 1.0
#      CREATED: 13.02.2019 08:56:38
#     REVISION: ---
#===============================================================================

use strict;
use warnings;
use utf8;
use Net::SNMP;
use Data::Printer;

$|=1; # отключаем буферизацию вывода
#----------------------------------------------------------------
my $file_input='~/Scripts/VST/info/ip-sw_model'; #
my $file_output='oid.txt';
my $community = 'public';
#----------------------------------------------------------------
my $val;
my @ipm; # массив ip адресов полученный из файла
my %ip_mac;

my $OID_mac_find='1.3.6.1.2.1.17.1.1.0'; #показать мак-адрес
my @oids=();
my @tt=();
my $kt;
$tt[0]='';

my %dlink2=(                           # D-Link DES-3200-28 DES-3200-28P/C1
	'sysDescr'                    => '1.3.6.1.2.1.1.1',   # название модели свитча
	'sysObjectID'                 => '1.3.6.1.2.1.1.2',   # VendorID
	'sysUpTime'                   => '1.3.6.1.2.1.1.3',   # время работы после последней перезагрузки
	'sysName'                     => '1.3.6.1.2.1.1.5',   # присвоенное пользователем имя
	'ifAdminStatus'               => '1.3.6.1.2.1.2.2.1.7', # массив значений по каждому сетевому интерфейсу
	'ifOperStatus'                => '1.3.6.1.2.1.2.2.1.8', # массив значений по каждому сетевому интерфейсу
	'ifInUcastPkts'               => '1.3.6.1.2.1.2.2.1.11', # массив значений по каждому сетевому интерфейсу
	'ifInNUcastPkts'              => '1.3.6.1.2.1.2.2.1.12', # массив значений по каждому сетевому интерфейсу
	'ifInErrors'                  => '1.3.6.1.2.1.2.2.1,13', # массив значений по каждому сетевому интерфейсу
	'ifOutUcastPkts'              => '1.3.6.1.2.1.2.2.1.11', # массив значений по каждому сетевому интерфейсу точка-точка
	'ifOutNUcastPkts'             => '1.3.6.1.2.1.2.2.1.12', # массив значений по каждому сетевому интерфейсу широковещат+мультикаст
	'ifOutErrors'                 => '1.3.6.1.2.1.2.2.1,13', # массив значений по каждому сетевому интерфейсу
	'entPhysicalSerialNum'        => '1.3.6.1.2.1.47.1.1.1.1.11', # строка с серийником
	'entPhysicalDescr'            => '1.3.6.1.2.1.47.1.1.1.1.2' , # (.*) 1 строка-модель устройства далее описание каждого порта 100мбит 10000 fiber copper
	'ifInMulticastPkts'           => '1.3.6.1.2.1.31.1.1.1.2', # (.*) количество мультикаст пакетов по  портам	
	'ifInBroadcastPkts'           => '1.3.6.1.2.1.31.1.1.1.3', # (.*) количество широковещат  пакетов по  портам	
	'ifOutMulticastPkts'          => '1.3.6.1.2.1.31.1.1.1.4', # (.*) количество мультикаст пакетов по всем портам	
	'ifOutBroadcastPkts'          => '1.3.6.1.2.1.31.1.1.1.5', # (.*) количество широковещат пакетов по всем портам
	#-- Cabel diagnostics --
	'swEtherCableDiagLinkStatus'  => '1.3.6.1.4.1.171.12.58.1.1.1.3', # (.*) состояние линка-соединения 	
	'swEtherCableDiagPair1Status' => '1.3.6.1.4.1.171.12.58.1.1.1.4', # (.*) состояние 1 пары {ok(0),open(1),short(2),open-short(3),crosstalk(4),unknown(5),count(6),no-cable(7),other(8)} 	
	'swEtherCableDiagPair2Status' => '1.3.6.1.4.1.171.12.58.1.1.1.5', # (.*) состояние 2 пары {ok(0),open(1),short(2),open-short(3),crosstalk(4),unknown(5),count(6),no-cable(7),other(8)} 	
	'swEtherCableDiagPair3Status' => '1.3.6.1.4.1.171.12.58.1.1.1.6', # (.*) состояние 3 пары {ok(0),open(1),short(2),open-short(3),crosstalk(4),unknown(5),count(6),no-cable(7),other(8)} 	
	'swEtherCableDiagPair4Status' => '1.3.6.1.4.1.171.12.58.1.1.1.7', # (.*) состояние 4 пары {ok(0),open(1),short(2),open-short(3),crosstalk(4),unknown(5),count(6),no-cable(7),other(8)} 	
	'swEtherCableDiagPair1Length' => '1.3.6.1.4.1.171.12.58.1.1.1.8', # (.*) примерная длина кабеля 1 пары в метрах (работает при длине не менее 2м) 	
	'swEtherCableDiagPair2Length' => '1.3.6.1.4.1.171.12.58.1.1.1.9', # (.*) примерная длина кабеля 2 пары в метрах (работает при длине не менее 2м) 	
	'swEtherCableDiagPair3Length' => '1.3.6.1.4.1.171.12.58.1.1.1.10', # (.*) примерная длина кабеля 3 пары в метрах (работает при длине не менее 2м) 	
	'swEtherCableDiagPair4Length' => '1.3.6.1.4.1.171.12.58.1.1.1.11', # (.*) примерная длина кабеля 4 пары в метрах (работает при длине не менее 2м) 
	#-- CPU, Port, RAM utilization --
	'agentCPUutilizationIn5sec'   => '1.3.6.1.4.1.171.12.1.1.6.1', # загрузка CPU в % за 5сек интервал
	'agentCPUutilizationIn1min'   => '1.3.6.1.4.1.171.12.1.1.6.2', # загрузка CPU в % за 1мин  интервал
	'agentCPUutilizationIn5min'   => '1.3.6.1.4.1.171.12.1.1.6.3', # загрузка CPU в % за 5мин интервал
	'agentPORTutilizationTX'      => '1.3.6.1.4.1.171.12.1.1.8.1.2', # (.*) текущая интенсивность отправляемых пакетов по порту
	'agentPORTutilizationRX'      => '1.3.6.1.4.1.171.12.1.1.8.1.3', # (.*) текущая интенсивность отправляемых пакетов по порту
	'agentPORTutilizationUtil'    => '1.3.6.1.4.1.171.12.1.1.8.1.4', # (.*) текущее процентное отношение статистики использования порта
	'agentDRAMutilization'        => '1.3.6.1.4.1.171.12.1.1.9.1.4', # (.*) загрузка оперативной памяти 0-100% 
	#-- Power, Fan, Temp --
	'swPowerUnitIndex'            => '1.3.6.1.4.1.171.12.11.1.6.1.1', # выдаёт 2 строки с id=1  источников питания  (осн., акк) 
	'swPowerID'                   => '1.3.6.1.4.1.171.12.11.1.6.1.2', # выдаёт 2 строки соответствие id - swPowerID.1.1 = 1 основной источник, swPowerID.1.2 = 2 аккум.
	'swPowerStatus'               => '1.3.6.1.4.1.171.12.11.1.6.1.3', # выдаёт 2 строки состояние по каждому источнику {other(0),lowVoltage(1),overCurrent(2),working(3),fail(4),connect(5),disconnect(6)}
	'swFanUnitIndex'              => '1.3.6.1.4.1.171.12.11.1.7.1.1', # выдаёт 2 строки для 2-х вентиляторов swFanUnitIndex.1.7 = 1, swFanUnitIndex.1.8 = 1
	'swFanID'                     => '1.3.6.1.4.1.171.12.11.1.7.1.2', # выдаёт 2 строки ID каждого вентилятора swFanID.1.7 = 7, swFanID.1.8 = 8
	'swFanStatus'                 => '1.3.6.1.4.1.171.12.11.1.7.1.3', # выдаёт 2 строки состояние каждого вентилятора {other(0),working(1),fail(2),speed-0(3),speed-low(4),speed-middle(5),speed-high(6)}
	'swTemperatureCurrent'        => '1.3.6.1.4.1.171.12.11.1.8.1.2', # информация с температурного датчика(ов) в градусах
	'agentSerialNumber'	      => '1.3.6.1.4.1.171.12.1.1.12', # серийный номер устройства
	#'agentFirmwareType'           => '1.3.6.1.4.1.171.12.1.1.13', # тип прошивки

);
push (@oids, $OID_mac_find);

# создаём массив адресов из файла
	open (FILE, $file_input) or die "can`t open file";
	while (defined (my $file_line = <FILE>)) {
		if ($file_line=~/\s(\d{1,3}[.]\d{1,3}[.]\d{1,3}[.]\d{1,3})\s/){
	push(@ipm,$1);
		}
	}
	foreach (@ipm) {print "$_\n"}
	close FILE;

		# блок индикатора загрузки
		my $mass_l=$#ipm+1;
		print "----------------------------\n";
		print "Find $mass_l ip.\n";
		print "Starting  SNMP send-request: \n"; 
		my $u_count=0; # подсчет неудачных запросов мак адреса
		my $ig=0; 
		my $igg=50; # максимальное  количество звёздочек на строку 
		my $var=$igg;
		my $z;

# по каждому адресу получаем мак и создаём хэш адрес-макa
	foreach (@ipm){
	my ($session, $error) = Net::SNMP->session(
                        -hostname      => $_,
                        -community     => $community,
                        -translate     => [-octetstring => 0],
                        -version       => 'snmpv2c',
			-timeout       => 1,
                     ); 
	if (!defined $session) {
	printf "ERROR: %s.\n", $error; 
	exit 1;}

	my $result = $session->get_request(
			-varbindlist => \@oids,
	);
	
#	my $rr;
#	if ((values %{$result})==0){@tt[0]='timeout'} else {@tt=values %{$result}}; 
#	if ($tt[0] eq 'timeout') {$rr='undefined'; $u_count++;} 
#		else {$rr=unpack("H*", $tt[0])};
#	$ip_mac{$_}=$rr;
#	$session->close();
# индикатор получения мак адреса

p $result;

	}
print "\n----------------------------\n";
print "Saccesful requests: ".($mass_l-$u_count)."\n";
print "Undefined requests: ".$u_count."\n";
print "----------------------------\n";
print "Save to file: $file_output\n";
# выводим хэш в файл 
	open (FILE,">$file_output") or die " can`t open file";

	while ( my ($ip_key, $ip_value) = each(%ip_mac) ) {
#	print FILE "$ip_key    $ip_value\n";
#вставляем в мак пробелы или чтонибудь другое
	$ip_value=~s/(..)(..)(..)(..)(..)(..)/$1 $2 $3 $4 $5 $6/;
	print FILE "        ";
	printf FILE "%-20s", $ip_key;
	print FILE "        ";
	printf FILE "%17s", "$ip_value\n";
	 }

	close(FILE);

# сортировка массива

