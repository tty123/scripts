Структура таблиц ClickHouse сервиса igator

create database igator_db;

create table igator_db.dataset (time DateTime, node String, ip String, data String, condition String) engine = MergeTree PARTITION BY time ORDER BY (time, node, ip) SETTINGS index_granularity = 8192;

create table igator_db.iptbl (id String, ips String, up_date Date) engine = ReplacingMergeTree(up_date, id, 8192);

create table igator_db.node_state (time  DateTime, node  String, state String) engine = MergeTree PARTITION BY time ORDER BY (time) SETTINGS index_granularity = 8192;

create table igator_db.nodes_ips (time Date, node String, ips String) engine = ReplacingMergeTree(time, node, 8192);




Полезные SQL

Выбрать данные таблиц time, соответствующие временному диапазону:
select igator_db.dataset.time from igator_db.dataset  where igator_db.dataset.time between toDateTime(1567120954) and toDateTime(1567121954)

Выбрать несколько полей из таблицы с условиями:
select igator_db.dataset.ip, igator_db.dataset.data, igator_db.dataset.time from igator_db.dataset  where (igator_db.dataset.time between toDateTime(1565120954) and toDateTime(1567121954)) and igator_db.dataset.ip = '10.240.73.24'
Тоже самое на алиасах:
 select igator_db.dataset.ip i, igator_db.dataset.data d, igator_db.dataset.time t from igator_db.dataset  where (t between toDateTime(1565120954) and toDateTime(1567121954)) and i = '10.240.73.24'

Вывести с упоряочиванием по полю:
select igator_db.dataset.time t from igator_db.dataset  where (t between toDateTime(1565120954) and toDateTime(1567121954)) and igator_db.dataset.ip = '10.240.73.44' order by t asc

select igator_db.dataset.time t from igator_db.dataset  where (t between toDateTime('2019-08-29 12:10:00') and toDateTime('2019-08-29 23:30:00')) and igator_db.dataset.ip = '10.240.73.44' order by t asc





