#!/usr/bin/env perl
#===============================================================================
#
#         FILE: test_substr.pl
#
#        USAGE: ./test_substr.pl  
#
#  DESCRIPTION: 
#
#      OPTIONS: ---
# REQUIREMENTS: ---
#         BUGS: ---
#        NOTES: ---
#       AUTHOR: Tolmachev V.N. (), 
# ORGANIZATION: 
#      VERSION: 1.0
#      CREATED: 11.03.2020 12:47:02
#     REVISION: ---
#===============================================================================

use strict;
use warnings;
use utf8;
use 5.010;

my $str= "020306ff1abc";
say "CRC=".substr($str,-4);
say "DATA=".substr($str,6,-4);
