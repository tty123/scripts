#!/usr/bin/env perl
#===============================================================================
#
#         FILE: test_do_file.pl
#
#        USAGE: ./test_do_file.pl  
#
#  DESCRIPTION: 
#
#      OPTIONS: ---
# REQUIREMENTS: ---
#         BUGS: ---
#        NOTES: ---
#       AUTHOR: Tolmachev V.N. (), 
# ORGANIZATION: 
#      VERSION: 1.0
#      CREATED: 16.07.2020 17:44:49
#     REVISION: ---
#===============================================================================

use strict;
use warnings;
use utf8;
use Data::Printer;
use 5.010;

my %templ = %{do "./test_do_file.conf"};

p %templ;
while  (my ($key, $value) = each %templ){
	say "$key = $value";

}
