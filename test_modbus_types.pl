#!/usr/bin/env perl
#===============================================================================
#
#         FILE: test_modbus_types.pl
#
#        USAGE: ./test_modbus_types.pl  
#
#  DESCRIPTION: перевод чисел из десятичной в двоичную систему, 
#  		преобразования типов для modbus переменных 
#
#      OPTIONS: ---
# REQUIREMENTS: ---
#         BUGS: ---
#        NOTES: ---
#       AUTHOR: Tolmachev V.N. (), 
# ORGANIZATION: 
#      VERSION: 1.0
#      CREATED: 10.02.2020 11:34:36
#     REVISION: ---
#===============================================================================

use strict;
use warnings;
use utf8;
use Data::Printer;
use Data::Dumper;
use Math::BigFloat;
use Data::IEEE754 qw( pack_float_be unpack_float_be );
#use Data::IEEE754::Tools qw/:convertFromInternalBinaryString :ulp/;
use 5.010; # for say

my $dec = 1000;
my $bin = sprintf ("%b", $dec);
print "$bin\n";
print Dumper $bin;
my @bits = split(//, $bin); # переводим бинарное число в битовый массив
p @bits;

my $dec1 = 65535;
my $dec2 = 17543;

my $bin1 = sprintf ("%b", $dec1); # получаем бинарный вид числа
say "Dec  $dec1 = $bin1";
my $bin2 = sprintf ("%b", $dec2);
say "Dec  $dec2 = $bin2";
my $bin_summ = $bin2.$bin1;
p $bin_summ;
#my $dec_summ = oct('0b'.$bin_summ); # oct переводит не только из восьмеричной системы, но и из двоичной если перед числом '0b'
#print $dec_summ."\n";
#say $dec_summ;
my $fl_x = Math::BigFloat->as_bin($bin_summ);
say "Перевод в вещественный тип";
say $fl_x;

my $data_bin = '01000001101101011011111110001000';
say unpack_float_be($data_bin);
#print convertFromInternalBinaryString("$data_bin");
say scalar(pack_float_be(26,5)); 
#################################################################################################
#################################################################################################

#########################################################################################################
# фукнкция преобразования десятичного значения из 1 регистра modbus в значение целого типа int со знаком
#########################################################################################################
sub prep_int{
my $dec=shift;
my $res; #сюда запишем отрицательное число
my $bin = sprintf ("%b", $dec); # получаем бинарный вид числа
#my $str = unpack("B16", pack("N", $dec));
			while (length($bin)<16){
				$bin="0".$bin;
			}
			say "дополнили нулями";
if (substr($bin,0,1) eq '0')
	{ return $dec;}
	else{
		my @bits = split(//, $bin); # переводим бинарное число в битовый массив
		my $i = 0;
		foreach (@bits){            # инверсия
			if ($_){
				$bits[$i]=0;
			}
			else {
				$bits[$i]=1;
			}
			$i++;
		}
		say "делаем массив";
		p @bits;
		$i =0;
		my $cur = 1;
		# далее прибавляем 1
	do {
		say "итерация $i";
		p @bits;
		if ($bits[($#bits)-$i]==1){
			if ($cur==1){
				$bits[($#bits)-$i]=0;
				$cur=1;
			}
		}else {
			if ($cur==1){
				$bits[($#bits)-$i]=1;
				$cur=0;
			}
		}
			$i++;
			if ($i==16){$cur=0;}
			say "\$cur = $cur";
	} while $cur;
		$res = join ('',@bits);
		my $dec_res = oct('0b'.$res);
		if ($dec_res != 0){$dec_res='-'.$dec_res;}
	return $dec_res; 
	}
}

#########################################################################################################
# фукнкция преобразования десятичного значения из 2 регистра modbus в значение целого типа long со знаком
#########################################################################################################
sub prep_long{
my ($dec1, $dec2) = @_;
my $res=''; # сюда сложим результат
my $bin1 = sprintf ("%b", $dec1);
my $bin2 = sprintf ("%b", $dec2);
	# дополняем бинарники нулями
			while (length($bin1)<16){
				$bin1="0".$bin1;
			}

			while (length($bin2)<16){
				$bin2="0".$bin2;
			}
my $bin_union = $bin2.$bin1;
if (substr($bin_union,0,1) eq '0')
	{ return  oct('0b'.$bin_union);}
	else{
		my @bits = split(//, $bin_union); # переводим бинарное число в битовый массив
		my $i = 0;
		foreach (@bits){            # инверсия
			if ($_){
				$bits[$i]=0;
			}
			else {
				$bits[$i]=1;
			}
			$i++;
		}
		p @bits;
		$i =0;
		my $cur = 1;
		# далее прибавляем 1
	do {
		say "итерация $i";
		p @bits;
		if ($bits[($#bits)-$i]==1){
			if ($cur==1){
				$bits[($#bits)-$i]=0;
				$cur=1;
			}
		}else {
			if ($cur==1){
				$bits[($#bits)-$i]=1;
				$cur=0;
			}
		}
			$i++;
			if ($i==32){$cur=0;}
	} while $cur;
		$res = join ('',@bits);
		my $dec_res = oct('0b'.$res);
		if ($dec_res != 0){$dec_res='-'.$dec_res;}
	return $dec_res; 
	}
}

#########################################################################################################
# фукнкция преобразования массива значений из 1 регистра modbus в значение типа bool 
#########################################################################################################
sub prep_bool{
return @_[0];
}

#########################################################################################################
# функция преобразования десятичных значений из 2 регистров modbus в значения вещественного типа со знаком
#########################################################################################################
sub prep_float {
my ($dec1, $dec2) = @_;
my $res=''; # сюда сложим результат
my $int_part_num; # целая часть числа
my $fract_part_num; # дробная часть числа

my $bin1 = sprintf ("%b", $dec1);
my $bin2 = sprintf ("%b", $dec2);
	# дополняем бинарники нулями
			while (length($bin1)<16){
				$bin1="0".$bin1;
			}

			while (length($bin2)<16){
				$bin2="0".$bin2;
			}
my $bin_union = $bin2.$bin1;
say "float number binary  $bin_union";
if ((substr $bin_union,0,1) eq '1') {$res='-';}  # установка знака числа
my $mantiss_bin = substr $bin_union,1,8;
my $num_mantiss = bin2dec($mantiss_bin)-127; # максимальная степень целого числа
my @num_fragment = split(//, substr($bin_union,9));
unshift (@num_fragment,'1'); # дополняем наше 2-ичное число сокращённой 1
p @num_fragment;
if ($num_mantiss == 0){ 
	$int_part_num = 0;
	}
	else{
		my $l_res=0;
		foreach (@num_fragment){
			p $l_res;
		$l_res = $l_res+$_*(2**$num_mantiss);
		$num_mantiss--;
		p $num_mantiss;
		if ($num_mantiss == 0){
			$int_part_num = $l_res;
			$l_res =0;
			}
		$fract_part_num = $l_res;
		}
	}
	my $num = $int_part_num+$fract_part_num;
return $res = "$res$num";
#my $int_part_num = bin2dec('1'.substr($bin_union,9,$num_mantiss));
#my $fract_part_num = bin2dec(substr($bin_union,9+$num_mantiss,23-$num_mantiss));
#return $res ="$res"."$int_part_num"."."."$fract_part_num";
#my @bits = split(//, $bin_union); # переводим бинарное число в битовый массив
#if (@bits[0])
}

sub dec2bin {
my $str = unpack("B32", pack("N", shift));
return $str;
}

sub bin2dec {
return unpack("N", pack("B32", substr("0" x 32 . shift, -32)));
}



#print prep_float (0,49776)."\n";


sub dec2bin16 {
my $str = unpack("B16", pack("V", shift));
return $str;
}
#print dec2bin16(65535);
#print "\n";
#print unpack("N", ((~(dec2bin16(65535)))&1));
#print "\n";
##vec
#my $decc=dec2bin16(65535);
#my $one=1;
#my $ress= ~$decc;
#$ress|=$one;
#print "\$res = $ress \n";
#print ~0666;

# значения берутся из вывода модуля Device::Modbus::TCP::Client тестовой программы для теплового узла
say prep_float(26252,17542); # 22.257 температура
#say prep_int (65435); # -1
#say prep_long(16962,0); # 1800626877
#my @mass=(1,0,0,0,0,0,0,0); # true
#say prep_bool(@mass);
#
sub bin2decf {
return unpack("F", pack("B32", substr("0" x 32 . shift, -32)));
}

#printf("%f",'11000010111011010100000000000000');
