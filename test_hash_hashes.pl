#!/usr/bin/env perl
#===============================================================================
#
#         FILE: tes_hash_hashes.pl
#
#        USAGE: ./tes_hash_hashes.pl  
#
#  DESCRIPTION: проверка работы Хэша хэшей 
#
#      OPTIONS: ---
# REQUIREMENTS: ---
#         BUGS: ---
#        NOTES: ---
#       AUTHOR: Tolmachev V.N. (), 
# ORGANIZATION: 
#      VERSION: 1.0
#      CREATED: 03.06.2019 16:43:00
#     REVISION: ---
#===============================================================================

use strict;
use warnings;
use utf8;

use Data::Printer;
my %add_hash = (
		'oid7' => 'undef',
		'oid8' => 'undef',
		);
my %data_hash;
my @ipm = ('10.240.1.1','10.240.1.2','10.240.1.3','10.240.1.4','10.240.1.5');
my @descrm = ('descr1','descr2','descr3','descr4');
my @oidm = ('oid1','oid2','oid3','oid4','oid5','oid6');

while (@ipm){
	my $ip = pop (@ipm);
	foreach my $des (@descrm){
			foreach (@oidm){
			$data_hash{$ip}{$des}{$_} = 'result';
			while (my ($key, $value) = each %add_hash ){
			$data_hash{$ip}{$des}{$key}=$value;
						}
					}
				}
}
p %data_hash;





