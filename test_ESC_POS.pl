#!/usr/bin/env perl
#===============================================================================
#
#         FILE: test_ESC_POS.pl
#
#        USAGE: ./test_ESC_POS.pl  
#
#  DESCRIPTION: 
#
#      OPTIONS: ---
# REQUIREMENTS: ---
#         BUGS: ---
#        NOTES: ---
#       AUTHOR: Tolmachev V.N. (), 
# ORGANIZATION: 
#      VERSION: 1.0
#      CREATED: 19.06.2020 19:01:32
#     REVISION: ---
#===============================================================================

use strict;
use warnings;
use utf8;
use Printer::ESCPOS;
 
my $path = '/dev/usb/lp0';
my $device = Printer::ESCPOS->new(
    driverType     => 'File',
    deviceFilePath => $path,
);
 
$device->printer->bold(1);
$device->printer->text("Bold Text\n");
$device->printer->bold(0);
$device->printer->text("Bold Text Off\n");
$device->printer->cutPaper();
$device->printer->print();
