#!/usr/bin/env perl
#===============================================================================
#
#         FILE: test_push.pl
#
#        USAGE: ./test_push.pl  
#
#  DESCRIPTION: 
#
#      OPTIONS: ---
# REQUIREMENTS: ---
#         BUGS: ---
#        NOTES: ---
#       AUTHOR: Tolmachev V.N. (), 
# ORGANIZATION: 
#      VERSION: 1.0
#      CREATED: 01.04.2020 11:48:35
#     REVISION: ---
#===============================================================================

use strict;
use warnings;
use utf8;
use 5.010;
use DDP;
use open qw(:std :utf8); 

my @a;
my %test=(
	'arr1'=>[],   # не подходят (''), \(''), @a
);

push (@{$test{'arr1'}}, 12); # только фигурные скобки, без них или с круглыми не прокатывает
push (@{$test{'arr1'}}, 13); # 
push (@{$test{'arr1'}}, 14);

p %test;

say "печатаем первый элемент массива $test{arr1}->[0]"
