#!/usr/bin/env perl
#===============================================================================
#
#         FILE: sort.pl
#
#        USAGE: ./sort.pl  
#
#  DESCRIPTION: Сортировка, исследование алгоритма
#
#      OPTIONS: ---
# REQUIREMENTS: ---
#         BUGS: ---
#        NOTES: ---
#       AUTHOR: Tolmachev V.N. (), 
# ORGANIZATION: 
#      VERSION: 1.0
#      CREATED: 14.02.2019 18:46:17
#     REVISION: ---
#===============================================================================

use strict;
use warnings;
use utf8;

my @mass=(11,2,3,4,8,7,15,6,23,9);
my @sort_mass=();
foreach (@mass){print "$_\n"};
print "\n";

my $count=0;
my $mass_ln=$#mass;

my $i;
for ($i=0; $i<$mass_ln;$i++){

	for (my $k=0; $k<$mass_ln; $k++){
		if ($mass[$k] > $mass[$k+1]){
			my $var = $mass[$k];
			$mass[$k]=$mass[$k+1];
			$mass[$k+1]=$var;
		};
	}
}	


foreach (@mass){print "$_ \n"};
print "\n";
