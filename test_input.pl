#!/usr/bin/env perl 
#===============================================================================
#
#         FILE: test_input.pl
#
#        USAGE: ./test_input.pl  
#
#  DESCRIPTION: тестирование ввода с клавиатуры 
#
#      OPTIONS: ---
# REQUIREMENTS: ---
#         BUGS: ---
#        NOTES: ---
#       AUTHOR: Tolmachev V.N. (), 
# ORGANIZATION: 
#      VERSION: 1.0
#      CREATED: 26.03.2020 10:23:32
#     REVISION: ---
#===============================================================================

use strict;
use warnings;
use utf8;
use open qw(:std :utf8);
use 5.010;


print "Введите значение A = "; 
chomp( my $a =<>);
if ($a eq 'a'){say 'OK'}else{say 'Not OK'}
say $a;

