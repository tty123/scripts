#!/usr/bin/env perl
#===============================================================================
#
#         FILE: test_pack.pl
#
#        USAGE: ./test_pack.pl  
#
#  DESCRIPTION: тестирование функций puck, unpuck
#
#      OPTIONS: ---
# REQUIREMENTS: ---
#         BUGS: ---
#        NOTES: ---
#       AUTHOR: Tolmachev V.N. (), 
# ORGANIZATION: 
#      VERSION: 1.0
#      CREATED: 12.03.2020 16:21:01
#     REVISION: ---
#===============================================================================

use strict;
use warnings;
use utf8;
use 5.010;
use DDP;

#---- Преобразование hex строки в ASCII строку -------
my $str="525553"; #hex

say unpack ("A*", pack("H*",$str)); # RUS
#------------------------------------------------------

#---- Разбиваем строку на байты в массив по 2 символа ---
$str = '0000000000F0';
my @conv = unpack ('(a[2])*', $str);
p @conv; 
#-------------------------------------------------------

