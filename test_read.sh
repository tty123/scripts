#! /bin/bash
#
# test_read.sh
# Copyright (C) 2020 Tolmachev V.N. <>
#
# Distributed under terms of the MIT license.
#


#echo "All components install complete." 
#read -p  "Starting service section deployment? (y/n):" -n 1 -r
#if [[ $REPLY=~^[Nn]$ ]]
#		then
#	        echo 'install script interrrupt!\n'
#		        exit 1
#			   fi  
#
read -p "Are you sure? " -n 1 -r
echo    # (optional) move to a new line
if [[ ! $REPLY =~ ^[Yy]$ ]]
then
	    exit 1
    fi
