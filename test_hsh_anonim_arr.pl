#!/usr/bin/env perl
#===============================================================================
#
#         FILE: test_hsh_anonim_arr.pl
#
#        USAGE: ./test_hsh_anonim_arr.pl  
#
#  DESCRIPTION: 
#
#      OPTIONS: ---
# REQUIREMENTS: ---
#         BUGS: ---
#        NOTES: ---
#       AUTHOR: Tolmachev V.N. (), 
# ORGANIZATION: 
#      VERSION: 1.0
#      CREATED: 07.02.2020 09:08:35
#     REVISION: ---
#===============================================================================

use strict;
use warnings;
use utf8;
use Data::Printer;
use Data::Dumper;

my %hsh = (
		'test' => ['0', 'one', '234'], # здесь идёт присвоение 2 массивов, общий, а в нём массив с нашими значениями
	);
my %hsh1 = (
		'test' => [['0', 'one', '234'],['456','579']], # здесь идёт присвоение 2 массивов, общий, а в нём массив с нашими значениями
	);	
my %hsh2 = (
		'test' => "keks",
	);
	p %hsh2;
	while (my ($key, @arr) = each %hsh1){
	
	p $key;
	p @arr;
	print $arr[0][1]."\n";
	}
