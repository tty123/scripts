#!/usr/bin/env perl
#===============================================================================
#
#         FILE: test_logical_bit.pl
#
#        USAGE: ./test_logical_bit.pl  
#
#  DESCRIPTION: 
#
#      OPTIONS: ---
# REQUIREMENTS: ---
#         BUGS: ---
#        NOTES: ---
#       AUTHOR: Tolmachev V.N. (), 
# ORGANIZATION: 
#      VERSION: 1.0
#      CREATED: 27.04.2020 17:10:42
#     REVISION: ---
#===============================================================================

use strict;
use warnings;
use utf8;
use 5.010;
use open qw(:std :utf8);
use Data::Printer;

my $var1=0xa;# 01010
my $var2=0x6;#  0110
my $var3= $var1&$var2;
say "побитовое умножение  $var1 & $var2 =".$var3;
$var3= $var1|$var2;
say "побитовое сложение  $var1 | $var2 =".$var3;
say "инверсия ~$var2 =".~$var2; # в данной ситуации надо учитывать, какими размер ячеки данных оперирует Perl в 64bit системе = 64бит в 32 = 32

say "__отработока логического побитового умножения для CCNET.pm_______________________________";
my $SET_BILLS = hex '0038FC';   # аргумент "выбор банкнот" для команды cmd_enable_bill_types
my $SET_BILLS_SEC = hex '0038FC'; # аргумент "выбор усиленной проверки банкнот"  для команды cmd_enable_bill_types
my $str = $SET_BILLS&$SET_BILLS_SEC;
my $bin_str = sprintf( "%b", $str);
say $bin_str;
my @arr = split (//, $bin_str);
@arr= reverse @arr;
p @arr;
