#
FROM perl:5.28
COPY ./test_valid_ip.pl /usr/src/myapp/
RUN apt update && apt install -y build-essential && cpan -i Data::Printer  JSON  DBI
WORKDIR /usr/src/myapp/
CMD [ "perl", "./test_valid_ip.pl" ]
