#!/usr/bin/env perl
#===============================================================================
#
#         FILE: test_ping.pl
#
#        USAGE: ./test_ping.pl  
#
#  DESCRIPTION: тестируем модуль Net::Ping , для пинговалки свитча
#
#      OPTIONS: ---
# REQUIREMENTS: ---
#         BUGS: ---
#        NOTES: ---
#       AUTHOR: Tolmachev V.N. (), 
# ORGANIZATION: 
#      VERSION: 1.0
#      CREATED: 18.12.2019 14:16:41
#     REVISION: ---
#===============================================================================

use strict;
use warnings;
use utf8;

use Net::Ping;
use Data::Dumper;
use Data::Printer;
#use Time::HiRes;

my $my_addr = "10.240.41.227";
#$p = Net::Ping->new();
#print "$host is alive.\n" if $p->ping($host);
#$p->close();
# 
my $p = Net::Ping->new("icmp");
    print "NOT " unless my @tt=$p->ping($my_addr, 0.5, "ipv4"); # время на пингование 1сек
    print "reachable.\n";
my @res=$p->icmp_result;
#my $message=$p->message_type();
my $ping_time=$p->time;
#$p->hires(1);
print "+++++++++++++++++++++++++++++\n";
print Dumper $p;
print "+++++++++++++++++++++++++++++\n";
p @tt;
print "+++++++++++++++++++++++++++++\n";
p @res;
print "+++++++++++++++++++++++++++++\n";
#p $ping_time;
print "+++++++++++++++++++++++++++++\n";
#p $message;
print "+++++++++++++++++++++++++++++\n";
$p->close();
 
