#!/usr/bin/env perl
#===============================================================================
#
#         FILE: mac-search_mt_ae.pl
#
#        USAGE: ./mac-search_mt_ae.pl  
#
#  DESCRIPTION: Асинхронно событийная версия скрипта поиска мак адреса коммутаторов 
#               по snmp. Задаем входной файл с ip адресами и в  выходной файл 
#               получаем IP + MAC
#
#      OPTIONS: ---
# REQUIREMENTS: ---
#         BUGS: ---
#        NOTES: ---
#       AUTHOR: Tolmachev V.N. (), 
# ORGANIZATION: 
#      VERSION: 1.0
#      CREATED: 01.04.2019 14:36:16
#     REVISION: ---
#===============================================================================

use Socket;
use AnyEvent;
use AnyEvent::SNMP;
use Devel::Timer;
use strict;
use warnings;
use utf8;
use Net::SNMP;
use Math::Utils 'ceil';
use Data::Printer;
use Data::Dumper;

my $timing = Devel::Timer->new();
$timing->mark('start programm');

$|=1; # отключаем буферизацию вывода
#################################################################
my $file_input='snmp_host.txt'; #'test_ip.txt';#'snmp_host.txt';#'test_ip.txt';#'sw.syslog.test.txt'; #; #
my $file_output='address-mac-snmp-ae.txt';
my $community : shared;
$community = 'public';
#################################################################
my @ipm; # массив ip адресов полученный из файла
my %ip_mac; # : shared; # итоговый хэш, общий для всех тредов

my $OID_mac_find='1.3.6.1.2.1.17.1.1.0'; #показать мак-адрес
my @oids : shared;
push (@oids, $OID_mac_find);
#============================================
# создаём массив ip адресов из файла
	open (FILE, $file_input) or die "can`t open file";
	while (defined (my $file_line = <FILE>)) {
		if ($file_line=~/(\d{1,3}[.]\d{1,3}[.]\d{1,3}[.]\d{1,3})/){
	push(@ipm,$1);
		}
	}
	foreach (@ipm) {print "-=$_=-\n"}
	close FILE;
my @base_ipm=@ipm; # запоминаем начальное состояние массива адресов
#============================================

$timing->mark('create @ipm');

# по каждому адресу получаем мак и создаём хэш адрес-макa
# основной цикл выполнения потоков -------------------------------

my $ipmass=$#ipm; # текущий размер массива
my @tr_mac; # массив тредов
#------------------------------------------------------------------
my $num_th=300; # количество процессов на запуск
# разбираем массив на части, основной цикл обработки
#------------------------------------------------------------------

$timing->mark('mass ip divided for treads');
$timing->mark('run all treads');

#---------------------------------------------------------------------------- 

# ============================================================================

#-----------------------------------------------------------------------
# функция для треда
#-----------------------------------------------------------------------
		my @tt;
		my %th_hash;
		my $res;
MMM: while (@ipm){
my $cv = AnyEvent->condvar; 
$cv->begin;
for (my $i=0; $i < $num_th; $i++){
#while (@ipm)	{

		my $ip_=pop @ipm;
		unless ($ip_) {last};
	#	my $ip_='10.1.1.1';
		$cv->begin;
		#my $session = 
		Net::SNMP->session(
				-hostname      => $ip_,
				-community     => $community,
			       -translate     => [-octetstring => 0],
			      # -version       => 'snmpv2c',
			#	-timeout       => 1,
				-nonblocking => 1,
			     )->get_request(
				-varbindlist => \@oids,
			       # -delay => 1,
				-callback => sub {
			my $snmp_q=shift;
			my %result;
p $ip_;
	if (defined $snmp_q->var_bind_list()){
	foreach my $oi (@oids){ $result{$oi} = $snmp_q->var_bind_list()->{$oi}; } }
p %result ;
		my $rr;
		my @vv=values %result;
		if (@vv==0){@tt[0]='timeout'} else {@tt=values %result}; 
		if ($tt[0] eq 'timeout') {$rr='undefined';} 
			else {$rr=unpack("H*", $tt[0])};
		$th_hash{$ip_}=$rr;
	print" thread ip = $ip_ mac=$rr; \n";
	#	Net::SNMP->close();
			$cv->send(\%th_hash);	});
#snmp_dispatcher();
		#$cv->end;
		}
$cv->end; 
$res=$cv->recv;   #wait;
p $res;
}
p $res;
%ip_mac=%$res;
my @new_ipm =keys %ip_mac;

# @a - исходный массив 
# @b - модифицированый массив 
 
my %seen_a = (); 
my %seen_b = (); 
my @aonly = (); # уникальные элементы в исходном массиве 
 
foreach my $item (@new_ipm) { $seen_b{$item} = 1;} 
foreach my $item (@base_ipm) { $seen_a{$item} = 1;} 
 
foreach my $item (@base_ipm) { push(@aonly, $item) unless $seen_b{$item} } 
p @aonly;

if (@aonly) {@ipm=@aonly;
print "GOTO NEXT ITERATION !!!\n"; goto MMM; };


# ===================================================================
print "Find  $ipmass IP address.\n";
print "Save to file: $file_output\n";
# выводим хэш в файл 
	open (FILE,">$file_output") or die " can`t open file";

	while ( my ($ip_key, $ip_value) = each(%ip_mac) ) {
#	print  "$ip_key    $ip_value\n";
#вставляем в мак пробелы или чтонибудь другое
	$ip_value=~s/(..)(..)(..)(..)(..)(..)/$1 $2 $3 $4 $5 $6/;
	print FILE "        ";
	printf FILE "%-20s", $ip_key;
	print  FILE "        ";
	printf FILE  "%17s", "$ip_value\n";
	 }

	close(FILE);

$timing->mark('save to file, end programm');
$timing->report(sort_by => 'interval');
