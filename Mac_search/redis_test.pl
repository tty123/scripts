#!/usr/bin/env perl
#===============================================================================
#
#         FILE: redis_test.pl
#
#        USAGE: ./redis_test.pl  
#
#  DESCRIPTION: 
#
#      OPTIONS: ---
# REQUIREMENTS: ---
#         BUGS: ---
#        NOTES: ---
#       AUTHOR: Tolmachev V.N. (), 
# ORGANIZATION: 
#      VERSION: 1.0
#      CREATED: 01.03.2019 08:58:12
#     REVISION: ---
#===============================================================================

use strict;
use warnings;
use utf8;
use Redis;

my $redis = Redis->new;
$redis->ping || die "redis do not answer";

my $redis_info_hash = $redis->info();

$redis->set('name', 'cheburator');

$redis->flushdb;
my $keys = $redis->dbsize;
print $keys."\n";
 
#foreach (keys %{$redis_info_hash}) {
#        print $_.": ".$redis_info_hash->{$_}."\n";
#
#my $keys = $redis->dbsize;
# 
#print $keys."\n";
#}
 
$redis->quit;
