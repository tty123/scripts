#!/usr/bin/env perl
#===============================================================================
#
#         FILE: mac-search_mt1.pl
#
#        USAGE: ./mac-search_mt1.pl  
#
#  DESCRIPTION: Мультипоточная версия скрипта поиска мак адреса коммутаторов 
#               по snmp. Задаем входной файл с ip адресами и в  выходной файл 
#               получаем IP + MAC. Используем Redis в качестве кэша.
#      OPTIONS: ---
# REQUIREMENTS: ---
#         BUGS: ---
#        NOTES: ---
#       AUTHOR: Tolmachev V.N. (), 
# ORGANIZATION: Vladlink.ru 
#      VERSION: 1.1
#      CREATED: 27.02.2019 12:00:38
#     REVISION: ---
#===============================================================================
use Socket;
use threads qw(yield);
use threads::shared;
use strict;
use warnings;
use utf8;
use Net::SNMP;
use Math::Utils 'ceil';
use Redis;

$|=1; # отключаем буферизацию вывода
#################################################################
my $file_input='snmp_host.txt'; #'test_ip.txt';#'snmp_host.txt';#'test_ip.txt';#'sw.syslog.test.txt'; #; #
my $file_output='address-mac-snmp-mt4.txt';
my $community : shared;
$community = 'public';
#################################################################
my @ipm : shared ; # массив ip адресов полученный из файла
my %ip_mac; # : shared; # итоговый хэш, общий для всех тредов

my $OID_mac_find='1.3.6.1.2.1.17.1.1.0'; #показать мак-адрес
my @oids : shared;
push (@oids, $OID_mac_find);
#============================================
# создаём массив ip адресов из файла
	open (FILE, $file_input) or die "can`t open file";
	while (defined (my $file_line = <FILE>)) {
		if ($file_line=~/(\d{1,3}[.]\d{1,3}[.]\d{1,3}[.]\d{1,3})/){
	push(@ipm,$1);
		}
	}
	foreach (@ipm) {print "-=$_=-\n"}
	close FILE;
#============================================


# по каждому адресу получаем мак и создаём хэш адрес-макa
# основной цикл выполнения потоков -------------------------------

my $ipmass=$#ipm; # текущий размер массива
my @tr_mac; # массив тредов
#------------------------------------------------------------------
my $num_th=300; # количество потоков на запуск
# разбираем массив на части, основной цикл обработки
#------------------------------------------------------------------

my ($ths, $bb) = mass_ip_div($num_th, \@ipm);
#	%ip_mac = run_threadblock($ths, $bb);


# ----------------------------------------------------------------------------
# процедура запуска тредов, на вход определённый массив ip адресов на выходе хэш с маком
# ----------------------------------------------------------------------------
#	sub run_threadblock {
			my ($n_th, $da)=($ths,$bb);
			my %data;
			my $i=0;
	for ($i; $i < $n_th; $i++){ 
my @a=@{@$da[$i]};	
#print "\@a=@a-----\n";
print "th $i started\n";
	$tr_mac[$i] = threads->create({'context' => 'list'},'get_mac', $da->[$i]); # создаем поток
	if (!@{@$da[$i]}){last};
	};
				
# блок диагностики потоков--------------------------------------
my $tf1=threads->list(threads::all);
my $tf2=threads->list(threads::running);
my $tf3=threads->list(threads::joinable);
print "threads::all = ~$tf1~\n";
print "threads::running = ~$tf2~\n";
print "threads::joinable = ~$tf3~\n";
#-----------------------------------------------------------------
	# забираем значение из треда
while (threads->list(threads::running)){};
	for (my $ni=0; $ni < $i; $ni++){
	my (%div_hash)=$tr_mac[$ni]->join();
print "join thred $ni ***********\n";
	@data{ keys %div_hash } = values %div_hash;
		}

#	threads->yield();
#	return %data;
#	};
# ============================================================================
%ip_mac=%data;

#$redis = Redis->set( '$ip_' => '$rr');



#---------------------------------------------------------------------------
# подключаем кэш-память на Редиске
#---------------------------------------------------------------------------



#---------------------------------------------------------------------------- 
# процедура разбивки рабочего массива на части для обработки каждым потоком
#----------------------------------------------------------------------------
sub mass_ip_div{
my ($n_threads, $imass)= @_;
#print "\$n_threads=$n_threads , @{$imass}\n";
my @r_mass;
my $m_size=@$imass;
my $cycl;
my $j=0;
	if ($m_size > $n_threads) { $cycl = ceil($m_size/$n_threads);} # выбор массы заряда для потока
		else { $cycl = $m_size;}
	while (@{$imass}){
		for (my $i=0; $i < $cycl; $i++){
		$r_mass[$j][$i]= shift @{$imass};
		if (!@{$imass}) { print "########### \$cycl=$cycl \$i = $i ###################\@ipm  divided on  \$j=$j!!!\n"; last; }
		}
	$j++;
	}
#--------тест код------------------
my $g=0;
for (my $s=0; $s<$j;$s++){
print "Массив $s: $#{$r_mass[$s]} \n";
#	foreach (@{$r_mass[$s]}){
#	print "~~количество элементов в массиве $_~~ \n";
#	}
}
return ($j , \@r_mass);
};
#============================================================================


#-----------------------------------------------------------------------
# функция для треда
#-----------------------------------------------------------------------
	sub get_mac{
#require Redis;
		my $data=shift;
		my $ip_;
		my @tt=();
		my %th_hash;
		my @oi=('1.3.6.1.2.1.17.1.1.0');
#my $t=threads->list(threads::all);
#while ($t<290){};
#sleep(1);
while (@{$data}){

		$ip_=shift @{$data};
		my ($session, $error) = Net::SNMP->session(
				-hostname      => $ip_,
				-community     => 'public',# $community,
			       -translate     => [-octetstring => 0],
			      # -version       => 'snmpv2c',
				-timeout       => 1,

			     ); 
		if (!defined $session) {
		printf "ERROR: %s.\n", $error; 
		exit 1;}

		my $result = $session->get_request(
				-varbindlist => \@oi,
		);
		my $rr;
		my @vv=values %{$result};
		if (@vv==0){@tt[0]='timeout'} else {@tt=values %{$result}}; 
		if ($tt[0] eq 'timeout') {$rr='undefined';} 
			else {$rr=unpack("H*", $tt[0])};
# здесь не работат my $redis = Redis->new( server => '127.0.0.1:6379', debug => 0 );
		$th_hash{$ip_}=$rr;
		#$ip_mac{$ip_}=$rr;
print"thread ip = $ip_ mac=$rr; \n";
		$session->close();
		}
	return (%th_hash);
	#threads->detach();
	#threads->exit();
	};
# ===================================================================
print "Find  $ipmass IP address.\n";
print "Save to file: $file_output\n";
# выводим хэш в файл 

my $r = Redis->new( server => '127.0.0.1:6379',
			 debug => 0 ,
			password=>'123456');
$r->ping || die "redis do not answer";
	open (FILE,">$file_output") or die " can`t open file";

	while ( my ($ip_key, $ip_value) = each(%ip_mac) ) {
#	print  "$ip_key    $ip_value\n";

$r->set($ip_key=>$ip_value);
# для красоты отображения,вставляем в мак пробелы или чтонибудь другое
	$ip_value=~s/(..)(..)(..)(..)(..)(..)/$1 $2 $3 $4 $5 $6/;
	print FILE "        ";
	printf FILE "%-20s", $ip_key;
	print  FILE "        ";
	printf FILE  "%17s", "$ip_value\n";
	 }

	close(FILE);

my $keys = $r->dbsize;
print $keys."\n";

my @r_keys=$r->keys('*');
print "---------============== Receive keys from REDIS: ==============------------\n";
foreach (@r_keys){
my $gt=$r->get($_);
print "-=ip: $_   mac: $gt=-\n";
}
$r->quit; 
