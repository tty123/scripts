#!/usr/bin/env perl
#===============================================================================
#
#         FILE: mac-search.pl
#
#        USAGE: ./mac-search.pl  
#
#  DESCRIPTION: ищем мак адреса коммутаторов по snmp, задаем входной файл с ip
#               адресами и в  выходной файл получаем IP + MAC
#      OPTIONS: ---
# REQUIREMENTS: ---
#         BUGS: ---
#        NOTES: ---
#       AUTHOR: Tolmachev V.N. (), 
# ORGANIZATION: Vladlink.ru 
#      VERSION: 1.0
#      CREATED: 13.02.2019 08:56:38
#     REVISION: ---
#===============================================================================

use strict;
use warnings;
use utf8;
use Net::SNMP;
$|=1; # отключаем буферизацию вывода
#----------------------------------------------------------------
my $ip='10.244.9.10'; #VERMAX
my $community = 'public';
#----------------------------------------------------------------
my $val;
my @ipm; # массив ip адресов полученный из файла
my %ip_mac;

my $OID_mac_find='1.3.6.1.4.1.40418.1.1.1.21'; #показать мак-адрес
my @oids=();
my @tt=();
my $kt;
push (@oids, $OID_mac_find);
# по каждому адресу получаем мак и создаём хэш адрес-макa
	my ($session, $error) = Net::SNMP->session(
                        -hostname      => $ip,
                        -community     => $community,
                        -translate     => [-octetstring => 0],
                        -version       => 'snmpv2c',
			-timeout       => 1,
                     ); 
	if (!defined $session) {
	printf "ERROR: %s.\n", $error; 
	exit 1;}

	my $result = $session->get_request(
			-varbindlist => \@oids,
	);
print "$_ $$result{$_}\n" for (keys %{$result});
#print " resault".@{[%{$result}]}."\n";
#		else {$rr=unpack("H*", $tt[0])};
	$session->close();
