#!/usr/bin/env perl
#===============================================================================
#
#         FILE: mac-search_mt_queue.pl
#
#        USAGE: ./mac-search_mt_queue.pl  
#
#  DESCRIPTION: Мультипоточная версия скрипта поиска мак адреса коммутаторов 
#               по snmp. Используем 2 очереди (in out) для тредов.
#               Задаем входной файл с ip адресами и в  выходной файл 
#               получаем IP + MAC. По полученным результатам в 2,5 раза эффективнее,
#               чем с разбивкой данных mac-search_mt.pl 
#
#      OPTIONS: ---
# REQUIREMENTS: ---
#         BUGS: ---
#        NOTES: ---
#       AUTHOR: Tolmachev V.N. (), 
# ORGANIZATION: 
#      VERSION: 1.0
#      CREATED: 29.03.2019 12:24:09
#     REVISION: ---
#===============================================================================
use Socket;
#use forks;
use Devel::Timer;
use threads qw(yield);
use threads::shared;
use Thread::Queue;
use strict;
use warnings;
use utf8;
use Net::SNMP;
#use Math::Utils 'ceil';

my $score_timer = Devel::Timer->new();
$score_timer->mark('start programm');

$|=1; # отключаем буферизацию вывода
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
my $file_input='snmp_host.txt'; #'test_ip.txt';#'snmp_host.txt';#'test_ip.txt';#'sw.syslog.test.txt'; #; #
my $file_output='address-mac-snmp-mt_queue.txt';
my $num_th=10; # количество потоков на запуск (4потока = 4ядра время 12мин, все результаты без ошибок)
my $community : shared;
$community = 'public';
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
my @ipm : shared ; # массив ip адресов полученный из файла
my %ip_mac; # : shared; # итоговый хэш, общий для всех тредов

my $OID_mac_find='1.3.6.1.2.1.17.1.1.0'; #показать мак-адрес
my @oids : shared;
push (@oids, $OID_mac_find);
my @tr_mac; # массив тредов
my $data_in_queue; # количество элементов входной очереди
my $qu_inp = Thread::Queue->new();# создаём очередь для входных данных
my $qu_out = Thread::Queue->new();# очередь для выходных данных
#============================================
# Процедура выдергивания ip из файла и помещения во входную очередь
sub get_ip {

	open (FILE, $file_input) or die "can`t open file";
	while (defined (my $file_line = <FILE>)) {
		if ($file_line=~/(\d{1,3}[.]\d{1,3}[.]\d{1,3}[.]\d{1,3})/){
	$qu_inp->enqueue($1); # закидываем данные в очередь
	print "-=$1=-\n";
		}
	}
	close FILE;
#	$qu_inp->enqueue('end'); # кидаем сигнал конец очереди
 $data_in_queue = $qu_inp->pending();

	$qu_inp->end();# закрываем прием данных в очередь
    };
#============================================

$score_timer->mark('create get_ip');

# основной цикл выполнения потоков -------------------------------

#------------------------------------------------------------------

# ----------------------------------------------------------------------------
# процедура запуска тредов
# ----------------------------------------------------------------------------
	sub run_threadblock {
			my $n_th=shift;
			my $i=0;
	for ($i; $i < $n_th; $i++){ 
print "th $i started\n";
	$tr_mac[$i] = threads->create('get_mac'); # создаем поток
	};
				
# блок диагностики потоков--------------------------------------
my $tf1=threads->list(threads::all);
my $tf2=threads->list(threads::running);
my $tf3=threads->list(threads::joinable);
print "threads::all = ~$tf1~\n";
print "threads::running = ~$tf2~\n";
print "threads::joinable = ~$tf3~\n";
	};
#-----------------------------------------------------------------

#-----------------------------------------------------------------------
# функция для треда
#-----------------------------------------------------------------------
	sub get_mac{
		my $data= shift @_;
		my $ip_= 0;
		my @tt=();
		my @th_hash;
#		my $trigger=0;
# my $item = $qu_inp->dequeue_nb();

while (1){        # цикл выборки из очереди
	#	unless ($qu_inp->pending()||($trigger) ){next;} # пропускаем итерацию если очередь пуста 
#		$trigger++;
		$ip_= $qu_inp->dequeue_nb();
		unless ($ip_) {last;};
		my ($session, $error) = Net::SNMP->session(
				-hostname      => $ip_,
				-community     => $community,
			       -translate     => [-octetstring => 0],
			      # -version       => 'snmpv2c',
				-timeout       => 1,

			     ); 
		if (!defined $session) {
		printf "ERROR: %s.\n", $error; 
		exit 1;}

		my $result = $session->get_request(
				-varbindlist => \@oids,
		);
		my $rr;
		my @vv=values %{$result};
		if (@vv==0){@tt[0]='timeout'} else {@tt=values %{$result}}; 
		if ($tt[0] eq 'timeout') {$rr='undefined';} 
			else {$rr=unpack("H*", $tt[0])};
		@th_hash=($ip_,$rr);
print"thread ip = $ip_ mac=$rr; \n";
	$qu_out->enqueue(\@th_hash); # ссылку на массив в очередь
		$session->close();
		}	
	threads->detach(); # отстреливаем отработанный процесс
	#threads->exit();
	};
# ===================================================================
# процедура получения результата и сохранения в файл
#--------------------------------------------------------------------
sub get_result {
my ($ip, $mac);
my $qdata;

while (threads->list(threads::running)){
my $data_queue = $qu_inp->pending();
my $list_threads = threads->list(threads::running);
print "running threads: $list_threads\n";
if ($data_queue) {print "elementh in input queue: $data_queue \n"};
}; # пока все треды не отработают, результат не выдавать
while ($qu_out->pending()){
$qdata=$qu_out->dequeue();
$ip_mac{$$qdata[0]} = $$qdata[1];
};
print "processed $data_in_queue elements input queue\n";
print "Save to file: $file_output\n";
# выводим хэш в файл 
	open (FILE,">$file_output") or die " can`t open file";

	while ( my ($ip_key, $ip_value) = each(%ip_mac) ) {
#	print  "$ip_key    $ip_value\n";
#вставляем в мак пробелы или чтонибудь другое
	$ip_value=~s/(..)(..)(..)(..)(..)(..)/$1 $2 $3 $4 $5 $6/;
	print FILE "        ";
	printf FILE "%-20s", $ip_key;
	print  FILE "        ";
	printf FILE  "%17s", "$ip_value\n";
	 }

	close(FILE);
};
#------------------------------------------------------------------
#==================================================================
# Основной поток выполнения
#==================================================================
	&get_ip;
$score_timer->mark('mass ip divided for treads');
	&run_threadblock($num_th);
$score_timer->mark('run all treads');
	&get_result;
$score_timer->mark('save to file, end programm');
$score_timer->report(sort_by => 'interval');
#=============== the end ==========================================
