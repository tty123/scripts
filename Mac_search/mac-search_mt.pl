#!/usr/bin/env perl
#===============================================================================
#
#         FILE: mac-search_mt.pl
#
#        USAGE: ./mac-search_mt.pl  
#
#  DESCRIPTION: Мультипоточная версия скрипта поиска мак адреса коммутаторов 
#               по snmp. Задаем входной файл с ip адресами и в  выходной файл 
#               получаем IP + MAC
#      OPTIONS: ---
# REQUIREMENTS: ---
#         BUGS: ---
#        NOTES: ---
#       AUTHOR: Tolmachev V.N. (), 
# ORGANIZATION: Vladlink.ru 
#      VERSION: 1.0
#      CREATED: 20.02.2019 12:00:38
#     REVISION: ---
#===============================================================================
use Socket;
#use forks;
use Devel::Timer;
use threads qw(yield);
use threads::shared;
use strict;
use warnings;
use utf8;
use Net::SNMP;
use Math::Utils 'ceil';
use Data::Printer; 

my $timing = Devel::Timer->new();
$timing->mark('start programm');

$|=1; # отключаем буферизацию вывода
#################################################################
my $file_input='snmp_host.txt'; #'test_ip.txt';#'snmp_host.txt';#'test_ip.txt';#'sw.syslog.test.txt'; #; #
my $file_output='address-mac-snmp-mt4.txt';
my $community : shared;
   $community = 'public';
my $num_th=50; #130 количество потоков на запуск
#################################################################
my @ipm : shared ; # массив ip адресов полученный из файла
my %ip_mac; # : shared; # итоговый хэш, общий для всех тредов

my $OID_mac_find='1.3.6.1.2.1.17.1.1.0'; #показать мак-адрес
my @oids : shared;
push (@oids, $OID_mac_find);
#============================================
# создаём массив ip адресов из файла
	open (FILE, $file_input) or die "can`t open file";
	while (defined (my $file_line = <FILE>)) {
		if ($file_line=~/(\d{1,3}[.]\d{1,3}[.]\d{1,3}[.]\d{1,3})/){
	push(@ipm,$1);
		}
	}
	foreach (@ipm) {print "-=$_=-\n"}
	close FILE;

my @base_ipm = @ipm; # массив для учета пропущенных адресов 
#============================================
my $ipmass=$#ipm; # текущий размер массива
#------------------------------------------------------------------


#---------------------------------------------------------------------------- 
# процедура разбивки рабочего массива на части для обработки каждым потоком
#----------------------------------------------------------------------------
sub mass_ip_div{
my ($n_threads, $imass)= @_;
#print "\$n_threads=$n_threads , @{$imass}\n";
my @r_mass;
my $m_size=@$imass;
my $cycl;
my $j=0;
	if ($m_size > $n_threads) { $cycl = ceil($m_size/$n_threads);} # выбор массы заряда для потока
		else { $cycl = $m_size;}
	while (@{$imass}){
		for (my $i=0; $i < $cycl; $i++){
		$r_mass[$j][$i]= shift @{$imass};
		if (!@{$imass}) { print "########### \$cycl=$cycl \$i = $i ###################\@ipm  divided on  \$j=$j!!!\n"; last; }
		}
	$j++;
	}
#--------тест код------------------
my $g=0;
for (my $s=0; $s<$j;$s++){
print "Massive $s: $#{$r_mass[$s]} \n";
#	foreach (@{$r_mass[$s]}){
#	print "~~количество элементов в массиве $_~~ \n";
#	}
}
return ($j , \@r_mass); # расчётное кол-во потоков, двумерный массив разделённых данных для каждого потока
};
#============================================================================

# ----------------------------------------------------------------------------
# процедура запуска тредов, на вход определённый массив ip адресов на выходе хэш с маком
# ----------------------------------------------------------------------------
	sub run_threadblock {
			my ($n_th, $da)=@_;# количество потоков, двумерный массив с данными для каждого потока
			my %data;
			my $i=0;
			my @tr_mac;
	for ($i; $i < $n_th; $i++){ 
my @a=@{@$da[$i]};	
#print "\@a=@a-----\n";
print "th $i started\n";
	$tr_mac[$i] = threads->create({'context' => 'list'},'get_mac', $da->[$i]); # создаем поток
	if (!@{@$da[$i]}){last};
	}
				
# блок диагностики потоков......................................
my $tf1=threads->list(threads::all);
my $tf2=threads->list(threads::running);
my $tf3=threads->list(threads::joinable);
print "threads::all = ~$tf1~\n";
print "threads::running = ~$tf2~\n";
print "threads::joinable = ~$tf3~\n";
#.................................................................
	# забираем значение из треда
while (threads->list(threads::running)){};
	for (my $ni=0; $ni < $i; $ni++){
	my (%div_hash)=$tr_mac[$ni]->join();
print "join thred $ni ***********\n";
	@data{ keys %div_hash } = values %div_hash;
		}

#	threads->yield();
	return %data;
	};
# ============================================================================

#-----------------------------------------------------------------------
# функция для треда
#-----------------------------------------------------------------------
	sub get_mac{
		my $data= shift @_;
		my $ip_;
		my @tt=();
		my %th_hash;

while (@{$data}){

		$ip_=shift @{$data};
		my ($session, $error) = Net::SNMP->session(
				-hostname      => $ip_,
				-community     => $community,
			       -translate     => [-octetstring => 0],
			      # -version       => 'snmpv2c',
				-timeout       => 1,

			     ); 
		if (!defined $session) {
		printf "ERROR: %s.\n", $error; 
		exit 1;}

		my $result = $session->get_request(
				-varbindlist => \@oids,
		);
		my $rr;
		my @vv=values %{$result};
		if (@vv==0){@tt[0]='timeout'} else {@tt=values %{$result}}; 
		if ($tt[0] eq 'timeout') {$rr='undefined';} 
			else {$rr=unpack("H*", $tt[0])};
		$th_hash{$ip_}=$rr;
		#$ip_mac{$ip_}=$rr;
print"thread ip = $ip_ mac=$rr; \n";
		$session->close();
		}
	return (%th_hash);
	#threads->detach();
	#threads->exit();
	};
# ===================================================================

# ----------- процедура вывода и сохранения результата -------------------------
sub get_result{
print "Find  $ipmass IP address.\n";
print "Save to file: $file_output\n";
# выводим хэш в файл 
	open (FILE,">$file_output") or die " can`t open file";

	while ( my ($ip_key, $ip_value) = each(%ip_mac) ) {
#	print  "$ip_key    $ip_value\n";
#вставляем в мак пробелы или чтонибудь другое
	$ip_value=~s/(..)(..)(..)(..)(..)(..)/$1 $2 $3 $4 $5 $6/;
	print FILE "        ";
	printf FILE "%-20s", $ip_key;
	print  FILE "        ";
	printf FILE  "%17s", "$ip_value\n";
	 }

	close(FILE);
}
# ===================================================================

# ----- функция выделения необработанных данных в массив -----
sub unprocessed_massive {            # post_obrabotka {
	my @a = @base_ipm;
	my @b = keys %ip_mac;

	# @a - исходный массив 
	# @b - модифицированый массив 
	 
	my %seen_a = (); 
	my %seen_b = (); 
	my @aonly = (); # уникальные элементы в исходном массиве 
	 
	foreach my $item (@b) { $seen_b{$item} = 1;} 
	foreach my $item (@a) { $seen_a{$item} = 1;} 
	 
	foreach my $item (@a) { push(@aonly, $item) unless $seen_b{$item} }; 
	return \@aonly; #ссылка на массив различий
}
# ==============================================================

##############  основной цикл выполнения ########################
$timing->mark('create @ipm');

# по каждому адресу получаем мак и создаём хэш адрес-макa
# основной цикл выполнения потоков -------------------------------

# разбираем массив на части, основной цикл обработки
#------------------------------------------------------------------

$timing->mark('start script');
my ($ths, $bb) = mass_ip_div($num_th, \@ipm);
				#
	%ip_mac = run_threadblock($ths, $bb);
				#$timing->mark('run all treads');
#my $ref_aonly =	unprocessed_massive();
#p @$ref_aonly;
#	my ($ths1, $bb1) = mass_ip_div('10', $ref_aonly);
				#$timing->mark('mass ip divided for treads');
#my	%ip_mac1 = run_threadblock($ths1, $bb1);
	&get_result;

# *******************************************

$timing->mark('save to file, end programm');
	$timing->report(sort_by => 'interval');
