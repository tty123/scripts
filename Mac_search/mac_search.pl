#!/usr/bin/env perl
#===============================================================================
#
#         FILE: mac-search.pl
#
#        USAGE: ./mac-search.pl  
#
#  DESCRIPTION: ищем мак адреса коммутаторов по snmp, задаем входной файл с ip
#               адресами и в  выходной файл получаем IP + MAC
#      OPTIONS: ---
# REQUIREMENTS: ---
#         BUGS: ---
#        NOTES: ---
#       AUTHOR: Tolmachev V.N. (), 
# ORGANIZATION: Vladlink.ru 
#      VERSION: 1.0
#      CREATED: 13.02.2019 08:56:38
#     REVISION: ---
#===============================================================================

use strict;
use warnings;
use utf8;
use Net::SNMP;
$|=1; # отключаем буферизацию вывода
#----------------------------------------------------------------
my $file_input='edfa.txt';#'sw.syslog.test.txt'; #snmp_host.txt'; #'sw.syslog.test.txt';
my $file_output='address-mac-snmp1111111111111111111111.txt';
my $community = 'public';
#----------------------------------------------------------------
my $val;
my @ipm; # массив ip адресов полученный из файла
my %ip_mac;

my $OID_mac_find='1.3.6.1.2.1.17.1.1.0'; #показать мак-адрес
my @oids=();
my @tt=();
my $kt;
$tt[0]='';
push (@oids, $OID_mac_find);

# создаём массив ip адресов из файла
	open (FILE, $file_input) or die "can`t open file";
	while (defined (my $file_line = <FILE>)) {
		if ($file_line=~/(\d{1,3}[.]\d{1,3}[.]\d{1,3}[.]\d{1,3})/){
	push(@ipm,$1);
		}
	}
	foreach (@ipm) {print "$_\n"}
	close FILE;

		# блок индикатора загрузки
		my $mass_l=$#ipm+1;
		print "----------------------------\n";
		print "Find $mass_l ip.\n";
		print "Starting  SNMP send-request: \n"; 
		my $u_count=0; # подсчет неудачных запросов мак адреса
		my $ig=0; 
		my $igg=50; # максимальное  количество звёздочек на строку 
		my $var=$igg;
		my $z;

# по каждому адресу получаем мак и создаём хэш адрес-макa
	foreach (@ipm){
	my ($session, $error) = Net::SNMP->session(
                        -hostname      => $_,
                        -community     => $community,
                        -translate     => [-octetstring => 0],
                        -version       => 'snmpv2c',
			-timeout       => 1,
                     ); 
	if (!defined $session) {
	printf "ERROR: %s.\n", $error; 
	exit 1;}

	my $result = $session->get_request(
			-varbindlist => \@oids,
	);
	my $rr;
	if ((values %{$result})==0){@tt[0]='timeout'} else {@tt=values %{$result}}; 
	if ($tt[0] eq 'timeout') {$rr='undefined'; $u_count++;} 
		else {$rr=unpack("H*", $tt[0])};
	$ip_mac{$_}=$rr;
	$session->close();
# индикатор получения мак адреса
			$ig++;
			if ($ig>$var){print"\n";$var+=$igg;}; # переводим строку
			if ($rr eq 'undefined'){print 'x'} else {print "*"};
	}
print "\n----------------------------\n";
print "Saccesful requests: ".($mass_l-$u_count)."\n";
print "Undefined requests: ".$u_count."\n";
print "----------------------------\n";
print "Save to file: $file_output\n";
# выводим хэш в файл 
	open (FILE,">$file_output") or die " can`t open file";

	while ( my ($ip_key, $ip_value) = each(%ip_mac) ) {
#	print FILE "$ip_key    $ip_value\n";
#вставляем в мак пробелы или чтонибудь другое
	$ip_value=~s/(..)(..)(..)(..)(..)(..)/$1 $2 $3 $4 $5 $6/;
	print FILE "        ";
	printf FILE "%-20s", $ip_key;
	print FILE "        ";
	printf FILE "%17s", "$ip_value\n";
	 }

	close(FILE);

# сортировка массива

