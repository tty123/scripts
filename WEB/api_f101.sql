-- FUNCTION: public.api_f101(text, bigint, bigint, character varying, character varying)

-- DROP FUNCTION public.api_f101(text, bigint, bigint, character varying, character varying);

CREATE OR REPLACE FUNCTION public.api_f101(
	ipp text,
	tfrom bigint,
	tto bigint,
	port_ character varying)
    RETURNS TABLE(addr inet, stime timestamp without time zone, facility smallint, severity smallint, message character varying, port character varying, port_state character varying) 
    LANGUAGE 'plpgsql'
 COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
DECLARE
    g RECORD;
	paddr character varying[];
	tfrom_t timestamp without time zone;
	tto_t timestamp without time zone;
	tfrom_cr bigint;
	tfrom_new bigint;
BEGIN
	IF (tto-tfrom)>86400 THEN tfrom_new = tto-86400; -- ограничение времени (24 часа)
		ELSE tfrom_new=tfrom;
	END IF;
	
	tfrom_t = public.from_unixtime(tfrom_new);
	tfrom_cr=tfrom_new - 86400;
	tto_t = public.from_unixtime(tto);
	paddr[1] = regexp_replace(regexp_replace(ipp,'\.','_','g'),'_\d+$','_0');
	FOR g IN SELECT s.table_name FROM unnest(paddr) as t INNER JOIN
		(SELECT c.table_name, substring(c.table_name::text from 'log_(\d{1,3}_\d{1,3}_\d{1,3}_\d{1,3})_\d{10}'::text) as addr,
			substring(c.table_name::text from 'log_\d{1,3}_\d{1,3}_\d{1,3}_\d{1,3}_(\d{10})'::text) as tstamp 
		FROM syslog.tables c) s ON t::text = s.addr WHERE s.tstamp::bigint >= tfrom_cr AND s.tstamp::bigint <= tto  ORDER BY s.table_name
	LOOP
		RETURN QUERY EXECUTE 'SELECT raddr, date_trunc(''second'', "time") as "time", fac, sev, mess, port, port_state FROM syslog.'|| g.table_name ||' 
		WHERE raddr=inet('''|| ipp ||''') AND port like '''|| port_ ||''' AND  "time" >='''||tfrom_t||'''
		AND "time"<='''||tto_t||''' ORDER BY raddr, "time";';
			
	END LOOP;
RETURN;
END;
$BODY$;

ALTER FUNCTION public.api_f101(text, bigint, bigint, character varying)
    OWNER TO sysloger;
