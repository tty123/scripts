#!/usr/bin/env perl
#===============================================================================
#
#         FILE: pos_printer_test.pl
#
#        USAGE: ./pos_printer_test.pl  
#
#  DESCRIPTION: тестирование и обкатка принтера VCP80II
#
#      OPTIONS: ---
# REQUIREMENTS: ---
#         BUGS: ---
#        NOTES: ---
#       AUTHOR: Tolmachev V.N. (), 
# ORGANIZATION: 
#      VERSION: 1.0
#      CREATED: 25.03.2020 11:15:13
#     REVISION: ---
#===============================================================================

use strict;
use warnings;
use utf8;
use 5.010;
use Printer::ESCPOS;
use Data::Printer;
#
########################################
# посмотреть имя принтера в системе 
# $lpstat -p
########################################
#
#
#
##my $file = './test_check';
##my $printer = 'HP_LaserJet_Pro_MFP_M225rdn';#'CUSTOM_Engineering_VKP80';
##
##my $printer =  `lp -o page-bottom=2 -o page-top=2 -d $printer  $file`;
##say '--------------------------------';
##say '$printer';

#---- Секция печати -----                                                                                                                                                              
my $device = Printer::ESCPOS->new(
          driverType => 'USB',
          vendorId   => 0x0dd4,
          productId  => 0x015d,
	  endPoint   => 0x02,
      );
       $device->printer->text("Hello Printer::ESCPOS\n");
        $device->printer->printAreaWidth( 255 );
         $device->printer->font('a'); #a, b, c
          $device->printer->bold(1);
           $device->printer->fontHeight(1); # 0-7
            $device->printer->fontWidth(1); # 0-7
$device->printer->charSpacing(5); #0-255
#$device->printer->lineSpacing($spacing); #0-255
$device->printer->cutPaper( feed => 0 );
$device->printer->print();
$device->printer->printerStatus;
p $device;
#
