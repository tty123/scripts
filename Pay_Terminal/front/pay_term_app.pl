#!/usr/bin/env perl
use Mojolicious::Lite;
use Mojo::JSON;

my $summ = 0;

get '/' => sub {
  my $c = shift;
  $c->stash('summ'=>$summ);
  $c->render(template => 'index');
};

app->start;
__DATA__

@@ index.html.ep
% layout 'default';
% title 'Welcome';
<h1>Welcome to the Mojolicious real-time web framework!</h1>

@@ layouts/default.html.ep
<!DOCTYPE html>
<html>
  <head>
    <title>программа тестирования API платёжного терминала</title>
  </head>
  <body>
    <table style="width: auto;" border="1">
      <tbody>
        <tr>
          <td><label for = "summ" >Принято к зачислению:</label> <textarea name="summ" minlenght="3" maxlength="5" readonly cols="1" rows="1"><%= $summ %></textarea></td>
        </tr>
        <tr>
	  <form method="post" action="http://localhost:8442/term">
          <td><button  name="escrow_on" formmethod="post">Начать приём купюр</button><br>
          </td>
	  </form>
        </tr>
        <tr>
	  <form method="post" action="http://localhost:8442/term">
          <td><button name="escrow_off" formmethod="post">Оплатить</button><br>
          </td>
	  </form>
        </tr>
        <tr>
	  <form method="post" action="http://localhost:8442/term">
          <td><button name="print_esc" form="print_esc" formmethod="post">Напечатать чек</button><br>
          </td>
	  </form>
        </tr>
      </tbody>
    </table>
    <SCRIPT src="scripts/payt_api.js" charset = "utf-8"> </SCRIPT>
  </body>
</html>
