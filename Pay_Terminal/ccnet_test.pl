#!/usr/bin/env perl
#===============================================================================
#
#         FILE: ccnet_test.pl
#
#        USAGE: ./ccnet_test.pl  
#
#  DESCRIPTION:  тестирование и отладка модуля ССNET
#
#      OPTIONS: ---
# REQUIREMENTS: ---
#         BUGS: ---
#        NOTES: ---
#       AUTHOR: Tolmachev V.N. (), 
# ORGANIZATION: 
#      VERSION: 1.0
#      CREATED: 18.03.2020 15:38:15
#     REVISION: ---
#===============================================================================

use strict;
use warnings;
use utf8;
use 5.010;
use Data::Printer;
use open qw(:std :utf8);

use FindBin qw($Bin); # юзы для моего модуля
use lib $Bin;
use CCNET; # подключаю свой модуль

$|=1;

my $pcash = CCNET-> new();

print "Выполнить RESET и выйти?"; 
chomp($a=<>);
if ($a eq "y") {
	$pcash->cmd_reset;
	exit;
};


my $res = $pcash-> init_power_on();
p $res;

#print "Едем дальше?"; 
#chomp(my $a=<>);
#if ($a eq "n") {exit};
#
#$pcash->cmd_poll();
#$pcash->cmd_reset();
#$pcash->cmd_poll();
#$pcash->cmd_poll();
#$pcash->cmd_poll();

print "Едем дальше?"; 
chomp($a=<>);
if ($a eq "n") {exit};

#$pcash->cmd_enable_bill_types ('0000ff','0000ff');
$pcash->set_bills('0038FC','0038FC'); # задаём использование купюр
my $arr6 = $pcash->cmd_get_status();
say "cmd_get_statust";
p $arr6;
my $res1 = $pcash -> escrow_on;
my $res4 = $pcash -> cmd_get_status;
my $nominals = $pcash -> get_nominals_table;
say "-------- nominals -------------";
p $nominals;
my $summ = 0;
	p $res4;
	while (1) {
	my $res2 = $pcash -> bill_stack_pos();
	p $res2;
	my $res3 = $pcash -> bill_stack_add();
	$pcash->cmd_poll; 
	p $res3; 
	$summ = $pcash -> get_escrow_sum;
	say "Принятая сумма = $summ";
	my $esc = $pcash -> get_escrow;
	p $esc;

	#	if ($res3[0]) {
	#		$summ += $res3[2];
	#		say "Принятая сумма = $summ";
	#	}
	$pcash->cmd_poll;
	#sleep(1);
	}
