#!/usr/bin/env perl
#===============================================================================
#
#         FILE: test_valid_ip.pl
#
#        USAGE: ./test_valid_ip.pl  
#
#  DESCRIPTION: Разработка функции валидации IP адреса. 
#  		На входе IP адрес в точечной нотации, на выходе 0 или 1 в случае
#  		правильного адреса.
#
#      OPTIONS: ---
# REQUIREMENTS: ---
#         BUGS: ---
#        NOTES: ---
#       AUTHOR: Tolmachev V.N. (), 
# ORGANIZATION: 
#      VERSION: 1.0
#      CREATED: 21.01.2020 09:43:27
#     REVISION: ---
#===============================================================================

use strict;
use warnings;
use utf8;
binmode(STDOUT,':utf8');

use Data::Printer;
#use Data::Dumper;


sub valid_ip {
	my $res;
	use re 'debug';  # включение отладки регулярки  
	my ($ip)= @_;
	my $regex=qr/^(\d{0,3})\.(\d{0,3})\.(\d{0,3})\.(\d{0,3})$/i;
	if ((${$ip}=~$regex)){   #  проверка валидности ip адреса
	p $1;
	p $2;
	p $3;
	p $4;
	if (!($1>=0 && $1<=255 && $2>=0 && $2<=255 && $3>=0 && $3<=255 && $4>=0 && $4<=255)){
		no re 'debug';   # выключение отладки регулярки  
		return ($res = 0);
		}
	}else {return ($res = 0); }

return	($res=1);
}

my $test = valid_ip(\'256.240.16.54');

p $test;
