#!/usr/bin/env perl
#===============================================================================
#
#         FILE: test_anonim_vars.pl
#
#        USAGE: ./test_anonim_vars.pl  
#
#  DESCRIPTION: тестирование анонимных вещей
#
#      OPTIONS: ---
# REQUIREMENTS: ---
#         BUGS: ---
#        NOTES: ---
#       AUTHOR: Tolmachev V.N. (), 
# ORGANIZATION: 
#      VERSION: 1.0
#      CREATED: 17.03.2020 15:43:43
#     REVISION: ---
#===============================================================================

use strict;
use warnings;
use utf8;
use 5.010;
use Data::Printer;
binmode(STDOUT,':utf8'); # чтобы не вылазило предупреждение "wide character in print"

# тестирование $_ в условии
my $str='8212';

say test();


say $_;

if (test()){
	say "условие выполнено".$_;
}
else {
	say "условие не выполнено".$_;
}
sub test{
return 0;
}


sub test2{
return (0, 1, "test");
}

my @t = test2();
p @t;
