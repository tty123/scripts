#!/usr/bin/env python3

# список покупок

shoplist = ['яблоки','манго','груши',',бананы']
print('Я должен сделать', len(shoplist), 'покупок')

# end=' ' вставляет пробел вместо перевода строки в конце print
print('Покупки:', end=' ')
for item in shoplist:
	print(item, end=' ')

print('\nТакже нужно купить риса.')
shoplist.append('рис')
print('Теперь мой список покупок таков:', shoplist)

print('Отсортирую свой список')
shoplist.sort()
print('Отсортированный список выглядит так:', shoplist)

print('Первое что нужно купить. это', shoplist[0])
olditem = shoplist[0]
del shoplist[0]
print('я купил', olditem)
print('теперь мой список покупок:', shoplist)

