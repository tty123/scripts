#! /usr/bin/env python3
#===============================================================================
#
#         FILE: simplestclass.py
#
#        USAGE: ./simplestclass.py
#
#  DESCRIPTION: простой класс
#
#      OPTIONS: ---
# REQUIREMENTS: ---
#         BUGS: ---
#        NOTES: ---
#       AUTHOR: Tolmachev V.N.
# ORGANIZATION:
#      VERSION: 1.0
#      CREATED: 26.11.2019  17:10
#     REVISION: ---
#===============================================================================

class Person:
    pass

p = Person()
print(p)
