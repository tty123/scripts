#! /usr/bin/env python3
#===============================================================================
#
#         FILE: backup_ver1.py
#
#        USAGE: ./backup_ver1.py
#
#  DESCRIPTION: скрипт бэкапа
#
#      OPTIONS: ---
# REQUIREMENTS: ---
#         BUGS: ---
#        NOTES: ---
#       AUTHOR: Tolmachev V.N.
# ORGANIZATION:
#      VERSION: 1.0
#      CREATED: 26.11.2019  15:07
#     REVISION: ---
#===============================================================================

import os
import time

# сохраняемые файлы и каталоги
source=['/home/tvn/Scripts', '/home/tvn/.bashrc']

# директория сохранения
target_dir='/home/tvn/backup'

target=target_dir+os.sep+'backup'+time.strftime('%Y%m%d%H%M%S')+'.zip'

zip_command = "zip -qr {0} {1}".format(target,' '.join(source))

if os.system(zip_command) == 0:
    print('Резервная копия успешно сделана в', target)
else:
    print('Создание резервной копии не удалось')
