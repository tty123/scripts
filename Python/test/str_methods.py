#! /usr/bin/env python3
#===============================================================================
#
#         FILE: str_methods.py
#
#        USAGE: ./str_methods.py
#
#  DESCRIPTION: ---
#
#      OPTIONS: ---
# REQUIREMENTS: ---
#         BUGS: ---
#        NOTES: ---
#       AUTHOR: Tolmachev V.N.
# ORGANIZATION:
#      VERSION: 1.0
#      CREATED: 26.11.2019  12:52
#     REVISION: ---
#===============================================================================

name = 'Swaroop'
if name.startswith('Swa'):
    print('Да строка начинается на "Swa"')
if 'a' in name:
    print('Да, она содержит строку "a"')
if name.find('war') != -1:
    print('Да она содержит строку "war"')
delimiter = '_*_'
mylist=['Бразилия','Индия','Россия','Китай']
print(delimiter.join(mylist))
