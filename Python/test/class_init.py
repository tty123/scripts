#! /usr/bin/env python3
#===============================================================================
#
#         FILE: class_init.py
#
#        USAGE: ./class_init.py
#
#  DESCRIPTION: ---
#
#      OPTIONS: ---
# REQUIREMENTS: ---
#         BUGS: ---
#        NOTES: ---
#       AUTHOR: Tolmachev V.N.
# ORGANIZATION:
#      VERSION: 1.0
#      CREATED: 27.11.2019  15:35
#     REVISION: ---
#===============================================================================

class Person:
    def __init__(self,name):
        self.name=name
    def sayHi(self):
        print('Hi! My name is Turgor!', self.name)
p=Person('Swaroop')
p.sayHi()

