#!/usr/bin/env python3

import sys

print("Аргументы команндной строки:")
for i in sys.argv:
	print (i)
print('\n\nПеременная PYTHONPATH содержит', sys.path, '\n')

# или делаем импорт всех переменных из модуля

from math import *  # импорт всех публичных имен модуля

