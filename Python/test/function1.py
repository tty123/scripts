#!/usr/bin/env python3

def sayHello(name):
	print ('Привет мир!', name)

sayHello('Dan')
sayHello('Вася')

x=10
def func_outer():
	x=2
	print('x =', x)
	def func_inner():
		nonlocal x
		x=5
	func_inner()
	print ('Локальное x сменилось на', x)
func_outer()
print ("Глобальный x равен", x)

#значения по умолчанию
def say (message, times=1):
	print (message*times)

say('Привет')
say('Вася Пельмень\n', 5)

#ключевые параметры
def func(a, b=5, c=10):
	print ('а равно',a,',b равно',b,', c равно',c)

func(3,7)
func(25,c=24)
func(c=50,a=100)

#переменное число параметров
def total(initial=5, *numbers, **keywords):
	count = initial
	for number in numbers:
		count += number
	for key in keywords:
		count += keywords[key]
	return count
print (total(10,1,2,3, vegetables=50, fruits = 100))

#только ключевые параметры
def total(initial=5, *numbers, extra_numbers):
	count = initial
	for number in numbers:
		count +=number
	count += extra_numbers
	print (count)
total(10,1,2,3,extra_numbers=45)
total(10,1,2,3)
	
