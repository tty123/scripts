#! /usr/bin/env python3
#===============================================================================
#
#         FILE: method.py
#
#        USAGE: ./method.py
#
#  DESCRIPTION: ---
#
#      OPTIONS: ---
# REQUIREMENTS: ---
#         BUGS: ---
#        NOTES: ---
#       AUTHOR: Tolmachev V.N.
# ORGANIZATION:
#      VERSION: 1.0
#      CREATED: 26.11.2019  17:17
#     REVISION: ---
#===============================================================================

class Person:
    def sayHi(self):
        print('Привет! Как дела?')

p=Person()
p.sayHi()

