#! /usr/bin/env python3
#===============================================================================
#
#         FILE: seq.py
#
#        USAGE: ./seq.py
#
#  DESCRIPTION: ---
#
#      OPTIONS: ---
# REQUIREMENTS: ---
#         BUGS: ---
#        NOTES: ---
#       AUTHOR: Tolmachev V.N.
# ORGANIZATION:
#      VERSION: 1.0
#      CREATED: 26.11.2019  12:14
#     REVISION: ---
#===============================================================================

shoplist = ['яблоки', 'манго', 'морковь', 'бананы']
name = 'swaroop'
print ('Элемент 0 -', shoplist[0])
print ('Элемент 1 -', shoplist[1])
print ('Элемент 2 -', shoplist[2])
print ('Элемент 3 -', shoplist[3])
print ('Элемент -1 -', shoplist[-1])
print ('Элемент -2 -', shoplist[-2])
print ('Символ 0 -', name[0])

#Вырезка из списка
print ('Элементы с 1-3 :', shoplist[0])
print ('Элементы с 2 до конца:', shoplist[2:])
print ('Элементы с 1 по -1 :', shoplist[1:-1])
print ('Элементы от начала до конца:', shoplist[:])
print ('Элемент 0 :', shoplist[0])

#Вырезка из строки
print ('Символы с 1 по 3:', name[1:3])
print ('Символы с 2 до конца:', name[2:])
print ('Символы с 1 по -1:', name[1:-1])
print ('Символы от начала до конца:', name[:])
