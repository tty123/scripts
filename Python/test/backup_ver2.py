#! /usr/bin/env python3
#===============================================================================
#
#         FILE: backup_ver2.py
#
#        USAGE: ./backup_ver2.py
#
#  DESCRIPTION: скрипт бэкапа
#
#      OPTIONS: ---
# REQUIREMENTS: ---
#         BUGS: ---
#        NOTES: ---
#       AUTHOR: Tolmachev V.N.
# ORGANIZATION:
#      VERSION: 1.0
#      CREATED: 26.11.2019  15:07
#     REVISION: ---
#===============================================================================

import os
import time

# сохраняемые файлы и каталоги
source=['/home/tvn/Scripts', '/home/tvn/.bashrc']

# директория сохранения
target_dir='/home/tvn/backup'

# имя создаваемой директории
today = target_dir + os.sep + time.strftime('%Y%m%d')

# часть имени файла, содер
now = time.strftime('%H%M%S')

if not os.path.exists(today):
    os.mkdir(today)
    print('Каталог успешно создан', today)

target=today+os.sep+'backup'+now+'.zip'

zip_command = "zip -qr {0} {1}".format(target,' '.join(source))

if os.system(zip_command) == 0:
    print('Резервная копия успешно сделана в', target)
else:
    print('Создание резервной копии не удалось')
