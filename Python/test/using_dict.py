#! /usr/bin/env python3
#===============================================================================
#
#         FILE: using_dict.py
#
#        USAGE: ./using_dict.py
#
#  DESCRIPTION: тестируем словарь
#
#      OPTIONS: ---
# REQUIREMENTS: ---
#         BUGS: ---
#        NOTES: ---
#       AUTHOR: Tolmachev V.N.
# ORGANIZATION:
#      VERSION: 1.0
#      CREATED: 25.11.2019  17:22
#     REVISION: ---
#===============================================================================

ab={ 'Swaroop' : 'swarooop@swaropa.com',
      'Larry' : 'larry@wall.com',
      'Matsumoto' : 'matz@matsum.com',
      'Spammer' : 'spammer@sper.org',
     }
print ("Адрес Swaroop-a:", ab['Swaroop'])
del ab['Spammer']
print ('\nВ адресной книге {0} контактов\n'.format(len(ab)))

for name, address in ab.items():
       print ('Контакт {0} с адресом {1}'.format(name, address))
# Добавление пары ключ -значение
ab['Guido'] = 'guido@fg.rm'
if 'Guido' in ab:
       print("\nАдресс Гуайдо:", ab['Guido'])




