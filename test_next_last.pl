#!/usr/bin/env perl
#===============================================================================
#
#         FILE: test_next_last.pl
#
#        USAGE: ./test_next_last.pl  
#
#  DESCRIPTION: 
#
#      OPTIONS: ---
# REQUIREMENTS: ---
#         BUGS: ---
#        NOTES: ---
#       AUTHOR: Tolmachev V.N. (), 
# ORGANIZATION: 
#      VERSION: 1.0
#      CREATED: 07.05.2020 14:20:28
#     REVISION: ---
#===============================================================================

use strict;
use warnings;
use utf8;
use 5.010;

if (1){
	say "in block";
	#last; #not worked for blocks, only in loop
	#next; #not worked for blocks, only in loop
	break ;
	say "last not worked";
} else { say "runing else"}; 

M1: say "OK";
?
