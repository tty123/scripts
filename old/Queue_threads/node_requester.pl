#!/usr/bin/env perl
#===============================================================================
#
#         FILE: node_requester.pl
#
#        USAGE: ./node_requester.pl  
#
#  DESCRIPTION: скрипт опрашивающий по SNMP коммутаторы узла
#
#      OPTIONS: ---
# REQUIREMENTS: ---
#         BUGS: ---
#        NOTES: ---
#       AUTHOR: Tolmachev V.N. (), 
# ORGANIZATION: 
#      VERSION: 1.0
#      CREATED: 17.05.2019 11:02:13
#     REVISION: ---
#===============================================================================

use strict;
use warnings;
use utf8;

use FindBin qw($Bin); # юзы для моего модуля
use lib $Bin;
use igator; 

use DBI();
use DBD::Pg;

use threads;
use Thread::Queue;
#use threads::shared;

use Socket;
use IO::Socket::INET;
#use AnyEvent;
#use AnyEvent::Ping;
#use AsyncPing;
#use AnyEvent::Run;
#use AnyEvent::Socket;
#use AnyEvent::FastPing;
#use AnyEvent::SNMP;

use Net::SNMP;
use Net::IP;
use Net::IP::Match::XS;

use Devel::Timer;
use Data::Printer;
use Data::Dumper;
use Storable;

#----------------------------------------------------------
#  заполнить для подключения к базе 
#----------------------------------------------------------
my $host = '172.17.31.14'; # 172.17.31.13 ++++++++++++++ change
my $db_name = "utm";
my $port=5440;
my $user = "bfork";
my $pass = "nw47g979wh4t94k";

#---------------------------------------------------------
#  Параметры узла.  Адрес: г.Владивосток ул. Некрасовская д. 76с3 п. 1 № 1 (3841) Cisco 6509   (3849) Cisco 6509 
#---------------------------------------------------------
my $node_ID = ''; # уникальный идентификатор узла
my @node_subsets = ('10.240.250.0/24','10.240.0.0/22','10.240.4.0/22','10.240.12.0/22',
'10.240.60.0/22','10.243.4.0/22'); # список подсетей принадлежащих узлу
my @node_ipm; # массив ip-адресов принадлежащих узлу
my @accepted_node_ipm; # очищенный список адресов (только то, что нужно)
#---------------------------------------------------------
# параметры SNMP
#---------------------------------------------------------
my $community = 'public';

#---------------------------------------------------------
my @snmp_response;
my @response;
#my %main_hsh :shared;
#----------------------------------------------------------

#==== Работа с базой данных ===============================
my $dbh = DBI->connect("DBI:Pg:dbname=$db_name; host=$host; port=$port", $user, $pass,{AutoCommit => 1}) 
		or die "Error connecting to database";
		$dbh->do("set application_name = 'perl_snmp';");
DBI->trace(1);
#----------------------------------------------------------

################# создаём массив ip адресов из базы #################################################
my @ipm; # массив ip адресов полученный из файла
my $ip_a;
my $query = $dbh->prepare("SELECT ip_address FROM switch_map.swp0;");
			  $query->execute;
		$query->bind_col(1,\$ip_a);
while ($query->fetch){ push (@ipm, $ip_a);
                                 } 
               $query->finish;
		$query = $dbh->disconnect;
p @ipm; #--test
#----------------------------------------------------------------------------------
################ создаём массив @node_ipm адресов для даннного узла  #################################
foreach (@ipm) {
	if (match_ip($_, @node_subsets)) { push(@node_ipm, $_);}
};
p @node_ipm; #--test#

#-------------- проверяем доступность адресов, отделяем молчащих  -------------------------------------
my $t = Devel::Timer -> new();
$t -> mark ('start pinging');
my ($ref_ipm_pinged, $ref_ipm_notpinged) = igator -> ping_ipm (\@node_ipm);
my @node_ipm_pinged = @$ref_ipm_pinged;
my @node_ipm_notpinged = @$ref_ipm_notpinged;

p @node_ipm_pinged;
p @node_ipm_notpinged;

$t -> mark ('end pinging');
$t -> report();

#----- забираем модель устройства для каждого адреса  -----

# получаем ссылку на хэш с vendor_oid и массив подвисших моделей, которые пингуются, но не отдают по SNMP 
my ($vendor_ipm_snmp, $ip_snmp_undef) = igator -> get_vendor_id (@node_ipm_pinged);

p $vendor_ipm_snmp;
p $ip_snmp_undef;
my $number = scalar @node_ipm; 
print "all node ip numbers = $number \n";
my $size = scalar keys %{$vendor_ipm_snmp};
print "vendor_oid_ip_numbers = $size\n"; 

#----- готовим финальный массив адресов, пропустив через список accepted vendor ID -----
my ($accepted_vendor_ip, $blocked_vendor_ip, $new_vendor_ip) = igator -> sort_vendor_id ($vendor_ipm_snmp);

p $accepted_vendor_ip;
p $blocked_vendor_ip;
p $new_vendor_ip;

#------ отрабатываем многопоточный сбор данных ------
#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
my $t = Devel::Timer -> new();
$t -> mark ('start');
my $q = Thread::Queue -> new();

my @acc_ipm = keys %$accepted_vendor_ip;
 splice (@acc_ipm,40);
my @main_data_hsh;
my @thr;
	$thr[0] = threads->new(\&queue_build_hash);  # поток обработки очереди
	my $threads =30; # задаём количество тредов
	my $num_thr = $#acc_ipm+2; # количество snmp потоков 
	while (@acc_ipm){
		
                 for (my $i=1; $i <= $threads; $i++){  # организовываем $threads потоков по каждому ip
			my $ip = pop (@acc_ipm);
			  $thr[$i] = threads->new ( sub {
	my $res_hsh = igator -> get_ip_data ($ip, $accepted_vendor_ip);
print "\$res_hsh = $res_hsh\n";
		if (defined($res_hsh)){$q -> enqueue($res_hsh);} # кидаем данные в очередь
print "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@\n";
#		p $res_hsh
			});

	#	while (my ($descr, $other) = each %temp_hsh){
	#		while (my ($oid,$ref_val) = each %$other){
	#			while (my($key,$value) = each %$ref_val){
	#				$main_data_hsh{$ip}{$descr}{$oid}{$key}=$value}}}
		}
			for (my $i=1; $i <= $threads; $i++){
		        $thr[$i]->join();
			}
	$num_thr = $num_thr - $threads;
	print "-------------- $num_thr -------------------------------------------------\n";
	if ($num_thr<$threads){$threads=$num_thr}
	}
$q -> enqueue('end');# маркируем конец очереди
$thr[0] -> join(); # дожидаемся обработки очереди
	my $ref_data = $q -> dequeue();
#	my %super_hsh = %$ref_data;
	my @super = @$ref_data;

p @super;
$t -> mark ('end ');
$t -> report();
print "$#acc_ipm num of ip adresses\n";

#---------------- очередь --------------------
sub queue_build_hash {
	my $q_data;
	my @data;
	my %main_hsh;
	my %temp;
	while ($q_data ne 'end'){
		 $q_data = $q -> dequeue();
	#	if ($q_data ne 'end'){%temp = %$q_data; %main_hsh = (%main_hsh, %temp);}
		if ($q_data ne 'end'){ push(@data, %$q_data);}
	}
	
		$q -> enqueue(\@data); # кидаем данные в очередь
return (\%main_hsh);
}
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

##----- фаза сборки данных ------
#my $t = Devel::Timer -> new();
#$t -> mark ('start node_investigate');
#my %time_data; # сбор данных
#my $ref_time_data = igator -> node_investigate ($accepted_vendor_ip, $ip_snmp_undef);
#store $ref_time_data , 'node_requester_data.txt';
#$t -> mark ('end node_investigate');
#$t -> report();
#my $hashref = retrieve ('node_requester_data.txt');
##p $hashref;
#print Dumper  %super_hsh;
store \@super, 'node_req_data_super.txt';
#p $ref_time_data;
############# разделяем адреса узла на 2 группы accepted и undefined  #####################
#my %node_info = %{res};
#my %accepted_
#my %undef_vendor = ;










