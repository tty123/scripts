#!/usr/bin/env perl
#===============================================================================
#
#         FILE: node_requester.pl
#
#        USAGE: ./node_requester.pl  
#
#  DESCRIPTION: скрипт собирающий SNMP данные с коммутаторов узла
#
#      OPTIONS: ---
# REQUIREMENTS: ---
#         BUGS: ---
#        NOTES: ---
#       AUTHOR: Tolmachev V.N. (), 
# ORGANIZATION: 
#      VERSION: 1.2
#      CREATED: 8.07.2019 10:02:13
#     REVISION: ---
#===============================================================================

use strict;
use warnings;
use utf8;

use threads;
use threads qw(yield);
use Thread::Queue;
#use threads::shared;

use FindBin qw($Bin); # юзы для моего модуля
use lib $Bin;
use igator; 

use DBI();
use DBD::Pg;


use JSON;
use ClickHouse;

use Socket;
use IO::Socket::INET;
use AnyEvent;
#use AnyEvent::Ping;
#use AsyncPing;
#use AnyEvent::Run;
#use AnyEvent::Socket;
#use AnyEvent::FastPing;
#use AnyEvent::SNMP;

use Net::SNMP;
use Net::IP;
use Net::IP::Match::XS;

use Devel::Timer;
use Data::Printer;
use Data::Dumper;
use Storable;

$|=1; #отключаем буферизацию вывода

my $queue_IN = Thread::Queue -> new();  # очередь входных данных (ip) для потоков обработки
my $queue_OUT = Thread::Queue -> new(); # очередь с SNMP данными по каждому IP
my $queue_RES = Thread::Queue -> new(); # очередь для конечного результата, сервисные сигналы



sub snmp_worker {	
	while (1) {
	my $ipo = $queue_IN -> dequeue();
p $ipo;	
		if (defined $ipo){
			if ($ipo->[0] eq 'start'){$queue_OUT -> enqueue($ipo); print "START!\n";}	
				elsif ($ipo->[0] eq 'end'){$queue_OUT -> enqueue($ipo); print "END!\n";}	
					elsif ($ipo->[0] eq 'ip_oid'){ 
					my $ipo_data = $ipo->[1];
p $ipo_data;
					my $res_hsh = igator -> get_ip_data($ipo_data);
					print "\$res_hsh = $res_hsh\n";
					if (defined($res_hsh)){$queue_OUT -> enqueue($res_hsh);} # кидаем данные в очередь
					}
		}
	}
}
#---- Запускаем основные потоки выполнения (SNMP) -----------
my $worker_numbers = 30;
my @snmp_wkr;
	for (my $i=0; $i < $worker_numbers; $i++){ 
	$snmp_wkr[$i] = threads -> new (\&snmp_worker);
			};
#---------------------------------------------------

sub queue_worker {
	my $ip_numbers;
	my $q_data;
	my @data;
	while (1) {
	my $q_data = $queue_OUT -> dequeue();
#p $q_data;
		if (defined $q_data){
			if ($q_data->[0] eq 'start'){ $ip_numbers = (); 
							@data = (); 
							$queue_RES -> enqueue('start_ok');
							}	
				elsif ($q_data->[0] eq 'end'){
							my @arr = ('end_ok', \@data);
							$queue_RES -> enqueue(\@arr);
								}	
					elsif ($q_data->[0] eq 'ip_oid') {	
						my @hip; @hip =  keys (%$q_data);
						my $json = encode_json ($q_data->{$hip[0]});
						my %new_hsh = ($hip[0] => $json);
						push(@data, \%new_hsh); # здесь формируем json данных по ip
						$queue_RES -> enqueue('1'); # значение для счётчика
						}
		}
	undef $q_data;
	}
}
#---- Запускаем поток агрегации очереди SNMP данных
my $queue_wkr = threads -> new (\&queue_worker); 
#--------------------------------------------------

sub run_cycle_worker {
my $t = Devel::Timer -> new();
$t -> mark ('start');
	while (1) {
	#---------------------------------------------------------
	#  Параметры узла.  Адрес: г.Владивосток ул. Некрасовская д. 76с3 п. 1 № 1 (3841) Cisco 6509   (3849) Cisco 6509 
	#---------------------------------------------------------
	my $node_ID = 'vld_nekr_76s3'; # уникальный идентификатор узла
	my @node_subsets = ('10.240.250.0/24','10.240.0.0/22','10.240.4.0/22','10.240.12.0/22',
	'10.240.60.0/22','10.243.4.0/22'); # список подсетей принадлежащих узлу
	my $ch = ClickHouse->new(                  
	database => 'snmpdb',
	host     => '109.126.3.21',
	user	 => 'default',
	port     => 8123,
	) or die "Error connecting clickhouse database" ;
	#==== Процедура временной синхронизации ==================
	my ($past_time_mark, $recent_time_mark, $delay_time) = igator -> time_mngmt();
	$ch->do("INSERT INTO snmpdb.node_state (time, node, state) VALUES (now(), '$node_ID', 'idle')");
	print "past time mark ($past_time_mark), waiting $delay_time sec. for  new time mark ($recent_time_mark)\n";
	sleep($delay_time);
	$ch->do("INSERT INTO snmpdb.node_state (time, node, state) VALUES (now(), '$node_ID', 'starting')");
	print "recent time mark ($recent_time_mark), running snmp data collecting...\n";
	#------------------------------------#
	#  заполнить для подключения к базе  # 
	#------------------------------------#
	my $host = '172.17.31.14'; # 172.17.31.13 ++++++++++++++ change
	my $db_name = "utm";
	my $port = 5440;
	my $user = "bfork";
	my $pass = "nw47g979wh4t94k";
	#==== Работа с базой данных ===============================
	my $dbh = DBI->connect("DBI:Pg:dbname=$db_name; host=$host; port=$port", $user, $pass,{AutoCommit => 1}) 
			or die "Error connecting to postgresql database";
			$dbh->do("set application_name = 'perl_node_requester';");
	DBI->trace(1);
	################# создаём массив ip адресов из базы #################################################
	my @ipm; # массив ip адресов полученный из файла
	my $ip_a;
	my $query = $dbh->prepare("SELECT ip_address FROM switch_map.swp0;");
				  $query->execute;
			$query->bind_col(1,\$ip_a);
	while ($query->fetch){ push (@ipm, $ip_a);
					 } 
		       $query->finish;
			$query = $dbh->disconnect;
	p @ipm; #--test
	#----------------------------------------------------------------------------------
	################ создаём массив @node_ipm адресов для даннного узла  #################################
	my @node_ipm = (); # массив ip-адресов принадлежащих узлу
	foreach (@ipm) {
		if (match_ip($_, @node_subsets)) { push(@node_ipm, $_);}
	};
	p @node_ipm; #--test#
	#-------------- проверяем доступность адресов, отделяем молчащих  -------------------------------------
	my $tt = Devel::Timer -> new();
	$tt -> mark ('start pinging');
	my ($ref_ipm_pinged, $ref_ipm_notpinged) = igator -> ping_ipm (\@node_ipm);
	my @node_ipm_pinged = @$ref_ipm_pinged;
	my @node_ipm_notpinged = @$ref_ipm_notpinged;

	p @node_ipm_pinged;
	p @node_ipm_notpinged;
	$tt -> mark ('end pinging');
	$tt -> report();

	#----- забираем модель устройства для каждого адреса  -----

	# получаем ссылку на хэш с vendor_oid и массив подвисших моделей, которые пингуются, но не отдают по SNMP 
	my ($vendor_ipm_snmp, $ip_snmp_undef) = igator -> get_vendor_id (@node_ipm_pinged);

	p $vendor_ipm_snmp;
	p $ip_snmp_undef;

	my $number = scalar @node_ipm; 
	print "all node ip numbers = $number \n";
	my $size = scalar keys %{$vendor_ipm_snmp};
	print "vendor_oid_ip_numbers = $size\n"; 

	#----- готовим финальный массив адресов, пропустив через список accepted vendor ID -----
	my ($accepted_vendor_ip, $blocked_vendor_ip, $new_vendor_ip) = igator -> sort_vendor_id ($vendor_ipm_snmp);
	print "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\n";
	#p $accepted_vendor_ip;
	#p $blocked_vendor_ip;
	#p $new_vendor_ip;
	print "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\n";
	
	#---- Взводим поток обработки ----
	#my $ip_acc_numbers = scalar keys %$accepted_vendor_ip; 
	my $ip_acc_numbers = 0;
	my @start=('start');  
	$queue_IN -> enqueue (\@start);
	while (1){
		my $res = $queue_RES -> dequeue();
		if ($res eq 'start_ok' ) {undef $res; last;} # если поток обработки очереди ответил, едем дальше
	}
	
	#---- заряжаем входную очередь ip отфильтрованными адресами -----
	while (my ($ip, $v_oid) = each %$accepted_vendor_ip){
		#my @ip_oid = ($ip, $v_oid);
		my @qu_data = ('ip_oid', \($ip,$v_oid));

p @qu_data;
		if ((defined $ip)&&(defined $v_oid)){
			$queue_IN -> enqueue (\@qu_data);
			$ip_acc_numbers++;
			}
	#---- цикл подсчёта обработанных потоком обработки адресов ----
	#---- и выдача команды на получение обобщённых данных ----
		my $i=0;
		while ( $i != $ip_acc_numbers){            # цикл счётчика
			my $res = $queue_RES -> dequeue();   # пока не обработаются
			if ($res eq '1' ) {undef $res; $i++;};	     # все ip адреса
		}
		my @end = ('end');
		$queue_IN -> enqueue (\@end);
		my $main_dat;             # ссылка на общий массив агрегированнных данных
		while (1){
			my $res = $queue_RES -> dequeue();
			if ($res->[0] eq 'end_ok'){
				$main_dat=$res->[1]; 
				last;
				}
		}

	#---- формируем и отправляем полученные данные в clickhouse ----
	#++++ данные по ping-ам ip адресов 
	my @str;
	my @arrstr;
		foreach (@node_ipm_notpinged) {
			@str=($recent_time_mark, $node_ID, $_, 'not pinged');
			push(@arrstr, [@str]);
			}
	#++++ состояние по зависшим, исключённым и новым устройствам 

		foreach ( @$ip_snmp_undef) {
			@str = ( $recent_time_mark, $node_ID, $_, 'not snmp'); 
			push(@arrstr, [@str]);
			}
		foreach (keys (%$blocked_vendor_ip)) {
			@str = ( $recent_time_mark, $node_ID, $_, 'excluded');
			push(@arrstr, [@str]);
			}
		foreach (keys (%$new_vendor_ip)) {
			@str = ($recent_time_mark, $node_ID, $_, 'new model');
			push(@arrstr, [@str]);
			}
		$ch->do("INSERT INTO snmpdb.dataset (time, node, ip, condition) VALUES", @arrstr ); 
	#+++ snmp данные по ip
	@arrstr = undef;
	@str = undef;
		foreach (@$main_dat) {
			my %ip_hash=%$_;
			my @ip = keys %ip_hash;
			my $ip_json = $_->{$ip[0]};
			@str = ( $recent_time_mark, $node_ID , $ip[0], $ip_json, 'ok');  # @str содержит ip и  json для инсерта 
			push(@arrstr, [@str]);
			}
		$ch->do("INSERT INTO snmpdb.dataset (time, node, ip, data, condition) VALUES", @arrstr);
		$ch->do("INSERT INTO snmpdb.node_state (time, node, state) VALUES (now(),'$node_ID', 'ending')"); # нода закончила работать
		$ch->disconnect;
		undef $main_dat;		
	#---- 
		}
	}
$t -> mark ('ended');
$t -> report();
}

#---- Запускаем поток основного цикла выполнения и управления -----
my $run_cycle_wkr = threads -> new (\&run_cycle_worker); 
#--------------------------------------------------


# таймер для того, чтобы в случае, если у нас отвалится один из тредов - перезагрузить его автоматом, а не ресетить всю прогу
my $thread_timer = AE::timer 10, 10, sub 
{
    if ($run_cycle_wkr->is_running()) 
    {   
        print "Cycle worker run\n";
    }   
    else 
    {   
        print "Cycle worker start_new\n"; 
	$run_cycle_wkr = threads -> new (\&run_cycle_worker); 
    }   
    if ($queue_wkr->is_running()) 
    {   
        print "Queue worker run\n";
    }   
    else 
    {   
        print "Queue worker start_new\n";
	$queue_wkr = threads -> new (\&queue_worker);
    }   

	for (my $i=0; $i < $worker_numbers; $i++){ 
	if ($snmp_wkr[$i]->is_running()){
		print "snmp worker [$i] run\n";
		} else {
	$snmp_wkr[$i] = threads -> new (\&snmp_worker);
			}
		};
};

    

AE::cv->recv;

for (my $i=0; $i < $worker_numbers; $i++){ 
$snmp_wkr[$i]->detach();
		};
$queue_wkr->detach();
$run_cycle_wkr->detach();




#------ отрабатываем многопоточный сбор данных ------
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++e


 #goto START; # повторяем всю программу


###--------------- синхронизатор - time management--------------
##sub time_mngt {
##	my $current_time = time();
##	my $time_mark = (int($current_time/300))*300; # пройденная временная метка
##	my $new_time_mark = $time_mark + 300; # следующая временная метка
##	my $delay_time = $current_time - $time_mark; # время до старта
##return ($time_mark, $new_time_mark, $delay_time);
##}
###+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

#---- Собранные данные нужно завернуть в JSON и отправить -------------------------
#my $json;
#if (@dat) {$json = encode_json(\@dat);}
#p $json;


##----- фаза сборки данных ------
#my $t = Devel::Timer -> new();
#$t -> mark ('start node_investigate');
#my %time_data; # сбор данных
#my $ref_time_data = igator -> node_investigate ($accepted_vendor_ip, $ip_snmp_undef);
#store \@dat , 'node_requester_data.txt';
#$t -> mark ('end node_investigate');
#$t -> report();
#my $hashref = retrieve ('node_requester_data.txt');
##p $hashref;
#print Dumper  %super_hsh;
 #store \%super_hsh, 'node_req_data_super.txt';
#p $ref_time_data;

