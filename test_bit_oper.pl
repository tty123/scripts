#!/usr/bin/env perl
#===============================================================================
#
#         FILE: test_bit_oper.pl
#
#        USAGE: ./test_bit_oper.pl  
#
#  DESCRIPTION: 
#
#      OPTIONS: ---
# REQUIREMENTS: ---
#         BUGS: ---
#        NOTES: ---
#       AUTHOR: Tolmachev V.N. (), 
# ORGANIZATION: 
#      VERSION: 1.0
#      CREATED: 23.03.2020 15:25:36
#     REVISION: ---
#===============================================================================

use strict;
use warnings;
use utf8;
use Data::Printer;
use 5.010;

my $bt=2;
#say vec($bt,,8);
my $mask=0000000; #битовая маска
my $bin_num = sprintf ("%b", $bt);
while (length($bin_num)<8){
	$bin_num = '0'.$bin_num; # добавляем нужное количество 0, чтобы получить битовый вектор
}

say $bin_num;
