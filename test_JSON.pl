#!/usr/bin/env perl
#===============================================================================
#
#         FILE: test_JSON.pl
#
#        USAGE: ./test_JSON.pl  
#
#  DESCRIPTION: 
#
#      OPTIONS: ---
# REQUIREMENTS: ---
#         BUGS: ---
#        NOTES: ---
#       AUTHOR: Tolmachev V.N. (), 
# ORGANIZATION: 
#      VERSION: 1.0
#      CREATED: 12.03.2019 11:08:57
#     REVISION: ---
#===============================================================================

use strict;
use warnings;
use utf8;
use diagnostics;
use JSON::XS;
use Data::Dumper;
use Data::Printer;
use 5.010;

my $json_data='{
  "href" : "http://localhost/profiles",
  "items" : [
    {
      "href" : "http://localhost/id111",
      "Child" : {
        "name" : "Jack",
        "age" : "11",
        "pet" : "Cat"
      }
    },
    {
      "href" : "http://localhost/id303",
      "Child" : {
        "name" : "David",
        "age" : "8",
        "pet" : "Dog"
      }
      },
      {
      "href" : "http://localhost/id516",
      "Child" : {
        "name" : "Merry",
        "age" : "10",
        "pet" : "Hamster"
      }
    }
  ]
}';

print Dumper(decode_json ($json_data));

my @test = (["ip" => "10.240.240.1", "port" =>"1"],["time"=>"1:40","port_state"=>"up","fac" => "2","sev"=>"3","mess" =>"testoviy log1"],["time"=>"1:50","port_state"=>"down","fac" => "4","sev"=>"5","mess" =>"testoviy log2"]);

my $FG = "$test[0] $test[1] $test[2]";
my $fr = encode_json(\@test);
print "$fr\n";

#------------------------------------------------
my @arr=([50,120], [100,211], [500,17],[1000, 51]);
say  encode_json(\@arr);

my %hsh=(
	one => [1,2,3],
	two => [4,5,6],
	three => [7,8,9],
);

say  encode_json(\%hsh);


my %hsh1=(
	one => 100,
	two => 200,
	three => 300,
	arr=> [[100,59],[200]],
	hsh=>{ 
		50=>112,
		100=>30,
		1000=>23,
	},
);

my $str1 = encode_json(\%hsh1);

say $str1;
say "---- from JSON -> to hsh ----";
my $hsh_from_JSON = decode_json($str1);
p $hsh_from_JSON;

say "________ for pay terminal________";
my %hsh2=(
	INCASSPERIODFROM => '10.02.2020',
	CURRENTDATETIME => '21.04.2020',
	bills_data => { 
		50=>112,
		100=>30,
		1000=>23,
	},
);

my $str2 = encode_json(\%hsh2);

say $str2;
say "---- from JSON -> to hsh ----";
my $hsh_from_JSON_term = decode_json($str2);
p $hsh_from_JSON_term;





































































































