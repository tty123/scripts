#!/usr/bin/env perl
#===============================================================================
#
#         FILE: test_length.pl
#
#        USAGE: ./test_length.pl  
#
#  DESCRIPTION: 
#
#      OPTIONS: ---
# REQUIREMENTS: ---
#         BUGS: ---
#        NOTES: ---
#       AUTHOR: Tolmachev V.N. (), 
# ORGANIZATION: 
#      VERSION: 1.0
#      CREATED: 13.03.2020 11:24:48
#     REVISION: ---
#===============================================================================

use strict;
use warnings;
use utf8;
use 5.010;

my $str="ff2345dde1e2f1f2a1a2";
say "$str  lenght (in dec) =".length($str);
say "lenght in hex = ". sprintf("%x", length($str));
