QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11

# The following define makes your compiler emit warnings if you use
# any Qt feature that has been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    ../../TestFirst/ccnet-cashcode/cashcodeprotocol.cpp \
    ../../TestFirst/ccnet-cashcode/ccpackage.cpp \
    ../../TestFirst/ccnet-cashcode/main.cpp \
    ../../TestFirst/ccnet-cashcode/serialport.cpp \
    main.cpp \
    mainwindow.cpp

HEADERS += \
    ../../TestFirst/ccnet-cashcode/cashcodeerrors.h \
    ../../TestFirst/ccnet-cashcode/cashcodeprotocol.h \
    ../../TestFirst/ccnet-cashcode/ccpackage.h \
    ../../TestFirst/ccnet-cashcode/commands.h \
    ../../TestFirst/ccnet-cashcode/serialport.h \
    mainwindow.h

FORMS += \
    mainwindow.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

SUBDIRS += \
    ../../TestFirst/ccnet-cashcode/CCNET.pro

DISTFILES += \
    ../../TestFirst/ccnet-cashcode/LICENSE \
    ../../TestFirst/ccnet-cashcode/README.md
