#!/usr/bin/env perl
use POSIX 'WNOHANG';
use strict;
use warnings;
use DBI ();
use DBD::Pg;
use Data::Dump qw(dump);
use Mojo::UserAgent;
use Mojo::JSON;
use FindBin qw($Bin);
use DateTime qw();

print "shswf_wc.pl libs loaded. Press Enter to begin script";
#my $inf = <STDIN>;

sub writesexlog {
    my ($text)=@_;
    my $wr="home/sorokin/scripts_sw/swsex.log";
    open FILE, ">>", $wr;
    print FILE $text;
    close(FILE);
  return;
}
my $sexhost=0;
$ENV{MOJO_MAX_MESSAGE_SIZE} = 10737418240;
$ENV{MOJO_MAX_MEMORY_SIZE} = 10737418240;
$ENV{MOJO_INACTIVITY_TIMEOUT} = 300;
#i think this check dont't need, because if lock file exists this script didn't load
if (-f "/scripts_sw/sw.lock") {
    print "ERROR! Previous main execute not correctly completed ! \n";
    print "Sending WARNING email to admins ... \n";
    print "Exiting ... \n ";
    exit(1);
};
print "NetUP UTM main: creating lock file ... \n";
open(LOCKFILE, ">/home/sorokin/scripts_sw/sw.lock");
print LOCKFILE $$;
close(LOCKFILE);
print "creating lock file - done";
#my $inf = <STDIN>;

my $ecc=0;
$ecc=$ARGV[0]||0;
print "\nargument of script shswf_wc.pl  net node = " . $ecc;
#my $inf = <STDIN>;

my %p = (
    1   => 'sw001f.pl',
    2   => 'sw001f.pl',
    3   => 'sw001f.pl',
    4   => 'sw004f.pl',
    5   => 'sw004f.pl',
    7   => 'sw001f.pl',
    8   => 'sw008f.pl',
    10  => 'sw001f.pl',
    15  => 'sw001f.pl',
    16  => 'sw001f.pl',
    17  => 'sw001f.pl',
    19  => 'sw001f.pl',
    21  => 'sw001f.pl',
    22  => 'sw001f.pl',
    23  => 'sw001f.pl',
    24  => 'sw001f.pl',
    26  => 'sw001f.pl',
    27  => 'sw001f.pl',
    29  => 'sw001f.pl',
    30  => 'sw001f.pl',
    31  => 'sw001f.pl',
    13  => 'sw013f.pl',
    25  => 'sw013f.pl',
    37  => 'sw013f.pl',
    41  => 'sw001f.pl',
    47  => 'sw001f.pl',
    59  => 'sw001f.pl',
    60  => 'sw001f.pl',
    122 => 'sw001f.pl'
);
#37 => 'sw013f.pl',
my %p2 = ();
my $json;# = Mojo::JSON;
my $ua; #   = Mojo::UserAgent;
print "\nstop";
my $inf = <STDIN>;
my $url = "http://127.0.0.1/obx/clearlog/";
if (my $t = $ua->get($url)->res->content->asset->slurp) {
    print dump $t;
}
clearpids();

sub trim {
    my $s = shift;
    $s =~ s/^\s+|\s+$//g;
    return $s;
};

sub envValues {
    my %env = ();
    open(ENVFILE, $Bin . "/.env");
    my @strings = <ENVFILE>;
    foreach my $line (@strings) {
        $line = trim($line);
        if (length($line) == 0 or substr($line, 0, 1) eq '#') {
            next;
        }
        ($a, $b) = split("=", $line, 2);
        $env{$a} = $b;
    }
    close(ENVFILE);
    return %env;
}
my %env = envValues();

my $DBI_data = "DBI:Pg:database=" . $env{'DB_DATABASE'} . ";host=" . $env{'DB_HOST'} . ";port=" . $env{'DB_PORT'} . ";";
my $dbh = undef;
reusecc();
sub reusecc {
    if (defined($dbh) && $dbh->ping) {
    }
    else {
        $dbh = DBI->connect_cached($DBI_data, $env{'DB_LOGIN'}, $env{'DB_PASSWORD'}, { AutoCommit => 1,
            Callbacks                                                                             => {
                connected => sub {
                    shift->do(q{set application_name = 'perl_sw_collector_get';});
                    return;
                },
            }
        });
    }
}
$dbh->do("INSERT INTO switch_history.added_work_logs (added_work_type_id) VALUES(28);");
$dbh->do("UPDATE switch_map.switch_place2 set demount=false where astate and demount and sw_id>0;");
$dbh->do("DELETE FROM dhcp.ipoe_sw_not;");
$dbh->do("DELETE FROM dhcp.ipoe_mult where usn=1;");

my $sql = "WITH RECURSIVE temp1(id, upper_sw, path, level) AS (
         SELECT t1.id,
            t1.upper_sw,
            t1.id AS path,
            0
           FROM switch_map.swp0 t1
          WHERE t1.upper_sw = 0 AND t1.l3 AND t1.id = ".$ecc."
        UNION
         SELECT t2.id,
            t2.upper_sw,
            temp1_1.path,
            temp1_1.level + 1
           FROM switch_map.swp0 t2
             JOIN temp1 temp1_1 ON temp1_1.id = t2.upper_sw
        )
SELECT row_number() over (order by CASE WHEN sww.model_id in (4,5) THEN 100 ELSE 1 END,sp2.down_no desc,sp2.id) as rownum,
    sww.model_id,sp2.id,sp2.down_no,sp2.ip_address, sp2.pass
    FROM switch_map.switch_place2 sp2 INNER JOIN switch_map.sw_warehouse sww ON sp2.sw_id=sww.id LEFT JOIN (SELECT net FROM dhcp.sw_net where c_id=".$ecc.") ff ON ff.net>>ip_address LEFT JOIN temp1 ON temp1.id=sp2.id
    where sp2.sw_id=sww.id and sww.model_id in (1,2,3,4,5,7,8,10,13,15,16,17,19,21,22,23,24,25,26,27,29,30,31,37,41,47,59,60,122) and sp2.astate 
    AND (CASE WHEN ".$ecc."=0 THEN true ELSE ff.net is not null END OR CASE WHEN ".$ecc."=0 THEN true ELSE temp1.id is not null END)
    ORDER BY CASE WHEN sww.model_id in (4,5) THEN 100 ELSE 1 END,sp2.down_no desc,sp2.id;";

my $data_all = $dbh->selectall_hashref($sql, 1);
if ($ecc>0) {
$sexhost=scalar(keys $data_all);
my $swr="start f cisco $ecc $sexhost dev ". DateTime->now->strftime('%Y-%m-%d %H:%M:%S')."\n"; 
writesexlog($swr);
}

$dbh->do("INSERT INTO switch_history.added_work_logs (added_work_type_id) VALUES(30);");
$dbh->disconnect;
#print dump $data_all;
{
    my $max_st = whateb();
    my $count_runned = 0;

    foreach my $dn (sort {$a <=> $b} keys $data_all) {
        #	print dump $$data_all{$dn};
        if (exists($p{$$data_all{$dn}{model_id}})) {
            my $cm = "/scripts_sw/" . $p{$$data_all{$dn}{model_id}} . " " . $$data_all{$dn}{id} . " " . $$data_all{$dn}{ip_address} . " " . $$data_all{$dn}{pass} . " " . $$data_all{$dn}{model_id} . " \&";
            #	  print $cm."\n";
            $$data_all{$dn}{work} = system($cm);
            $count_runned++;
            if ($count_runned % 1000 == 0) {
                reusecc();
                $dbh->do("INSERT INTO switch_history.added_work_logs (added_work_type_id) VALUES(35);");
                $dbh->disconnect;
            }
            $max_st = $max_st - 1;
            while ($max_st < 1) {
                sleep(2);
                clearpids();
                $max_st = whateb();
            }
        }
    }
}

{
    my $nono = whatebnono();
    if ($nono == 0) {
        sleep(10);
        clearpids();
    }
}

reusecc();
$dbh->do("INSERT INTO switch_history.added_work_logs (added_work_type_id) VALUES(34);");
$dbh->disconnect;

foreach my $dn (sort {$a <=> $b} keys $data_all) {
    #	print dump $$data_all{$dn};
    if (exists($p2{$$data_all{$dn}{model_id}})) {
        my $cm = "/scripts_sw/" . $p2{$$data_all{$dn}{model_id}} . " " . $$data_all{$dn}{id} . " " . $$data_all{$dn}{ip_address} . " " . $$data_all{$dn}{pass} . " " . $$data_all{$dn}{model_id} . " \&";
        #	  print $cm."\n";
        $$data_all{$dn}{work} = system($cm);
        my $n = whateb();
        while ($n == 0) {
            clearpids();
            sleep(2);
            $n = whateb();
        }
    }
}
sleep(120);
{
    my $nono = whatebnono();
    if ($nono == 0) {
        sleep(30);
        clearpids();
        $nono = whatebnono();
    }
}
#norm_exit();
reusecc();
$dbh->do("INSERT INTO switch_history.added_work_logs (added_work_type_id) VALUES(31);");
$sql = "SELECT nspname ||'.'||relname as tb FROM pg_class c LEFT JOIN pg_namespace n ON n.oid = c.relnamespace WHERE c.relkind = 'r'::\"char\" AND n.nspname = 'switch_tmp'::name;";
$data_all = $dbh->selectall_arrayref($sql, { Slice => {} });
foreach my $k (@$data_all) {
    $sql = "TRUNCATE TABLE " . $$k{'tb'};
    $dbh->do($sql);
}
$dbh->do("SELECT setval('switch_tmp.shfdb_id_seq', 1, true);");
$dbh->do("INSERT INTO switch_history.added_work_logs (added_work_type_id) VALUES(32);");
{
    opendir(DIR, "/tmpsw");
    my @DirContent = grep {!/^\.{1,2}$/} readdir(DIR);
    closedir(DIR);
    if (scalar(@DirContent) > 0) {
        foreach my $file (@DirContent) {
            print $file . "\n";
            open FILE, "</tmpsw/" . $file or die $!;
            $sql = "";
            while (<FILE>) {
                $sql = $sql . $_;
            }
            close(FILE);
            if ($sql ne "") {
                $dbh->do($sql);
            }
        }
    }
}
`/bin/rm -rf /tmpsw/*`;
#norm_exit();
$dbh->do("INSERT INTO switch_history.added_work_logs (added_work_type_id) VALUES(33);");
$url = "http://127.0.0.1/obx/getlog/";

system('rm -r '.$Bin.'/prev_log/');
system('cp -R /tmpsw/ '.$Bin.'/prev_log/');

if (my $t = $json->decode($ua->get($url)->res->content->asset->slurp)) {
    if (scalar(keys $t) > 0) {
        print dump $t;
        my $etim = $$t{t};
        opendir(DIR, "/tmpsw");
        my @DirContent = grep {!/^\.{1,2}$/} readdir(DIR);
        closedir(DIR);
        print dump \@DirContent;
        if (scalar(@DirContent) > 0) {
            foreach my $file (@DirContent) {
                if ($file =~ /^$etim\.(\S+)$/i) {
                    my %arry;
                    my $typesql = $1;
                    print $file . "\n";
                    $file = "/tmpsw/" . $file;
                    my %esid = %{do $file};
                    `/bin/rm -rf $file`;
                    foreach my $sid (keys %esid) {
                        if ($typesql eq "shsw") {
                            my $shsw = $esid{$sid};
                            foreach my $nname (keys $shsw) {
                                push(@{$arry{$typesql}}, "(" . $sid . ",'" . $nname . "','" . $$shsw{$nname} . "')");
                            }
                        }
                        elsif ($typesql eq "log") {
                            my $log = $esid{$sid};
                            #print dump $log;
                            foreach my $norec (keys $log) {
                                my $recf = $dbh->quote($$log{$norec}{rectext});
                                push(@{$arry{$typesql}}, "(" . $sid . ", " . $norec . ", '" . $$log{$norec}{stime} . "', " . $recf . ")");
                            }
                        }
                        elsif ($typesql eq "fdb") {
                            my $fdb = $esid{$sid};
                            #print dump $log;
                            foreach my $vlan (keys $fdb) {
                                foreach my $mac (keys $$fdb{$vlan}) {
                                    push(@{$arry{$typesql}}, "(" . $sid . ",'" . $vlan . "','" . $mac . "','" . $$fdb{$vlan}{$mac} . "')");
                                }
                            }
                        }
                        elsif ($typesql eq "port_1000") {
                            my $port_1000 = $esid{$sid};
                            #print dump $log;
                            foreach my $port (@$port_1000) {
                                push(@{$arry{$typesql}}, "(" . $sid . ", " . $$port{port} . ")");
                            }
                        }
                        elsif ($typesql eq "qbc") {
                            my $qbc = $esid{$sid};
                            #print dump $log;
                            foreach my $port (keys $qbc) {
                                foreach my $xz (keys $$qbc{$port}) {
                                    push(@{$arry{$typesql}}, "($sid, array[" . $port . "]::integer[], " . $xz . ", " . $$qbc{$port}{$xz} . ")");
                                }
                            }
                        }
                        elsif ($typesql eq "multicast_ports") {
                            my $multicast_ports = $esid{$sid};
                            #print dump $log;
                            foreach my $port (keys $multicast_ports) {
                                push(@{$arry{$typesql}}, "($sid,  array[" . $port . "]::integer[], '" . $$multicast_ports{$port}{mstate} . "', '" . $$multicast_ports{$port}{maccess} . "')");
                            }
                        }
                        elsif ($typesql eq "multicast_ports_rule") {
                            my $multicast_ports_rule = $esid{$sid};
                            #print dump $log;
                            foreach my $port (keys $multicast_ports_rule) {
                                foreach my $xz (keys $$multicast_ports_rule{$port}) {
                                    push(@{$arry{$typesql}}, "($sid, array[" . $port . "]::integer[], '" . $xz . "')");
                                }
                            }
                        }
                        elsif ($typesql eq "rule5") {
                            my $rule5 = $esid{$sid};
                            my $estrv = "(" . $sid . ", " . $$rule5{r10} . ", " . $$rule5{r20} . ", " . $$rule5{r30} . ")";
                            push(@{$arry{$typesql}}, $estrv);
                        }
                        elsif ($typesql eq "vlan") {
                            my $vlan = $esid{$sid};
                            #print dump $log;
                            foreach my $nvlan (keys $vlan) {
                                my $estrv = "(" . $sid . ", " . $nvlan . ", '" . $$vlan{$nvlan}{vname} . "', array[" . $$vlan{$nvlan}{tag} . "]::integer[], array[" . $$vlan{$nvlan}{untag} . "]::integer[], array[" . $$vlan{$nvlan}{igmp_router_ports} . "]::integer[], array[" . $$vlan{$nvlan}{igmp_forbid_router_ports} . "]::integer[])";
                                push(@{$arry{$typesql}}, $estrv);
                            }
                        }
                        elsif ($typesql eq "dhcp_local") {
                            my $dhcp_local = $esid{$sid};
                            my $estrv = "(" . $sid . ", array[" . $$dhcp_local{vlan} . "]::integer[], array[" . $$dhcp_local{p_keep} . "]::integer[], array[" . $$dhcp_local{p_replace} . "]::integer[], " . $$dhcp_local{en} . ", " . $$dhcp_local{address_binding_dhcp_snoop} . ", " . $$dhcp_local{address_binding_trap_log} . ",  array[" . $$dhcp_local{address_binding_ports} . "]::integer[])";
                            push(@{$arry{$typesql}}, $estrv);
                        }
                        elsif ($typesql eq "rule4") {
                            my $rule4 = $esid{$sid};
                            #print dump $log;
                            foreach my $nof (keys $rule4) {
                                my $estrv = "(" . $sid . ", " . $nof . ", '" . $$rule4{$nof}{ip} . "', '" . $$rule4{$nof}{port} . "', '" . $$rule4{$nof}{sp} . "', '" . $$rule4{$nof}{dp} . "')";
                                push(@{$arry{$typesql}}, $estrv);
                            }
                        }
                        elsif ($typesql eq "rulevlan") {
                            my $rule4 = $esid{$sid};
                            #print dump $log;
                            foreach my $nof (keys $rule4) {
                                my $estrv = "(" . $sid . ", " . $nof . ", '" . $$rule4{$nof}{vlan} . "', '" . $$rule4{$nof}{port} . "', '" . $$rule4{$nof}{speed} . "')";
                                push(@{$arry{$typesql}}, $estrv);
                            }
                        }
                        elsif ($typesql eq "loopdetect") {
                            my $loopdetect = $esid{$sid};
                            my $estrv = "(" . $sid . ", " . $$loopdetect{en_ld} . ", array[" . $$loopdetect{en_port} . "]::integer[], array[" . $$loopdetect{dis_port} . "]::integer[], " . $$loopdetect{en_stp} . ",'" . $$loopdetect{ver_stp} . "', array[" . $$loopdetect{sec_port} . "]::integer[])";
                            push(@{$arry{$typesql}}, $estrv);
                        }
                        elsif ($typesql eq "lacp") {
                            my $lacp = $esid{$sid};
                            #print dump $log;
                            foreach my $gid (keys $lacp) {
                                my $estrv = "(" . $sid . ", " . $gid . ", '" . $$lacp{$gid}{master_port} . "', array[" . $$lacp{$gid}{member_port} . "]::integer[], " . $$lacp{$gid}{status} . ")";
                                push(@{$arry{$typesql}}, $estrv);
                            }
                        }
                    }
                    my $gsql = "";
                    if ($typesql eq "shsw") {
                        $gsql = "INSERT INTO switch_tmp.shsw(swid, swopt, swvalue) VALUES " . join(',', @{$arry{$typesql}}) . ";";
                        #} elsif ($typesql eq "log") {
                        #$gsql = "INSERT INTO switch_tmp.sw_logging(swp_id, nrec, stime, rectext) VALUES  ".join(',',@{$arry{$typesql}}).";";
                    }
                    elsif ($typesql eq "fdb") {
                        $gsql = "INSERT INTO switch_tmp.shfdb(swid, vlan, mac_addr, port) VALUES " . join(',', @{$arry{$typesql}}) . ";";
                    }
                    elsif ($typesql eq "port_1000") {
                        $gsql = "INSERT INTO switch_tmp.sw_port_1000(swp_id, port) VALUES " . join(',', @{$arry{$typesql}}) . ";";
                    }
                    elsif ($typesql eq "qbc") {
                        $gsql = "INSERT INTO switch_tmp.sw_qbc(swp_id, port, quer, sp) VALUES " . join(',', @{$arry{$typesql}}) . ";";
                    }
                    elsif ($typesql eq "multicast_ports") {
                        $gsql = "INSERT INTO switch_tmp.sw_multicast_port(swp_id, mport, mstate, maccess) VALUES " . join(',', @{$arry{$typesql}}) . ";";
                    }
                    elsif ($typesql eq "multicast_ports_rule") {
                        $gsql = "INSERT INTO switch_tmp.sw_multicast_port_rule(swp_id, mport, mrule) VALUES " . join(',', @{$arry{$typesql}}) . ";";
                    }
                    elsif ($typesql eq "rule5") {
                        $gsql = "INSERT INTO switch_tmp.rule5(swp_id, r10, r20, r30) VALUES " . join(',', @{$arry{$typesql}}) . ";";
                    }
                    elsif ($typesql eq "vlan") {
                        $gsql = "INSERT INTO switch_tmp.shvlan(swp_id, vid, vname, tag, untag, igmp_router_ports, igmp_forbid_router_ports) VALUES " . join(',', @{$arry{$typesql}}) . ";";
                    }
                    elsif ($typesql eq "dhcp_local") {
                        $gsql = "INSERT INTO switch_tmp.dhcp_local_relay(swp_id, vlan, p_keep, p_replace, en, address_binding_dhcp_snoop, address_binding_trap_log, address_binding_ports) VALUES " . join(',', @{$arry{$typesql}}) . ";";
                    }
                    elsif ($typesql eq "rule4") {
                        $gsql = "INSERT INTO switch_tmp.rule4(swp_id, nof, ip, port, sp, dp) VALUES " . join(',', @{$arry{$typesql}}) . ";";
                    }
                    elsif ($typesql eq "rulevlan") {
                        $gsql = "INSERT INTO switch_tmp.rulevlan(swp_id, nof, vlan, port, sp) VALUES " . join(',', @{$arry{$typesql}}) . ";";
                    }
                    elsif ($typesql eq "loopdetect") {
                        $gsql = "INSERT INTO switch_tmp.sw_loopback(swp_id, en_ld, en_port, dis_port, en_stp, ver_stp, sec_port) VALUES " . join(',', @{$arry{$typesql}}) . ";\n";
                    }
                    elsif ($typesql eq "lacp") {
                        $gsql = "INSERT INTO switch_tmp.sw_lacp(swp_id, gid, master_port, member_port, status) VALUES " . join(',', @{$arry{$typesql}}) . ";";
                    }
                    if ($typesql eq "log") {
                        my %logpart;
                        my $lj = 0;
                        my $li = 1;
                        foreach my $k (@{$arry{$typesql}}) {
                            push(@{$logpart{$lj}}, $k);
                            $li++;
                            if ($li > 101) {
                                $lj++;
                                $li = 1;
                            }
                        }
                        foreach my $k (keys %logpart) {
                            $gsql = "INSERT INTO switch_tmp.sw_logging(swp_id, nrec, stime, rectext) VALUES  " . join(',', @{$logpart{$k}}) . ";";
                            my $ferr = 0;
                            $dbh->do($gsql) or $ferr = 1;
                            if ($ferr == 1) {
                                my $efg1 = "/scripts_sw/" . $typesql . "_" . time() . "_" . $k . ".tt";
                                open my $out, '>', $efg1;
                                print {$out} dump $dbh->errstr;
                                print {$out} "\n";
                                print {$out} dump $gsql;
                                print {$out} "\n";
                                close $out;
                            }
                        }
                    }
                    else {
                        my $ferr = 0;
                        $dbh->do($gsql) or $ferr = 1;
                        if ($ferr == 1) {
                            my $efg1 = "/scripts_sw/" . $typesql . "_" . time() . ".tt";
                            open my $out, '>', $efg1;
                            print {$out} dump $dbh->errstr;
                            print {$out} "\n";
                            print {$out} dump $gsql;
                            print {$out} "\n";
                            close $out;
                        }
                    }
                }
            }
        }
    }
}
$dbh->do("INSERT INTO switch_history.added_work_logs (added_work_type_id) VALUES(29);");
$dbh->disconnect;

$url = "http://127.0.0.1/obx/clearlog/";
if (my $t = $ua->get($url)->res->content->asset->slurp) {
    print dump $t;
    print "\n";
}

norm_exit();
sub norm_exit {
if ($ecc>0) {
my $swr="stop f cisco $ecc $sexhost dev ". DateTime->now->strftime('%Y-%m-%d %H:%M:%S')."\n"; 
writesexlog($swr);
}
    unlink("/scripts_sw/sw.lock");
    exit;
}

sub clearpids {
    opendir(DIR, "/scripts_sw/f.pid");
    my @DirContent = grep {!/^\.{1,2}$/} readdir(DIR);
    closedir(DIR);
    if (scalar(@DirContent) > 0) {
        foreach my $file (@DirContent) {
            if ($file =~ /\d+\.(\d+)\.pid/i) {
                my $exists = kill 0, $1;
                #print "\n".$exists."\n".$$."\n";
                if ($exists == 0) {unlink("/scripts_sw/f.pid/$file");}
            }
        }
    }
    return;
}
sub whateb {
    opendir(DIR, "/scripts_sw/f.pid");
    my @DirContent = grep {!/^\.{1,2}$/} readdir(DIR);
    closedir(DIR);
    my $i = 64 - scalar(@DirContent);
    return $i;
}

sub whatebnono {
    opendir(DIR, "/scripts_sw/f.pid");
    my @DirContent = grep {!/^\.{1,2}$/} readdir(DIR);
    closedir(DIR);
    if (scalar(@DirContent) > 0) {
        return 0;
    }
    else {
        return 1;
    }
}
