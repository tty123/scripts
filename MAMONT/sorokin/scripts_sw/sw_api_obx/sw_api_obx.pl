#!/usr/bin/env perl 
use POSIX qw(strftime);
use feature qw(switch say);
use Mojolicious::Lite;
#use LWP::UserAgent;
use DBI;
use utf8;
use Data::Dump qw(dump);
use strict;
use Mojo::JSON;

$ENV{MOJO_MAX_MESSAGE_SIZE} = 10737418240;
$ENV{MOJO_MAX_MEMORY_SIZE} = 10737418240;
$ENV{MOJO_INACTIVITY_TIMEOUT} = 300;
my $json;#  = Mojo::JSON->new;
my %loggg=();

############################################
get '/' => sub {
  my $self = shift;
  $self->render('index');
};

#########################################
any '/obx/log/:swid' => sub{
    my $self = shift;
    if (defined($self->param('swid')) && $self->param('swid')=~/^\d+$/i) {
      my $swid = int($self->param('swid'));
      if (my $hlog=$json->decode($self->req->content->asset->slurp)) {
	foreach my $k (keys $hlog) {
	    $loggg{$k}{$swid}=${$hlog}{$k};
	}
      }
    }
    $self->render(text =>"Ok");
};

any '/obx/clearlog' => sub{
    my $self = shift;
    undef(%loggg);
#    %loggg=undef;
    %loggg=();
    $self->render(text =>"Ok");
};

any '/obx/getlog' => sub{
    my $self = shift;
    my $t=time();
    foreach my $k (keys %loggg) {
        my $efg="/tmpsw/".$t.".".$k;
        open my $out, '>', $efg or die $!;
        print {$out} dump $loggg{$k};
        close $out;
    }
    my %ret;
    $ret{t}=$t;
    $self->render(json => \%ret);
};

#app->secret('Mojolicious rocks GGG fuck');
app->start;
