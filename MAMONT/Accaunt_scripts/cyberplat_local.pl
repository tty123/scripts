use strict;
use warnings;
use Data::Dump qw(dump);
use XML::Simple;
use Data::Dumper;
use DBI();
use DBD::Pg;

my %apor=%{do "/scripts/cyperplat.apor"};
my $work=0;
foreach my $k (keys %apor) {
    opendir(DIR, "$apor{$k}{dir}");
    @{$apor{$k}{cont}} = grep { /^ext-\d+\.pkt$/} readdir(DIR);
    closedir(DIR);
    if (scalar(@{$apor{$k}{cont}})>0) { $work=1;}
}
if ($work>0) {
    my $DBI_data="DBI:Pg:database=utm;host=pghost;";
    my $dbh = DBI->connect("$DBI_data","bfork","ggggg",{AutoCommit => 1});
    $dbh->do("set client_encoding to 'WIN1251'");
    foreach my $k (keys %apor) {
	if (scalar(@{$apor{$k}{cont}})>0) {
	    foreach my $file (@{$apor{$k}{cont}}) {
		print "$apor{$k}{dir}/$file\n";
		if (-e "$apor{$k}{dir}/$file") {
		    if (my $xs=XML::Simple->new()->XMLin("$apor{$k}{dir}/$file")) {
			my $user_id=${$xs}{'fields'}{'field100'};
			my $datex=${$xs}{'payment_create_dt'};
			my $amount=${$xs}{'fields'}{'AMOUNT_ALL'};
			if ($user_id>0){
			    my $ulogin="";
			    my $sth = $dbh->prepare("select ulogin from users where id=".$user_id." and deleted=0");
			    $sth->execute();
			    if ($sth->rows>0) {
				while (my $ips = $sth->fetchrow_hashref()) {
				    $ulogin=$ips->{'ulogin'};
				}
			    }
			    $sth->finish();
			    my $query = "insert into bills_history (ulogin, date, qnt, who, what, comments, qnt_currency, real_pay_date, uid, ofid) 
			    VALUES ('".$ulogin."', unix_timestamp(to_timestamp('".$datex."', 'DD.MM.YYYY HH24:MI:SS')), '".$amount."', 'local_cyber','payment','Local_Cyber','".$amount."', 
			    unix_timestamp(to_timestamp('".$datex."', 'DD.MM.YYYY HH24:MI:SS')), '".$user_id."', '".$apor{$k}{ofid}."')";
			    print $query."\n";
			    $dbh->do($query);
			    $query = "update users set bill=bill+'".$amount."' where id='".$user_id."';";
			    print $query."\n";
			    $dbh->do($query);
			    `rm $apor{$k}{dir}/$file`;
			}
		    }
		}
	    }
	}
    }
    $dbh->disconnect;
}


--------------------------
<?xml version="1.0" encoding="windows-1251" standalone="yes"?>
<root>
  <fields>
    <field100>00000</field100>
    <AMOUNT>1000.00</AMOUNT>
    <AMOUNT_ALL>1000.00</AMOUNT_ALL>
  </fields>
  <processor_type>External</processor_type>
  <operator_id>1000</operator_id>
  <initial_session_num>07021817411700010849</initial_session_num>
  <last_session></last_session>
  <transid></transid>
  <status>0</status>
  <last_error_code>0</last_error_code>
  <cancelled>0</cancelled>
  <payment_create_dt>07.02.2018 17:41:17</payment_create_dt>
  <first_try_dt>07.02.2018 17:41:17</first_try_dt>
  <next_try_dt>07.02.2018 18:41:32</next_try_dt>
  <num_of_tries>1</num_of_tries>
  <notes>
    <note validator_id="0" currency_id="RUR" nominal="1000" count="1"/>
  </notes>
  <saved_data></saved_data>
  <saved_card_string></saved_card_string>
  <last_payment_error_code>12</last_payment_error_code>
</root>




-----------------------


<?xml version="1.0" encoding="windows-1251" standalone="yes"?>
<root>
  <fields>
    <field100>131883</field100>
    <AMOUNT>1000.00</AMOUNT>
    <AMOUNT_ALL>1000.00</AMOUNT_ALL>
  </fields>
  <processor_type>External</processor_type>
  <operator_id>1000</operator_id>
  <initial_session_num>20051910285300010849</initial_session_num>
  <last_session></last_session>
  <transid></transid>
  <status>0</status>
  <last_error_code>0</last_error_code>
  <cancelled>0</cancelled>
  <payment_create_dt>20.05.2019 10:28:53</payment_create_dt>
  <first_try_dt>20.05.2019 10:28:53</first_try_dt>
  <next_try_dt>20.05.2019 11:29:28</next_try_dt>
  <num_of_tries>1</num_of_tries>
  <notes>
    <note validator_id="0" currency_id="RUR" nominal="1000" count="1"/>
  </notes>
  <saved_data></saved_data>
  <saved_card_string></saved_card_string>
  <last_payment_error_code>288</last_payment_error_code>
</root>



