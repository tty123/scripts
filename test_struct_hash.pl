#!/usr/bin/env perl
#===============================================================================
#
#         FILE: test_struct_hash.pl
#
#        USAGE: ./test_struct_hash.pl  
#
#  DESCRIPTION: 
#
#      OPTIONS: ---
# REQUIREMENTS: ---
#         BUGS: ---
#        NOTES: ---
#       AUTHOR: Tolmachev V.N. (), 
# ORGANIZATION: 
#      VERSION: 1.0
#      CREATED: 24.06.2019 16:07:11
#     REVISION: ---
#===============================================================================

use strict;
use warnings;
use utf8;
use Data::Printer;

my %HSH=('time1'=>{'ip'=>{'descr'=>{'oid'=>"JSON_data"}}});
my @key;
p %HSH;

my @KK = keys %HSH;
 $key[0] = @KK[0];
print "$key[0]\n";

@KK = keys %{$HSH{$key[0]}};
 $key[1] = @KK[0];
print "$key[1]\n";

@KK = keys %{$HSH{$key[0]}{$key[1]}};
 $key[2] = @KK[0];
print "$key[2]\n";

@KK = keys %{$HSH{$key[0]}{$key[1]}{$key[2]}};
 $key[3] = @KK[0];
print "$key[3]\n";

print "$HSH{$key[0]}{$key[1]}{$key[2]}{$key[3]}\n";
#-----------------------------------------
my @dat;
my %thsh = %HSH;
while (my ($keyh, $res) = each %thsh){
push(@dat,$keyh);
%thsh = %$res;
} 

