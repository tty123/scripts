#!/usr/bin/env perl
#===============================================================================
#
#         FILE: test_printer_ESCPOS.pl
#
#        USAGE: ./test_printer_ESCPOS.pl  
#
#  DESCRIPTION: печатать на  POS принтер 
#               VKP80II 
#      OPTIONS: ---
# REQUIREMENTS: ---
#         BUGS: ---
#        NOTES: ---
#       AUTHOR: Tolmachev V.N. (), 
# ORGANIZATION: 
#      VERSION: 1.0
#      CREATED: 22.06.2020 11:59:53
#     REVISION: ---
#===============================================================================

use strict;
use warnings;
use utf8;
use Data::Printer;
#use encoding 'iso-8859-1';
use Encode qw(encode decode);

$|=1; #отключаем буферизацию вывода

sub print_to_dev {
my $device="/dev/usb/lp0";
open(PRN,"+>$device")|| die "device not open: $!";

my $cmd_str_ESC = kbyte('2764');
my $cmd_str_LF = kbyte ('10'); # \x{1b}\x{52}\x{46}
my $string=encode("CP866","Vladlink");
#my $string = "\x{0a}\x{82}\x{ab}\x{a0}\x{a4}\x{ab}\x{a8}\x{ad}\x{aa}\x{0a}\x{98}\x{74}\x{65}\x{73}\x{74}\x{0a}";
										   
p $string;
print(PRN  $cmd_str_ESC);
print(PRN $cmd_str_LF);
print(PRN $string); 

my $ret;
#read PRN, $ret, 1;
#p $ret;
##close(PRN);
#print unpack('B*',$ret)."\n";
#print unpack('H*',$ret)."\n";
#print unpack('C*',$ret)."\n";
#
#my $status = unpack('C*', $ret);
#
}
sub kbyte{
my $num=shift;
return pack('H*', $num);
}

sub stchar{
my $num=shift;
return pack('C*', $num);
}

print_to_dev;
sleep(5);
print_to_dev;
sleep(5);
print_to_dev;
close (PRN);
