#!/usr/bin/env perl
#===============================================================================
#
#         FILE: test_split.pl
#
#        USAGE: ./test_split.pl  
#
#  DESCRIPTION: тестирование split, разбивка строки в массив значений
#
#      OPTIONS: ---
# REQUIREMENTS: ---
#         BUGS: ---
#        NOTES: ---
#       AUTHOR: Tolmachev V.N. (), 
# ORGANIZATION: 
#      VERSION: 1.0
#      CREATED: 20.01.2020 16:30:56
#     REVISION: ---
#===============================================================================

use strict;
use warnings;
use utf8;
use 5.010;
use Data::Printer;

my $a = "1, 2,3 4  5, 6  back_door";
my $b = "12";
my @arr1 = split (/,\s?|\s+/, $a);
my @arr2 = split (/,\s?|\s+/, $b);

p @arr1;
p @arr2;

# результат @arr1 = (1,2,3,4,5,6,back_door)
# результат @arr2 = (12)

my $c = "bc00ff1a";
my @arr3 = split (/(.{2})/, $c, 2);

say '@arr3='; 
p@arr3;
my @arr4 = unpack '(a[2])*',$c; #разбиваем строку на байты

say '@arr4';
p @arr4;


my $d = "bc00ff1affccddgghhkk";
my @arr5 = unpack '(a[10])*',$d; #разбиваем строку на байты

say '@arr5';
p @arr5;

say hex('FF');
#-------------------------------------------

#--- Разбиваем и запихиваем в массив ----
my $str = '1234567';
my @arr;
unshift (@arr, $_) for split ('', $str);
p @arr;
