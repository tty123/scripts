#!/usr/bin/env perl
#===============================================================================
#
#         FILE: port_ins_in_syslog_table.pl
#
#        USAGE: ./port_ins_in_syslog_table.pl  
#
#  DESCRIPTION: скрипт для вставки  символа 'x' в пустые поля port port_state таблиц вида
#  log_10_240_20_0_15400000
#
#      OPTIONS: ---
# REQUIREMENTS: ---
#         BUGS: ---
#        NOTES: ---
#       AUTHOR: Tolmachev V.N. (), 
# ORGANIZATION: 
#      VERSION: 1.0
#      CREATED: 21.01.2019 12:24:14
#     REVISION: ---
#===============================================================================

use strict;
use warnings;
use utf8;
use DBI();
use DBD::Pg;

my @tables_names_mass;
my $db_name = "postgres";
my $host = '10.240.240.54';
#$host = '172.17.31.13'; +++++++++ new ++++ адрес базы ++ отключено до конечной отладки скрипта
my $user = "postgres";
my $pass = "postgres";

    my $dbh = DBI->connect("DBI:Pg:dbname=$db_name; host=$host", $user, $pass,{AutoCommit => 1}) or die "Error connecting to database";
    $dbh->do("set application_name = 'perl_port_ins_syslog';")
                or  die "My Error  set application name";  #++++++++++++++++new+++++++++

DBI->trace(1); #++++++++++++++++new++включение отладки

  
    my $query = $dbh->prepare("SELECT * from syslog.tables; ");
    $query->execute or die "Error select from database";
    undef @tables_names_mass;
    my $table_name;
    $query->bind_columns(undef, \($table_name));
    while ($query->fetch)
    {
        push (@tables_names_mass, $table_name);
        print "$table_name \n";
    }
    $query->finish;

   foreach (@tables_names_mass){
    $dbh->do("update syslog.$_ set port='x', port_state='x' where port is NULL or port_state is NULL;") or die "Error inserting 'x'";

}
