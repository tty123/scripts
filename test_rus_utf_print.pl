#!/usr/bin/env perl 
#===============================================================================
#
#         FILE: test_rus_utf_print.pl
#
#        USAGE: ./test_rus_utf_print.pl  
#
#  DESCRIPTION: 
#
#      OPTIONS: ---
# REQUIREMENTS: ---
#         BUGS: ---
#        NOTES: ---
#       AUTHOR: Tolmachev V.N. (), 
# ORGANIZATION: 
#      VERSION: 1.0
#      CREATED: 25.12.2019 14:24:40
#     REVISION: ---
#===============================================================================

use strict;
use warnings;
use utf8;
binmode(STDOUT,':utf8'); # чтобы не вылазило предупреждение "wide character in print"


print "Привет, мир!\n";

