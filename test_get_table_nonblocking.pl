#!/usr/bin/env perl
#===============================================================================
#
#         FILE: test_get_table.pl
#
#        USAGE: ./test_get_table.pl  
#
#  DESCRIPTION: отработка работы метода get_table
#  		модуля Net::SNMP в неблокирующем режиме
#
#      OPTIONS: ---
# REQUIREMENTS: ---
#         BUGS: ---
#        NOTES: ---
#       AUTHOR: Tolmachev V.N. (), 
# ORGANIZATION: 
#      VERSION: 1.0
#      CREATED: 04.06.2019 11:34:05
#     REVISION: ---
#===============================================================================

use strict;
use warnings;
use utf8;
use Net::SNMP;
use AnyEvent;
use Data::Printer;
use Data::Dumper; 
use Devel::Timer;



my $t = Devel::Timer -> new();
$t -> mark ('start node_investigate');


my %dlink3=(                          # DES-3200-28
	'sysDescr'                    => '1.3.6.1.2.1.1.1',   # название модели свитча
	'sysObjectID'                 => '1.3.6.1.2.1.1.2',   # VendorID
	'sysUpTime'                   => '1.3.6.1.2.1.1.3',   # время работы после последней перезагрузки
	'sysName'                     => '1.3.6.1.2.1.1.5',   # присвоенное пользователем имя
	'ifAdminStatus'               => '1.3.6.1.2.1.2.2.1.7', # массив значений по каждому сетевому интерфейсу
	'ifOperStatus'                => '1.3.6.1.2.1.2.2.1.8', # массив значений по каждому сетевому интерфейсу
	'ifInUcastPkts'               => '1.3.6.1.2.1.2.2.1.11', # массив значений по каждому сетевому интерфейсу
	'ifInNUcastPkts'              => '1.3.6.1.2.1.2.2.1.12', # массив значений по каждому сетевому интерфейсу
	'ifInErrors'                  => '1.3.6.1.2.1.2.2.1.14', # массив значений по каждому сетевому интерфейсу
	'ifOutUcastPkts'              => '1.3.6.1.2.1.2.2.1.11', # массив значений по каждому сетевому интерфейсу точка-точка
	'ifOutNUcastPkts'             => '1.3.6.1.2.1.2.2.1.12', # массив значений по каждому сетевому интерфейсу широковещат+мультикаст
	'ifOutErrors'                 => '1.3.6.1.2.1.2.2.1.20', # массив значений по каждому сетевому интерфейсу
	'entPhysicalSerialNum'        => '1.3.6.1.2.1.47.1.1.1.1.11', # строка с серийником
	'entPhysicalDescr'            => '1.3.6.1.2.1.47.1.1.1.1.2' , # (.*) 1 строка-модель устройства далее описание каждого порта 100мбит 10000 fiber copper
	'ifInMulticastPkts'           => '1.3.6.1.2.1.31.1.1.1.2', # (.*) количество мультикаст пакетов по  портам	
	'ifInBroadcastPkts'           => '1.3.6.1.2.1.31.1.1.1.3', # (.*) количество широковещат  пакетов по  портам	
	'ifOutMulticastPkts'          => '1.3.6.1.2.1.31.1.1.1.4', # (.*) количество мультикаст пакетов по всем портам	
	'ifOutBroadcastPkts'          => '1.3.6.1.2.1.31.1.1.1.5', # (.*) количество широковещат пакетов по всем портам
	#-- Cabel diagnostics --
	'swEtherCableDiagLinkStatus'  => '1.3.6.1.4.1.171.12.58.1.1.1.3', # (.*) состояние линка-соединения 	
	'swEtherCableDiagPair1Status' => '1.3.6.1.4.1.171.12.58.1.1.1.4', # (.*) состояние 1 пары {ok(0),open(1),short(2),open-short(3),crosstalk(4),unknown(5),count(6),no-cable(7),other(8)} 	
	'swEtherCableDiagPair2Status' => '1.3.6.1.4.1.171.12.58.1.1.1.5', # (.*) состояние 2 пары {ok(0),open(1),short(2),open-short(3),crosstalk(4),unknown(5),count(6),no-cable(7),other(8)} 	
	'swEtherCableDiagPair3Status' => '1.3.6.1.4.1.171.12.58.1.1.1.6', # (.*) состояние 3 пары {ok(0),open(1),short(2),open-short(3),crosstalk(4),unknown(5),count(6),no-cable(7),other(8)} 	
	'swEtherCableDiagPair4Status' => '1.3.6.1.4.1.171.12.58.1.1.1.7', # (.*) состояние 4 пары {ok(0),open(1),short(2),open-short(3),crosstalk(4),unknown(5),count(6),no-cable(7),other(8)} 	
	'swEtherCableDiagPair1Length' => '1.3.6.1.4.1.171.12.58.1.1.1.8', # (.*) примерная длина кабеля 1 пары в метрах (работает при длине не менее 2м) 	
	'swEtherCableDiagPair2Length' => '1.3.6.1.4.1.171.12.58.1.1.1.9', # (.*) примерная длина кабеля 2 пары в метрах (работает при длине не менее 2м) 	
	'swEtherCableDiagPair3Length' => '1.3.6.1.4.1.171.12.58.1.1.1.10', # (.*) примерная длина кабеля 3 пары в метрах (работает при длине не менее 2м) 	
	'swEtherCableDiagPair4Length' => '1.3.6.1.4.1.171.12.58.1.1.1.11', # (.*) примерная длина кабеля 4 пары в метрах (работает при длине не менее 2м) 
	#-- CPU, Port, RAM utilization --
	'agentCPUutilizationIn5sec'   => '1.3.6.1.4.1.171.12.1.1.6.1', # загрузка CPU в % за 5сек интервал
	'agentCPUutilizationIn1min'   => '1.3.6.1.4.1.171.12.1.1.6.2', # загрузка CPU в % за 1мин  интервал
	'agentCPUutilizationIn5min'   => '1.3.6.1.4.1.171.12.1.1.6.3', # загрузка CPU в % за 5мин интервал
	'agentPORTutilizationTX'      => '1.3.6.1.4.1.171.12.1.1.8.1.2', # (.*) текущая интенсивность отправляемых пакетов по порту
	'agentPORTutilizationRX'      => '1.3.6.1.4.1.171.12.1.1.8.1.3', # (.*) текущая интенсивность отправляемых пакетов по порту
	'agentPORTutilizationUtil'    => '1.3.6.1.4.1.171.12.1.1.8.1.4', # (.*) текущее процентное отношение статистики использования порта
	'agentDRAMutilization'        => '1.3.6.1.4.1.171.12.1.1.9.1.4', # (.*) загрузка оперативной памяти 0-100% 
	#-- Power, Fan, Temp --
	'agentSerialNumber'	      => '1.3.6.1.4.1.171.12.1.1.12', # серийный номер устройства
);




my %main_data_hsh; # хэш для всех данных
my $community = 'public';
my @ipm=('10.240.5.109');
	while (@ipm){	# основной цикл выборки данных
			my $cv = AE::cv;
			   $cv -> begin;
		my $threads = 100; # задаём количество тредов
		for (my $i=1; $i <= $threads; $i++){  # организовываем $threads потоков по каждому ip
				$cv -> begin;
	my $ip = pop (@ipm);
	my $model_oids = \%dlink3; # ссылка на  хэш с оидами модели свича
#p $vendor_oid;
print "-----------------\n";
		my $num_oids = scalar keys %{$model_oids};
		my @oids_descr = keys %{$model_oids};
		
print " ip $ip started thread: $i\n";
		my $next;        # используем замыкание
		$next = sub {
			return if (!@oids_descr);
			my $descr = pop (@oids_descr);
#print "$descr\n";
			my $oid = $model_oids -> {$descr};
#-----------------------------------------------------
			
 my ($session, $error) = Net::SNMP->session(
					-hostname      => $ip,
					-community     => $community,
					-translate     => [-octetstring => 0],
					-version       => 'snmpv2c',
					-timeout       => 2,
					-nonblocking  => 1,
				           ) ->get_table(
					-baseoid => $oid,
					-callback => sub{
						my $self = shift;
p $self;
						my $result = $self->var_bind_list();
						if (!defined $result->{$oid}){ while (my ($key,$value) = each %$result){
						$main_data_hsh{$ip}{$descr}{$oid}{$key}=$value}
										}
						print "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@\n";
						p $self;
							},
						);
#p $result;
#			if (!defined $result->{$oid}){ while (my ($key,$value) = each %$result){
#				$main_data_hsh{$ip}{$descr}{$oid}{$key}=$value}
#							} #$main_data_hsh{$ip}{$descr} = $result};
#print "+++++++++++++++++++++++\n";
#p $main_data_hsh{$ip};
#-----------------------------------------------------------
			$next->();
				};
$next->();
print " ip $ip ended thread: $i\n";
			$cv -> end;
			if (!@ipm){last}; # заканчиваем основной цикл, если адреса закончились
		}
			$cv -> end;
			$cv -> recv;
	}


p %main_data_hsh;


$t -> mark ('end node_investigate');
$t -> report();

