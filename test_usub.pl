#!/usr/bin/env perl
#===============================================================================
#
#         FILE: test_usub.pl
#
#        USAGE: ./test_usub.pl  
#
#  DESCRIPTION: Пример работы  анонимной функции  и замыкания
#
#      OPTIONS: ---
# REQUIREMENTS: ---
#         BUGS: ---
#        NOTES: ---
#       AUTHOR: Tolmachev V.N. (), 
# ORGANIZATION: 
#      VERSION: 1.0
#      CREATED: 28.02.2019 12:32:54
#     REVISION: ---
#===============================================================================

use strict;
use warnings;
use utf8;
use diagnostics;

# анонимная функция ------------------------
my $s=sub{
	my $msg=shift;
	printf "Hellow chubackkus!!!: %s\n", $msg;
	return 241;	
	};
$s->("Zaebok vector plus 34 chuvaki!!!");

# замыкание --------------------------------

sub adder($) {
	my $x=shift;
	return sub ($) {
	my $y=shift;
	return $x+$y;
	};
};
my $add1=adder(1);
print $add1."\n";
print $add1->(10)."\n";

my $sub1=adder(-1);
print $sub1->(10)."\n";
#--------------------------------------------------

sub f {
	my $v=shift;
		print 'f: \$v = '.\$v.' $v='.$v."\n";
	return \$v;
}
my $a=f("grendel");
print "\$a = $a". '$$a = '. $$a."\n";
#--------------------------------------------------




