#!/usr/bin/env perl
#===============================================================================
#
#         FILE: test_CRC.pl
#
#        USAGE: ./test_CRC.pl  
#
#  DESCRIPTION: отработка подсчёта CRC
#
#      OPTIONS: ---
# REQUIREMENTS: ---
#         BUGS: ---
#        NOTES: ---
#       AUTHOR: Tolmachev V.N. (), 
# ORGANIZATION: 
#      VERSION: 1.0
#      CREATED: 05.03.2020 12:27:06
#     REVISION: ---
#===============================================================================

use strict;
use warnings;
use utf8;
use Data::Printer;
use Digest::CRC;
use 5.010;

my $poly='8408';
print Digest::CRC::crc16_hex(pack('H*','02030619'),,,,,$poly ); # последовательность для купюроприёмника
say;
my $ctx = Digest::CRC->new(type=>"crc16", poly=>0x);
#$ctx = Digest::CRC->new(width=>16, init=>0x2345, xorout=>0x0000, 
#        refout=>1, poly=>0x8005, refin=>1, cont=>1);
		 
		#		$ctx->add($data);
		#		$ctx->addfile(*FILE);
		 
		#		$digest = $ctx->digest;
		#		$digest = $ctx->hexdigest;
		#		$digest = $ctx->b64digest;
sub ccnet_crc16{               # принимаем строку в hex-а[
my $InDataStr=shift;
my $POLYNOM = 33800;# = 8408h
my @InData = unpack '(a[2])*',$InDataStr;
my $sizeData=@InData;
my $CRC=0;
        for (my $i=0; $i < $sizeData; $i++)
	{
	        $CRC^=hex($InData[$i]);
	                for (my $j=0; $j<8; $j++)
			{
				if ($CRC&1){
					$CRC>>=1;
					$CRC^=$POLYNOM;
				}
				else
				{
					$CRC>>=1; 
				}
			}    
		}
		$CRC = sprintf("%x", $CRC); # переводим в hex, может получиться 3 символа вместо 4-х, тогда ->
		if (length($CRC) < 4){    # -> добавляем 0, если число получилось в 3 знака (наппр. при 02030619)
			$CRC ='0'.$CRC;
		}
		my @arr = unpack '(a[2])*', $CRC;  #разбиваем строку на байты
		p @arr;
		return $arr[1].$arr[0];
	}
say ccnet_crc16 ('02030619');
