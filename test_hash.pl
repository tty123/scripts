#!/usr/bin/env perl
#===============================================================================
#
#         FILE: test_hash.pl
#
#        USAGE: ./test_hash.pl  
#
#  DESCRIPTION: интересные способы инициализации хэша
#               загружаем данные из hash.dat
#      OPTIONS: ---
# REQUIREMENTS: ---
#         BUGS: ---
#        NOTES: ---
#       AUTHOR: Tolmachev V.N. (), 
# ORGANIZATION: 
#      VERSION: 1.0
#      CREATED: 20.05.2020 12:52:25
#     REVISION: ---
#===============================================================================

use strict;
use warnings;
use utf8;
use Data::Printer;

my %hsh = %{do "/home/tvn/Scripts/hash.dat"}; #инициализация из файла
p %hsh;
