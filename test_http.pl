#!/usr/bin/env perl
#===============================================================================
#
#         FILE: test_http.pl
#
#        USAGE: ./test_http.pl  
#
#  DESCRIPTION: 
#
#      OPTIONS: ---
# REQUIREMENTS: ---
#         BUGS: ---
#        NOTES: ---
#       AUTHOR: Tolmachev V.N. (), 
# ORGANIZATION: 
#      VERSION: 1.0
#      CREATED: 13.03.2019 15:11:30
#     REVISION: ---
#===============================================================================

use strict;
use warnings;
use utf8;
use HTTP::Daemon;
use HTTP::Status;

use Data::Dumper;
use Data::Printer;
use DDP;

use AnyEvent::HTTPD;


my $httpd = AnyEvent::HTTPD->new (port => 8442);
my $arr=$httpd->allowed_methods;
push (@$arr, "OPTIONS");


my $json="[[one,1],[two,2],[three,3],[forth,4]]";
$httpd->reg_cb (
   request => sub {
      my ($httpd, $req) = @_;
	my $url=$req->url;
	my $mt=$req->method;
	my $hds=$req->headers;
	print "--- /$httpd ----\n";
	p $httpd;
	print "---- debug headers---\n";
	p $hds;
	print "---- debug methods---\n";
	p $mt;
	print "---- debug urls---\n";
	p $url;
	my %dt=$req->vars;
print "\$url = $url \n";
print %{$hds}." \n";
#-----------------------------
 if ($mt eq 'OPTIONS'){
	 $req->respond([200,"", {   'Access-Control-Allow-Origin'=>'*', 
				    'Access-Control-Allow-Methods'=>'POST,GET,OPTIONS', 
				    'Access-Control-Allow-Headers'=>'DNT,User-Agent,X-Requested-With,If-Modified-Since,Cache-Control,Content-Type,Range',
				    #'Access-Control-Expose-Headers'=>'*',
			    }]);
 }
#-----------------------------
#	if (($url eq '/')&&($mt eq 'POST')){
		#if ($dt{api} eq 'syslog'){
			#if ($dt{cmd} eq '100'){
			      $req->respond ([200,"",{ 'Access-Control-Allow-Origin'=>'*', 'Access-Control-Allow-Methods'=>'POST,GET,OPTIONS', 'Content-Type' =>'application/json'}, $json]);
				print "command running \n";
				#	}else{$req->respond ({ content => ['text/plain', 'Error! Wrong command!']} ) }; # elsif(){};
				#	}
				#	}	
my @tr= keys %{$hds};
while ( my ($key, $value) = each(%$hds) ) {
        print "$key => $value\n";
    };
p %dt;

#print @tr;

#	if ($url->path=~/\/slg/){
       
#	}

   },
   #'/test' => sub {
   #   my ($httpd, $req) = @_;
 
   #},
);
 
$httpd->run; # making a AnyEvent condition variable would also work



#my $d = HTTP::Daemon->new(LocalPort => 8200) || die;
#print "Please contact me at: <URL:", $d->url, ">\n";
# 
#while (my $c = $d->accept) {
#my  $r = $c->get_request;
#  if ($r) {
#    $c->send_basic_header;
#    $c->print("Content-Type: text/plain");
#    $c->send_crlf;
#    $c->send_crlf;
#    $c->print("Test Ok\n");
# 
#  }
#  $c->close;
#  undef($c);
#}
#exit;
