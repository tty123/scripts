#!/usr/bin/env perl
#===============================================================================
#
#         FILE: test_defined.pl
#
#        USAGE: ./test_defined.pl  
#
#  DESCRIPTION: 
#
#      OPTIONS: ---
# REQUIREMENTS: ---
#         BUGS: ---
#        NOTES: ---
#       AUTHOR: Tolmachev V.N. (), 
# ORGANIZATION: 
#      VERSION: 1.0
#      CREATED: 18.03.2020 12:04:07
#     REVISION: ---
#===============================================================================

use strict;
use warnings;
use utf8;
use 5.010;

my  $k=\"a";# Важно!!!! ''- пустое значение defined воспринимает как инициализированную переменную

if (defined $k)
{ say '$k = '.$k;}
else {say '$k = undefined'};

my $m=undef;
say "\$m =".scalar(defined($m));
