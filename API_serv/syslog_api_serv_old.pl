#!/usr/bin/env perl
#===============================================================================
#
#         FILE: syslog_api_serv.pl
#
#        USAGE: ./syslog_api_serv.pl  
#
#  DESCRIPTION: сервер принимает http запрос с (ip. port, start time, stop time)
#               и отдаёт в JSON результат поиска в базе syslog.
#      OPTIONS: ---
# REQUIREMENTS: ---
#         BUGS: ---
#        NOTES: ---
#       AUTHOR: Tolmachev V.N. (), 
# ORGANIZATION: 
#      VERSION: 1.0
#      CREATED: 07.03.2019 17:40:39
#     REVISION: ---
#===============================================================================

use strict;
use warnings;
use utf8;

use DBI();
use DBD::Pg;
use JSON;
use AnyEvent::HTTPD;
#----------------------------------------------------------
#  заполнить для подключения к базе 
#----------------------------------------------------------
my $host = '172.17.31.45'; # 172.17.31.13 ++++++++++++++ change
my $db_name = "syslogbd2";
my $port=5432;
my $user = "sysloguser";
my $pass = "9vZcCZWX";
my $dbh;
#==========================================================

#==== Работа с базой данных ===============================
sub db_conn {
$dbh = DBI->connect("DBI:Pg:dbname=$db_name; host=$host; port=$port", $user, $pass,{AutoCommit => 1}) 
		or die "Error connecting to database";
}
&db_conn; # обязательный запуск процедуры чтобы инициализировать глобальную переменную $dbh
#==========================================================

my $httpd = AnyEvent::HTTPD->new (port => 8462);

$httpd->reg_cb (
   request => sub {
      my ($httpd, $req) = @_;
	my $url=$req->url;
	my $mt=$req->method;
	my $hds=$req->headers;
	my %dt=$req->vars; # хэш значений формы
	my @data;
	if (($url eq '/slg')&&($mt eq 'POST')){
		if ($dt{api} eq 'syslog'){
			if ($dt{cmd} eq 'get_port_hist'){
		$dbh->do("set application_name = 'perl_syslogm_api_server';");
		# загружаем список таблиц в массив
		my $query = $dbh->prepare("SELECT * FROM api_get_port_hist('$dt{ip}','$dt{t_from}','$dt{t_to}','$dt{port}');");
			  $query->execute;
		my ($addr,$stime,$fac,$sev,$mess,$port,$port_state);
		$query->bind_columns(undef,\$addr,\$stime,\$fac,\$sev,\$mess,\$port,\$port_state);
			while ($query->fetch){
				push (@data,[$addr,$stime,$fac,$sev,$mess,$port,$port_state]);
				}
		$query->finish;
		if (@data){ my $json = encode_json(\@data); # подготовили  к отправке
			      $req->respond ({ content => ['application/json', $json]});
				@data=();
				print "command $dt{cmd} running \n";
				} else{$req->respond ({ content => ['application/json', '{"error":"no result"}']} ) };
			} else{$req->respond ({ content => ['application/json', '{"error":"cmd"}']} ) }; 	
					} else{$req->respond ({ content => ['application/json', '{"error":"api"}']} ) }; 
	} else{$req->respond ({ content => ['application/json','{"error":"url"}']} ) }; 
   },
);

my $dbi_timer = AE::timer 0, 120, sub   # 2 минутный таймер для проверки и восстановления коннекта к базе
{
    if (my $p=$dbh->ping) 
    {   
        print "$p postgresql connect active.\n";
    }   
    else 
    {   
        print "$p postgresql start connection.\n";
        &db_conn;
    }   
};
$httpd->run; # запуск сервера 
